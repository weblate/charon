<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220929084608 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE job (id INT NOT NULL, space_id INT NOT NULL, type INT NOT NULL, data JSON NOT NULL, status INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_FBD8E0F823575340 ON job (space_id)');
        $this->addSql('COMMENT ON COLUMN job.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN job.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE space (id INT NOT NULL, submission_id INT NOT NULL, domain VARCHAR(255) NOT NULL, details JSON NOT NULL, contact_email VARCHAR(255) NOT NULL, contact_name VARCHAR(255) NOT NULL, locale VARCHAR(255) NOT NULL, status INT DEFAULT 0 NOT NULL, organization_name VARCHAR(255) NOT NULL, enabled BOOLEAN NOT NULL, office INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2972C13AA7A91E0B ON space (domain)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2972C13AE1FD4933 ON space (submission_id)');
        $this->addSql('COMMENT ON COLUMN space.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN space.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE submission (id INT NOT NULL, type VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, object TEXT NOT NULL, topics JSON NOT NULL, main_actions TEXT NOT NULL, creation_year INT DEFAULT NULL, identifier VARCHAR(255) DEFAULT NULL, website VARCHAR(255) DEFAULT NULL, nb_employees BIGINT DEFAULT NULL, nb_members BIGINT DEFAULT NULL, nb_beneficiaries BIGINT DEFAULT NULL, budget BIGINT DEFAULT NULL, reasons TEXT DEFAULT NULL, domain VARCHAR(255) NOT NULL, identity VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, status INT DEFAULT 0 NOT NULL, locale VARCHAR(255) NOT NULL, already_used JSON NOT NULL, accounts_needed VARCHAR(255) NOT NULL, country VARCHAR(255) NOT NULL, responsible INT NOT NULL, organisation_language VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_DB055AF3A7A91E0B ON submission (domain)');
        $this->addSql('COMMENT ON COLUMN submission.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN submission.updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE messenger_messages (id BIGSERIAL NOT NULL, body TEXT NOT NULL, headers TEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, available_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, delivered_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_75EA56E0FB7336F0 ON messenger_messages (queue_name)');
        $this->addSql('CREATE INDEX IDX_75EA56E0E3BD61CE ON messenger_messages (available_at)');
        $this->addSql('CREATE INDEX IDX_75EA56E016BA31DB ON messenger_messages (delivered_at)');
        $this->addSql('CREATE OR REPLACE FUNCTION notify_messenger_messages() RETURNS TRIGGER AS $$
            BEGIN
                PERFORM pg_notify(\'messenger_messages\', NEW.queue_name::text);
                RETURN NEW;
            END;
        $$ LANGUAGE plpgsql;');
        $this->addSql('DROP TRIGGER IF EXISTS notify_trigger ON messenger_messages;');
        $this->addSql('CREATE TRIGGER notify_trigger AFTER INSERT OR UPDATE ON messenger_messages FOR EACH ROW EXECUTE PROCEDURE notify_messenger_messages();');
        $this->addSql('ALTER TABLE job ADD CONSTRAINT FK_FBD8E0F823575340 FOREIGN KEY (space_id) REFERENCES space (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE space ADD CONSTRAINT FK_2972C13AE1FD4933 FOREIGN KEY (submission_id) REFERENCES submission (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE job DROP CONSTRAINT FK_FBD8E0F823575340');
        $this->addSql('ALTER TABLE space DROP CONSTRAINT FK_2972C13AE1FD4933');
        $this->addSql('DROP TABLE job');
        $this->addSql('DROP TABLE space');
        $this->addSql('DROP TABLE submission');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
