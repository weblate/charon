<?php

namespace App\Tests\Controller;

use App\Entity\Job;
use App\Entity\NormalizedOrganization;
use App\Entity\Space;
use App\Factory\JobFactory;
use App\Factory\SpaceFactory;
use App\Factory\SubmissionFactory;
use App\Services\OrganizationLookup\CheckRNAService;
use App\Services\OrganizationLookup\CheckSIRETService;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class APIControllerTest extends WebTestCase
{
    public function testSearchOrganisationWithException(): void
    {
        $client = static::createClient();
        $siretServiceMock = $this->createMock(CheckSIRETService::class);
        $siretServiceMock->expects($this->once())->method('getInfosFromText')->with('Framasoft')->willThrowException(new \Exception('Something went wrong'));
        $container = static::getContainer();
        $container->set(CheckSIRETService::class, $siretServiceMock);
        $client->request('POST', '/api/v1/searchOrganization', ['search' => 'Framasoft']);
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals('Unable to get details from data providers', $content['error']);
        $this->assertEquals('Something went wrong', $content['details']);
    }

    public function testSearchOrganisationWithNoParams(): void
    {
        $client = static::createClient();
        $client->request('POST', '/api/v1/searchOrganization');
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testSearchOrganisationWithNameSearchParam(): void
    {
        $client = static::createClient();
        $client->catchExceptions(false);

        $siretServiceMock = $this->createMock(CheckSIRETService::class);
        $siretServiceMock->expects($this->once())->method('getInfosFromText')->with('Framasoft')->willReturn([
            (new NormalizedOrganization())
            ->setName('FRAMASOFT')
            ->setCreationYear('2003')
            ->setSiren('500715776')
            ->setTrancheEffectifs('11')
            ->setCategorieJuridique('Association déclarée')
        ]);
        $container = static::getContainer();
        $container->set(CheckSIRETService::class, $siretServiceMock);

        $client->request('POST', '/api/v1/searchOrganization', ['search' => 'Framasoft']);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals('FRAMASOFT', $content[0]['name']);
        $this->assertEquals('2003', $content[0]['creationYear']);
        $this->assertEquals('500715776', $content[0]['siren']);
        $this->assertEquals('11', $content[0]['trancheEffectifs']);
        $this->assertEquals('Association déclarée', $content[0]['categorieJuridique']);
    }

    public function testSearchOrganisationWithBadNameSearchParam(): void
    {
        $client = static::createClient();
        $client->catchExceptions(false);

        $siretServiceMock = $this->createMock(CheckSIRETService::class);
        $siretServiceMock->expects($this->once())->method('getInfosFromText')->with('Something else')->willReturn([]);
        $container = static::getContainer();
        $container->set(CheckSIRETService::class, $siretServiceMock);

        $client->request('POST', '/api/v1/searchOrganization', ['search' => 'Something else']);
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals('No results found', $content['error']);
    }

    public function testSearchOrganisationWithSIRENSearchParam(): void
    {
        $client = static::createClient();
        $client->catchExceptions(false);

        $data = json_decode(file_get_contents(__DIR__ . '/../Fixtures/Services/SIRET/SIREN_Frama.json'), true);

        $siretServiceMock = $this->createMock(CheckSIRETService::class);
        $siretServiceMock->expects($this->once())->method('getInfosFromText')->with('500715776')->willReturn($data);
        $container = static::getContainer();
        $container->set(CheckSIRETService::class, $siretServiceMock);

        $client->request('POST', '/api/v1/searchOrganization', ['search' => '500715776']);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals('W751179246', $content['uniteLegale']['identifiantAssociationUniteLegale']);
        $this->assertEquals('2003-12-02', $content['uniteLegale']['dateCreationUniteLegale']);
        $this->assertEquals('2016-01-01', $content['uniteLegale']['periodesUniteLegale'][0]['dateDebut']);
        $this->assertEquals('FRAMASOFT', $content['uniteLegale']['periodesUniteLegale'][0]['denominationUniteLegale']);
        $this->assertEquals('94.99Z', $content['uniteLegale']['periodesUniteLegale'][0]['activitePrincipaleUniteLegale']);
    }

    public function testSearchOrganisationWithRNASearchParam(): void
    {
        $client = static::createClient();
        $client->catchExceptions(false);

        $data = json_decode(json_decode(file_get_contents(__DIR__ . '/../Fixtures/Services/RNA/FRAMASOFT.json'), true)['content'], true);

        $rnaServiceMock = $this->createMock(CheckRNAService::class);
        $rnaServiceMock->expects($this->once())->method('getInfosFromText')->with('W751179246')->willReturn($data);
        $container = static::getContainer();
        $container->set(CheckRNAService::class, $rnaServiceMock);

        $client->request('POST', '/api/v1/searchOrganization', ['search' => 'W751179246']);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals('2003-12-02', $content['association']['date_creation']);
        $this->assertEquals('FRAMASOFT', $content['association']['titre']);
        $this->assertEquals('éducation populaire aux enjeux du numérique et des communes culturels', $content['association']['objet']);
        $this->assertEquals('69007', $content['association']['adresse_code_postal']);
    }

    /**
     * @dataProvider dataForTestIsAvailable
     */
    public function testIsAvailable(string $subdomain, int $code): void
    {
        SubmissionFactory::createOne(['domain' => 'taken']);
        $client = static::createClient();
        $client->catchExceptions(false);
        $client->request('POST', '/api/v1/isAvailable', ['subdomain' => $subdomain]);
        $this->assertEquals($code, $client->getResponse()->getStatusCode());
    }

    public function dataForTestIsAvailable(): array
    {
        return [
            ['s3', 409],
            ['taken', 409],
            ['', 400],
            ['available', 200]
        ];
    }

    public function testGetJobsNotloggedIn(): void
    {
        $client = static::createClient();
        $client->request('GET', '/api/v1/jobs', ['status' => 'done']);
        $this->assertEquals(401, $client->getResponse()->getStatusCode());
        $this->assertContains('www-authenticate', array_keys($client->getResponse()->headers->all()));
        $this->assertStringContainsString('Basic', $client->getResponse()->headers->all()['www-authenticate'][0]);
    }

    /**
     * @dataProvider dataForTestGetJobs
     */
    public function testGetJobs(JobFactory $factory): void
    {
        JobFactory::createOne(['status' => Job::JOB_STATUS_ONGOING]);
        $job = $factory->create();
        $client = static::createClient();
        $client->request('GET', '/api/v1/jobs', ['status' => $job->getStatus()], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto'
        ]);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount($job->object()->getStatus() === Job::JOB_STATUS_ONGOING ? 2 : 1, $content['items']);
        $this->assertEquals($job->getStatus(), $content['items'][0]['status']);
    }

    public function dataForTestGetJobs(): iterable
    {
        return [
            yield [JobFactory::new(['status' => Job::JOB_STATUS_PENDING])],
            yield [JobFactory::new(['status' => Job::JOB_STATUS_ONGOING])],
            yield [JobFactory::new(['status' => Job::JOB_STATUS_ERROR])],
            yield [JobFactory::new(['status' => Job::JOB_STATUS_DONE])]
        ];
    }

    public function testGetMultipleJobs(): void
    {
        JobFactory::createMany(15, ['status' => Job::JOB_STATUS_PENDING]);
        $client = static::createClient();
        $client->request('GET', '/api/v1/jobs', ['status' => 'pending'], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto'
        ]);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals(1, $content['pagination']['current_page']);
        $this->assertTrue($content['pagination']['has_next_page']);
        $this->assertEquals(15, $content['pagination']['total_items']);
        $this->assertEquals(2, $content['pagination']['total_pages']);

        $client->request('GET', '/api/v1/jobs', ['status' => '0', 'page' => 2], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto'
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount(5, $content['items']);
        $this->assertFalse($content['pagination']['has_next_page']);

        $client->request('GET', '/api/v1/jobs', ['status' => '0', 'page' => 1, 'perPage' => 30], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto'
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount(15, $content['items']);
        $this->assertFalse($content['pagination']['has_next_page']);
    }

    public function testUpdateJob(): void
    {
        $job = JobFactory::createOne(['status' => Job::JOB_STATUS_PENDING]);
        $client = static::createClient();
        $client->request('PUT', '/api/v1/jobs/' . $job->getId(), [], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto'
        ]);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals(Job::JOB_STATUS_ONGOING, $content['status']);
    }

    public function testUpdateJobNotFound(): void
    {
        $client = static::createClient();
        $client->request('PUT', '/api/v1/jobs/235', [], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto'
        ]);
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider dataForTestFinalizeJob
     */
    public function testFinalizeJob(mixed $success, ?string $payload, int $status): void
    {
        $job = JobFactory::createOne(['status' => Job::JOB_STATUS_PENDING]);
        $client = static::createClient();
        $client->request('POST', '/api/v1/jobs/' . $job->getId(), [], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto'
        ], json_encode(['success' => $success, 'payload' => $payload]));
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals($status, $content['status']);
        $this->assertEquals($payload, $content['body']);
    }

    public function dataForTestFinalizeJob(): array
    {
        return [
            [true, 'whatever done', Job::JOB_STATUS_DONE],
            [false, 'whatever fails', Job::JOB_STATUS_ERROR],
            ['hello', 'whatever unknown', Job::JOB_STATUS_ERROR],
            [true, null, Job::JOB_STATUS_DONE]
        ];
    }

    public function testFinalizeJobWithNoPayload(): void
    {
        $job = JobFactory::createOne(['status' => Job::JOB_STATUS_PENDING]);
        $client = static::createClient();
        $client->request('POST', '/api/v1/jobs/' . $job->getId(), [], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto'
        ]);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals(Job::JOB_STATUS_ERROR, $content['status']);
        $this->assertEquals(null, $content['body']);
    }

    public function testGetSpacesNotloggedIn(): void
    {
        $client = static::createClient();
        $client->request('GET', '/api/v1/spaces', ['status' => 'done']);
        $this->assertEquals(401, $client->getResponse()->getStatusCode());
        $this->assertContains('www-authenticate', array_keys($client->getResponse()->headers->all()));
        $this->assertStringContainsString('Basic', $client->getResponse()->headers->all()['www-authenticate'][0]);
    }

    /**
     * @dataProvider dataForTestGetSpaces
     */
    public function testGetSpacesFilterByStatus(SpaceFactory $factory): void
    {
        SpaceFactory::createOne(['status' => Space::SPACE_STATUS_PENDING]);
        $space = $factory->create();
        $client = static::createClient();
        $client->request('GET', '/api/v1/spaces', ['status' => $space->getStatus()], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto'
        ]);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount($space->object()->getStatus() === Space::SPACE_STATUS_PENDING ? 2 : 1, $content['items']);
        $this->assertEquals($space->getStatus(), $content['items'][0]['status']);
    }

    public function dataForTestGetSpaces(): iterable
    {
        return [
            yield [SpaceFactory::new(['status' => Space::SPACE_STATUS_PENDING])],
            yield [SpaceFactory::new(['status' => Space::SPACE_STATUS_CREATED])],
        ];
    }

    public function testGetSpacesFilterByEnabled(): void
    {
        SpaceFactory::createOne(['enabled' => false]);
        $client = static::createClient();
        $client->request('GET', '/api/v1/spaces', ['enabled' => true], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto'
        ]);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertEmpty($content['items']);
    }

    public function testGetMultipleSpaces(): void
    {
        SpaceFactory::createMany(15, ['status' => Space::SPACE_STATUS_PENDING]);
        $client = static::createClient();
        $client->request('GET', '/api/v1/spaces', ['status' => 'pending'], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto'
        ]);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals(1, $content['pagination']['current_page']);
        $this->assertTrue($content['pagination']['has_next_page']);
        $this->assertEquals(15, $content['pagination']['total_items']);
        $this->assertEquals(2, $content['pagination']['total_pages']);

        $client->request('GET', '/api/v1/spaces', ['status' => '0', 'page' => 2], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto'
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount(5, $content['items']);
        $this->assertFalse($content['pagination']['has_next_page']);

        $client->request('GET', '/api/v1/spaces', ['status' => '0', 'page' => 1, 'perPage' => 30], [], [
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW' => 'toto'
        ]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount(15, $content['items']);
        $this->assertFalse($content['pagination']['has_next_page']);
    }
}
