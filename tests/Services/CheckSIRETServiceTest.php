<?php

namespace App\Tests\Services;

use App\Services\OrganizationLookup\CheckSIRETService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class CheckSIRETServiceTest extends KernelTestCase
{
    private CheckSIRETService $checkSIRETService;

    protected function setUp(): void
    {
        parent::setUp();
        $params = $this->createMock(ContainerBagInterface::class);
        $logger = $this->createMock(LoggerInterface::class);

        $buildResponse = function (string $method, string $url, $options): MockResponse {
            $fileName = match ($url) {
                "https://api.insee.fr/entreprises/sirene/V3/siret/48077929700013" => "SIRET",
                "https://api.insee.fr/entreprises/sirene/V3/siren/480779297" => "SIREN",
                "https://api.insee.fr/entreprises/sirene/V3/siren?q=periode(denominationUniteLegale:%22Framasoft%22)" => "Search",
                "https://api.insee.fr/metadonnees/V1/codes/nafr2/sousClasse/94.99Z", "https://api.insee.fr/metadonnees/V1/codes/nafr2/sousClasse/72.1Z" => "NAF",
                "https://api.insee.fr/token" => "Token",
            };
            $fileData = json_decode(file_get_contents(__DIR__ . '/../Fixtures/Services/SIRET/' . $fileName . '.json'), true);
            return new MockResponse($fileData['content'], ['http_code' => $fileData['statusCode']]);
        };

        $this->checkSIRETService = new CheckSIRETService(new MockHttpClient($buildResponse), $params, $logger);
    }

    public function testGetInfosFromSIRET()
    {
        $siretData = $this->checkSIRETService->getSIRETInfos('48077929700013');
        $this->assertEquals('ASS FORMATION SECOURISME PAYS PLOUAY', $siretData[0]->getName());
        $this->assertEquals('2004', $siretData[0]->getCreationYear());
    }

    public function testGetInfosFromSIREN()
    {
        $siretData = $this->checkSIRETService->getSIRENInfos('480779297');
        $etablissement = $siretData['uniteLegale']['periodesUniteLegale'][0];
        $this->assertEquals('ASS FORMATION SECOURISME PAYS PLOUAY', $etablissement['denominationUniteLegale']);
        $this->assertEquals('2017-07-09', $etablissement['dateDebut']);
    }

    public function testGetInfosFromSearch()
    {
        $siretData = $this->checkSIRETService->getInfosFromText('Framasoft');
        $etablissement = $siretData[0];
        $this->assertEquals('FRAMASOFT', $etablissement->getName());
        $this->assertEquals('2003', $etablissement->getCreationYear());
    }
}
