<?php

namespace App\Tests\Services;

use App\Services\OrganizationLookup\CheckRNAService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class CheckRNAServiceTest extends KernelTestCase
{
    private CheckRNAService $checkRNAService;

    protected function setUp(): void
    {
        parent::setUp();
        $container = static::getContainer();
        /** @var CheckRNAService $checkRNAService */

        $buildResponse = function (string $method, string $url, $options): MockResponse {
            $fileName = match ($url) {
                "https://entreprise.data.gouv.fr/api/rna/v1/id/W751179246" => "FRAMASOFT",
                "https://entreprise.data.gouv.fr/api/rna/v1/siret/48077929700013" => "PLOUAY",
                "https://entreprise.data.gouv.fr/api/rna/v1/full_text/April%20promotion%20informatique%20libre" => "Search",
            };
            $json = file_get_contents(__DIR__ . '/../Fixtures/Services/RNA/' . $fileName . '.json');
            $fileData = json_decode($json, true);
            return new MockResponse($fileData['content'], ['http_code' => $fileData['statusCode']]);
        };

        $this->checkRNAService = new CheckRNAService(new MockHttpClient($buildResponse));
    }

    public function testGetInfosFromRNA()
    {
        self::bootKernel();

        $rnaData = $this->checkRNAService->getInfosFromRNA('W751179246');
        $this->assertEquals('FRAMASOFT', $rnaData['association']['titre']);
        $this->assertEquals('2003-12-02', $rnaData['association']['date_creation']);

        $this->assertEquals($this->checkRNAService->getInfosFromRNA('W751179246'), $this->checkRNAService->getInfosFromText('W751179246'));
    }

    public function testGetInfosFromSIRET()
    {
        $siretData = $this->checkRNAService->getInfosFromSiret('48077929700013');
        $this->assertEquals('ASSOCIATION DE FORMATION AU SECOURISME DU PAYS DE PLOUAY', $siretData['association']['titre']);
        $this->assertEquals('2004-07-05', $siretData['association']['date_creation']);

        $this->assertEquals($this->checkRNAService->getInfosFromSiret('48077929700013'), $this->checkRNAService->getInfosFromText('48077929700013'));
    }

    public function testGetInfosFromSearch()
    {
        self::bootKernel();

        $organizations = $this->checkRNAService->getInfosFromSearch('April promotion informatique libre');
        $this->assertEquals('APRIL ASSOCIATION POUR LA PROMOTION ET LA RECHERCHE EN INFORMATIQUE LIBRE', $organizations[0]->getName());
        $this->assertEquals('1996', $organizations[0]->getCreationYear());

        $this->assertEquals($this->checkRNAService->getInfosFromSearch('April promotion informatique libre'), $this->checkRNAService->getInfosFromText('April promotion informatique libre'));
    }
}
