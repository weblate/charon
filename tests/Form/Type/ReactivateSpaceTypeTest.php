<?php

namespace App\Tests\Form\Type;

use App\Form\Type\DeleteSpaceType;
use App\Form\Type\ReactivateSpaceType;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\Forms;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\Validator\Validation;

class ReactivateSpaceTypeTest extends TypeTestCase
{
    /**
     * @dataProvider dataForTestSubmitValidData
     */
    public function testSubmitValidData(array $formData, bool $valid)
    {
        $validator = Validation::createValidator();

        $formFactory = Forms::createFormFactoryBuilder()
        ->addExtension(new ValidatorExtension($validator))
        ->getFormFactory();

        // $model will retrieve data from the form submission; pass it as the second argument
        $form = $formFactory->create(ReactivateSpaceType::class, []);

        // submit the data to the form directly
        $form->submit($formData);

        // This check ensures there are no transformation failures
        $this->assertTrue($form->isSynchronized());

        $this->assertEquals($valid, $form->isValid());
    }

    public function dataForTestSubmitValidData(): array
    {
        return [
            [
                [
                    'confirm' => true,
                ],
                true
            ],
            [
                [
                    'confirm' => false,
                ],
                false
            ],
        ];
    }
}
