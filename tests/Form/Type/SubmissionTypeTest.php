<?php

namespace App\Tests\Form\Type;

use App\Entity\Submission;
use App\Form\Type\SubmissionType;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\Forms;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\Validator\Validation;

class SubmissionTypeTest extends TypeTestCase
{
    /**
     * @dataProvider dataForTestSubmitValidData
     */
    public function testSubmitValidData(array $formData, bool $valid, bool $equals, string $errorString = '')
    {
        $validator = Validation::createValidator();

        $formFactory = Forms::createFormFactoryBuilder()
        ->addExtension(new ValidatorExtension($validator))
        ->addExtensions($this->getExtensions())
        ->getFormFactory();

        $now = new \DateTimeImmutable();

        $model = new Submission();
        $model->setCreatedAt($now);
        $model->setUpdatedAt($now);

        // $model will retrieve data from the form submission; pass it as the second argument
        $form = $formFactory->create(SubmissionType::class, $model);

        $expected = new Submission();
        $expected->setIdentity($formData['identity']);
        $expected->setEmail($formData['email']);
        $expected->setResponsible($formData['responsible']);
        $expected->setAlreadyUsed($formData['alreadyUsed']);
        $expected->setAccountsNeeded($formData['accountsNeeded']);
        $expected->setName($formData['name']);
        $expected->setDomain($formData['domain']);
        $expected->setType($formData['type']);
        $expected->setCreatedAt($now);
        $expected->setUpdatedAt($now);

        // submit the data to the form directly
        $form->submit($formData);

        // This check ensures there are no transformation failures
        $this->assertTrue($form->isSynchronized());

        $this->assertTrue($form->isSubmitted());
        $this->assertEquals($errorString, trim($form->getErrors(true)->__toString()));
        $this->assertEquals($valid, $form->isValid());
        if ($equals) {
            $this->assertEquals($expected, $model);
        } else {
            $this->assertNotEquals($expected, $model);
        }
    }

    public function dataForTestSubmitValidData(): array
    {
        return [
            [
                [
                    'identity' => 'hello',
                    'email' => 'someone@somewhere.com',
                    'responsible' => Submission::SUBMISSION_NOT_RESPONSIBLE,
                    'alreadyUsed' => [Submission::SUBMISSION_ALREADY_USED_AS_USER],
                    'accountsNeeded' => '21-30',
                    'name' => 'Some organisation',
                    'domain' => 'some-org',
                    'type' => Submission::SUBMISSION_TYPE_ASSOCIATION_1901,
                    'acceptContact' => true,
                    'betaWarning' => true,
                    'acceptCGU' => true,
                    'acceptCSU' => true,
                ],
                true,
                true,
            ],
            [
                [
                    'identity' => 'hello',
                    'email' => 'someone@somewhere.com',
                    'responsible' => Submission::SUBMISSION_NOT_RESPONSIBLE,
                    'alreadyUsed' => [],
                    'accountsNeeded' => '21-30',
                    'name' => 'Some organisation',
                    'domain' => 'www',
                    'type' => 'something else',
                    'acceptContact' => true,
                    'betaWarning' => true,
                    'acceptCGU' => true,
                    'acceptCSU' => true,
                ],
                false,
                false,
                'ERROR: The selected choice is invalid.'
            ],
            [
                [
                    'identity' => 'hello',
                    'email' => 'someone@somewhere.com',
                    'responsible' => Submission::SUBMISSION_NOT_RESPONSIBLE,
                    'alreadyUsed' => [Submission::SUBMISSION_ALREADY_USED_AS_USER],
                    'accountsNeeded' => '21-30',
                    'name' => 'Some organisation',
                    'domain' => 'some-org',
                    'type' => Submission::SUBMISSION_TYPE_ASSOCIATION_1901,
                    'acceptContact' => true,
                    'betaWarning' => false,
                    'acceptCGU' => true,
                    'acceptCSU' => true,
                ],
                false,
                true,
                'ERROR: This value should be true.'
            ]
        ];
    }
}
