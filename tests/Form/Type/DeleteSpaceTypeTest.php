<?php

namespace App\Tests\Form\Type;

use App\Form\Type\DeleteSpaceType;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\Forms;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\Validator\Validation;

class DeleteSpaceTypeTest extends TypeTestCase
{
    /**
     * @dataProvider dataForTestSubmitValidData
     */
    public function testSubmitValidData(array $formData, bool $valid)
    {
        $validator = Validation::createValidator();

        $formFactory = Forms::createFormFactoryBuilder()
        ->addExtension(new ValidatorExtension($validator))
        ->getFormFactory();

        // $model will retrieve data from the form submission; pass it as the second argument
        $form = $formFactory->create(DeleteSpaceType::class, [], ['subdomain_compare' => 'a-domain']);

        // submit the data to the form directly
        $form->submit($formData);

        // This check ensures there are no transformation failures
        $this->assertTrue($form->isSynchronized());

        $this->assertEquals($valid, $form->isValid());
    }

    public function dataForTestSubmitValidData(): array
    {
        return [
            [
                [
                    'understoodConsequences' => true,
                    'madeBackups' => true,
                    'disconnectedSyncClients' => true,
                    'subdomain' => 'a-domain',
                ],
                true
            ],
            [
                [
                    'understoodConsequences' => false,
                    'madeBackups' => true,
                    'disconnectedSyncClients' => true,
                    'subdomain' => 'a-domain',
                ],
                false
            ],
            [
                [
                    'understoodConsequences' => true,
                    'madeBackups' => true,
                    'disconnectedSyncClients' => true,
                    'subdomain' => 'not-a-domain',
                ],
                false
            ]
        ];
    }
}
