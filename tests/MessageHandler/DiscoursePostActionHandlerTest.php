<?php

namespace App\Tests\MessageHandler;

use App\Factory\SubmissionFactory;
use App\Message\DiscoursePostAction;
use App\MessageHandler\DiscoursePostActionHandler;
use App\Repository\SubmissionRepository;
use App\Services\DiscourseService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DiscoursePostActionHandlerTest extends KernelTestCase
{
    public function testHandleDiscoursePostAction(): void
    {
        self::bootKernel();

        $submission = SubmissionFactory::createOne();

        $submissionRepository = $this->createMock(SubmissionRepository::class);
        $submissionRepository->expects($this->once())->method('find')->with($submission->object()->getId())->willReturn($submission->object());
        $discourseService = $this->createMock(DiscourseService::class);
        $discourseService->expects($this->once())->method('postNewSubmission')->with($submission->object())->willReturn(true);
        $discoursePostActionHandler = new DiscoursePostActionHandler($submissionRepository, $discourseService);
        $discoursePostActionHandler(new DiscoursePostAction($submission->object()->getId()));
    }
}
