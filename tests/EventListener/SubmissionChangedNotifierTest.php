<?php

namespace App\Tests\EventListener;

use App\EventListener\SubmissionChangedListener;
use App\Events\SubmissionCreatedEvent;
use App\Factory\SubmissionFactory;
use App\Services\AdminUrlGenerator;
use App\Services\Notifications\Mailer;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class SubmissionChangedNotifierTest extends KernelTestCase
{
    public function testPostUpdate(): void
    {
        self::bootKernel();
        $submission = SubmissionFactory::createOne();

        $mailer = $this->createMock(Mailer::class);
        $mailer->expects($this->once())->method('sendPendingEmail')->with($submission->object());
        $mailer->expects($this->once())->method('sendLoggingMail');
        $adminURLGenerator = $this->createMock(AdminUrlGenerator::class);
        $adminURLGenerator->expects($this->once())->method('backendAdminURL')->with($submission->object());

        $jobChangedNotifier = new SubmissionChangedListener($mailer, $adminURLGenerator);
        $jobChangedNotifier->handleSubmissionCreated(new SubmissionCreatedEvent($submission->object()));
    }
}
