<?php

namespace App\Tests\EventListener;

use App\Entity\Job;
use App\EventListener\JobChangedNotifier;
use App\Factory\JobFactory;
use App\SpaceManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class JobChangedNotifierTest extends KernelTestCase
{
    /**
     * @dataProvider postUpdateData
     */
    public function testPostUpdate(JobFactory $jobFactory, ?string $method): void
    {
        self::bootKernel();
        $job = $jobFactory->create();

        $spaceManager = $this->createMock(SpaceManager::class);
        if ($method) {
            $spaceManager->expects($this->once())->method($method)->with($job->object()->getSpace());
        } else {
            $spaceManager->expects($this->never())->method($this->anything());
        }
        $jobChangedNotifier = new JobChangedNotifier($spaceManager);
        $jobChangedNotifier->postUpdate($job->object());
    }

    public static function postUpdateData(): iterable
    {
        return [
            yield [JobFactory::new(['type' => Job::JOB_TYPE_DELETE_SPACE, 'status' => Job::JOB_STATUS_DONE]), 'deleteSpace'],
            yield [JobFactory::new(['type' => Job::JOB_TYPE_DISABLE_SPACE, 'status' => Job::JOB_STATUS_DONE]), 'postDisableSpace'],
            yield [JobFactory::new(['type' => Job::JOB_TYPE_REENABLE_SPACE, 'status' => Job::JOB_STATUS_DONE]), 'postReenableSpace'],
            yield [JobFactory::new(['type' => Job::JOB_TYPE_CHANGE_OFFICE, 'status' => Job::JOB_STATUS_DONE]), 'postChangeOfficeType'],
            yield [JobFactory::new(['type' => Job::JOB_TYPE_CHANGE_OFFICE, 'status' => Job::JOB_STATUS_ERROR]), null]
        ];
    }
}
