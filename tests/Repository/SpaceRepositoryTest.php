<?php

namespace App\Tests\Repository;

use App\Entity\Space;
use App\Factory\SpaceFactory;
use App\Repository\SpaceRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Zenstruck\Foundry\Proxy;

use function Zenstruck\Foundry\faker;

class SpaceRepositoryTest extends KernelTestCase
{
    public function testFindBiggestNcHpBackendPort(): void
    {
        self::bootKernel();
        $container = static::getContainer();
        $spaceRepository = $container->get(SpaceRepository::class);

        $this->assertEquals(10000, $spaceRepository->findBiggestNcHpBackendPort(), 'Biggest NCHPBackend port is not what expected');

        SpaceFactory::createOne(['details' => ['nc_hp_backend' => 42000, 'office_port' => 12500, 'office_server' => 'office-2']]);

        $this->assertEquals(42000, $spaceRepository->findBiggestNcHpBackendPort(), 'Biggest NCHPBackend port is not what expected');
    }

    public function testFindBiggestOfficePort(): void
    {
        self::bootKernel();
        $container = static::getContainer();
        $spaceRepository = $container->get(SpaceRepository::class);

        $this->assertEquals(10000, $spaceRepository->findBiggestPortForOfficeServer('office-1'), 'Biggest NCHPBackend port is not what expected');

        SpaceFactory::createOne(['details' => ['nc_hp_backend' => 42000, 'office_port' => 12500, 'office_server' => 'office-2']]);

        $this->assertEquals(10000, $spaceRepository->findBiggestPortForOfficeServer('office-1'), 'Biggest NCHPBackend port is not what expected');

        $this->assertEquals(12500, $spaceRepository->findBiggestPortForOfficeServer('office-2'), 'Biggest NCHPBackend port is not what expected');
    }

    public function testCountNewSpacesByMonth(): void
    {
        self::bootKernel();
        $container = static::getContainer();
        $spaceRepository = $container->get(SpaceRepository::class);

        $this->assertEmpty($spaceRepository->countNewSpacesByMonths());

        SpaceFactory::createMany(5, ['status' => Space::SPACE_STATUS_CREATED, 'enabled' => true]);
        SpaceFactory::new(['status' => Space::SPACE_STATUS_CREATED, 'enabled' => true])
        ->withAttributes(function () {
            return ['createdAt' => \DateTimeImmutable::createFromMutable(new \DateTime('2020-07-06'))];
        })
        /**
         * createdAt is always instantiated to current date, so we need to override it
         */
        ->afterPersist(function (Proxy $proxy, array $attributes) {
            /** @var Space $proxy */
            $proxy->setCreatedAt($attributes['createdAt']);
        })
        ->create();
        SpaceFactory::createMany(5, ['status' => Space::SPACE_STATUS_PENDING, 'enabled' => false]);

        $results = $spaceRepository->countNewSpacesByMonths();

        $this->assertEquals(1, array_values(array_filter($results, fn ($result) => $result['createdAtMonth'] === '2020-07-01 00:00:00'))[0]['spaceCount']);
        $this->assertEquals(5, array_values(array_filter($results, fn ($result) => $result['createdAtMonth'] === date('Y-m-01 00:00:00')))[0]['spaceCount']);
    }

    public function testMesureOfficeTypeDistribution(): void
    {
        self::bootKernel();
        $container = static::getContainer();
        $spaceRepository = $container->get(SpaceRepository::class);

        $this->assertEmpty($spaceRepository->mesureOfficeTypeDistribution());

        SpaceFactory::createMany(5, ['status' => Space::SPACE_STATUS_CREATED, 'enabled' => true, 'office' => Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE]);
        SpaceFactory::createMany(3, ['status' => Space::SPACE_STATUS_CREATED, 'enabled' => true, 'office' => Space::SPACE_OFFICE_TYPE_ONLYOFFICE]);

        $this->assertEquals([['spaceCount' => 5, 'office' => Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE], ['spaceCount' => 3, 'office' => Space::SPACE_OFFICE_TYPE_ONLYOFFICE]], $spaceRepository->mesureOfficeTypeDistribution());
    }
}
