<?php

namespace App\Tests\Repository;

use App\Entity\Submission;
use App\Factory\SubmissionFactory;
use App\Repository\SubmissionRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class SubmissionRepositoryTest extends KernelTestCase
{
    public function testCountApprovedSubmissionsByType(): void
    {
        self::bootKernel();
        $container = static::getContainer();
        $submissionRepository = $container->get(SubmissionRepository::class);

        $this->assertEquals([], $submissionRepository->countApprovedSubmissionsByType());

        SubmissionFactory::createOne(['status' => Submission::SUBMISSION_STATUS_REJECTED, 'type' => Submission::SUBMISSION_TYPE_ENTREPRISE]);
        SubmissionFactory::createOne(['status' => Submission::SUBMISSION_STATUS_PENDING, 'type' => Submission::SUBMISSION_TYPE_SCOP]);

        $this->assertEquals([], $submissionRepository->countApprovedSubmissionsByType());

        SubmissionFactory::createOne(['status' => Submission::SUBMISSION_STATUS_APPROVED, 'type' => Submission::SUBMISSION_TYPE_ASSOCIATION_1901]);
        SubmissionFactory::createOne(['status' => Submission::SUBMISSION_STATUS_APPROVED, 'type' => Submission::SUBMISSION_TYPE_ASSOCIATION_1901]);
        SubmissionFactory::createOne(['status' => Submission::SUBMISSION_STATUS_APPROVED, 'type' => Submission::SUBMISSION_TYPE_SYNDICATE]);

        $this->assertEquals([['submissionCount' => 2, 'type' => Submission::SUBMISSION_TYPE_ASSOCIATION_1901], ['submissionCount' => 1, 'type' => Submission::SUBMISSION_TYPE_SYNDICATE]], $submissionRepository->countApprovedSubmissionsByType());
    }
}
