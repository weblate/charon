<?php

namespace App\Tests\Repository;

use App\Repository\JobRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class JobRepositoryTest extends KernelTestCase
{
    public function testFindAllQueryWithFilterQueryBuilder(): void
    {
        self::bootKernel();
        $container = static::getContainer();
        $jobRepository = $container->get(JobRepository::class);

        $this->assertInstanceOf(QueryBuilder::class, $jobRepository->findAllQueryWithFilterQueryBuilder());
    }
}
