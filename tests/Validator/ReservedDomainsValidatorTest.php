<?php

namespace App\Tests\Validator;

use App\Validator\ReservedDomains;
use App\Validator\ReservedDomainsValidator;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Constraints\Positive;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;
use Symfony\Component\Validator\Violation\ConstraintViolationBuilderInterface;

class ReservedDomainsValidatorTest extends KernelTestCase
{
    public function testValidateWithOtherValidation(): void
    {
        self::bootKernel();
        $this->expectException(UnexpectedTypeException::class);
        $this->getValidator(false)->validate(3, new Positive());
    }

    public function testValidateWithBadTypeValue(): void
    {
        self::bootKernel();
        $this->expectException(UnexpectedValueException::class);
        $this->getValidator(false)->validate(3, new ReservedDomains());
    }

    /**
     * @dataProvider dataForTestValidate
     */
    public function testValidate(?string $value, bool $validates): void
    {
        self::bootKernel();
        $this->getValidator(!$validates)->validate($value, new ReservedDomains());
    }

    public function dataForTestValidate(): array
    {
        return [
            // First two do not send anything because it's caught by something else
            [null, true],
            ['', true],
            ['s3-api', false],
            ['office-7', false],
            ['hello', true],
            ['office-online', true]
        ];
    }

    /**
     * Instancie un validateur pour nos tests
     **/
    private function getValidator(bool $expectedViolation): ReservedDomainsValidator
    {
        $validator = new ReservedDomainsValidator();
        $context = $this->getContext($expectedViolation);
        $validator->initialize($context);
        return $validator;
    }

    /**
     * Génère un contexte qui attend (ou non) une violation
     **/
    private function getContext(bool $expectedViolation): ExecutionContextInterface
    {
        $context = $this->getMockBuilder(ExecutionContextInterface::class)->getMock();
        if ($expectedViolation) {
            $violation = $this->getMockBuilder(ConstraintViolationBuilderInterface::class)->getMock();
            $violation->expects($this->any())->method('setParameter')->willReturn($violation);
            $violation->expects($this->once())->method('addViolation');
            $context
                ->expects($this->once())
                ->method('buildViolation')
                ->willReturn($violation);
        } else {
            $context
                ->expects($this->never())
                ->method('buildViolation');
        }
        return $context;
    }
}
