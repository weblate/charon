/* eslint-disable no-irregular-whitespace */
/* eslint-disable object-shorthand */

const app = document.createElement('div');
app.id = 'app';
document.getElementsByName('submission')[0].insertAdjacentElement('beforebegin', app);

const { createApp } = Vue; // eslint-disable-line

createApp({
    data() {
        const labels = {};
        const placeholders = {};
        const topics = [];
        document.querySelectorAll('form[name="submission"] label').forEach((label) => {
            if (label.hasAttribute('for')) {
                labels[label.attributes.for.nodeValue.replace('submission_', '')] = label.innerHTML;
            }
        });
        document.querySelectorAll('form[name="submission"] input[id^="submission_topics_"]').forEach((input) => {
            topics.push(input.attributes.value.nodeValue);
        });
        topics.pop(); // remove « Other »

        document.querySelectorAll('form[name="submission"] input').forEach((input) => {
            if (input.hasAttribute('placeholder')) {
                placeholders[input.attributes.id.nodeValue.replace('submission_', '')] = input.attributes.placeholder.nodeValue;
            }
        });

        labels.manager = 'Je suis un⋅e des responsables de l’organisation';
        labels.nextcloud = 'J’ai déjà utilisé le logiciel libre Nextcloud';
        labels.nbFSAccount = 'Nombre estimé de comptes Framaspace nécessaire';
        labels.identifier = 'Numéro d’identification (SIREN / RNA)';
        labels.country = 'Pays de l’organisation';
        labels.topics = ' Domaines d’intervention';
        labels.topics_others = 'Autre';

        placeholders.identifier = 'ex: « 500715776 » ou « W751179246 »';
        placeholders.name = 'ex: « Framasoft » ou « Squat de la rue Lepic »';
        placeholders.creationYear = 'ex: « 2001 »';
        placeholders.topics_others = 'ex: « protection civile, finance, aide juridique, ergonomie,… »';
        placeholders.reasons = 'ex: « En tant que responsable de mon organisation, je souhaite ouvrir un compte Framaspace parce que… »';
        placeholders.website = 'ex: « framasoft.org »';

        return {
            form: {
                /* Admin */
                email: '', /* Email (email) */
                identity: '', /* Nom ou pseudo (text) */
                manager: '', /* Responsable de l’orga (radio) */
                nextcloud: ['', '', '', ''], /* Utilisateur·rice de NX (checkbox) */
                nbFSAccount: '', /* Nb estimé de compte FS (select) */
                /* Orga */
                identifier: '', /* RNA/SIREN (RNA = W[A-Z0-9]{9} ; SIREN = [0-9]{8}) */
                name: '', /* Nom de l’orga (name in API) */
                domain: '', /* Lien du FSpace (text) */
                type: 'collective', /* Type de l’orga (select) */
                creationYear: '', /* Année de création de l’orga (creationYear in API) */
                country: 'FR', /* Pays */
                object: '', /* Objet social ou mission (object in API) */
                mainActions: '', /* Actions principales (text) */
                /* Meet */
                topics: [], /* Domaines d’intervention (array of bool) */
                topics_others: '', /* Autres domaines (text) */
                reasons: '', /* Raisons du besoin de FSpace */
                website: '', /* Site web (text) */
                nbEmployees: '', /* Nb de salarié·es (int) */
                nbMembers: '', /* Nb de membres/adhérent·es (int) */
                nbBeneficiaries: '', /* Nb (approximatif) de bénéficiaires (int) */
                budget: '' /* Budget (approximatif) annuel de l’orga */,
                /* Frama */
                acceptContact: '', /* Recontact admin */
                betaWarning: '', /* Recontact beta */
                acceptLaw: '', /* CGU */
                acceptCSU: '', /* CSU */
                /* Submit */
                token: document.getElementById('submission__token').value,
            },
            icons: {
                shield: '<svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><!--! Font Awesome Pro 6.2.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc.--><path d="M256 0c-4.6 0-9.2 1-13.3 2.9L54.3 82.8c-22 9.3-38.4 31-38.3 57.2.5 99.2 41.3 280.7 213.5 363.2 16.7 8 36.1 8 52.8 0C454.7 420.7 495.5 239.2 496 140c.1-26.2-16.3-47.9-38.3-57.2L269.4 2.9C265.2 1 260.6 0 256 0zm-17.418 127.992h51.1c39.9 0 70.9 31 70 70 0 24-13 47-34 60l-45.1 28v2c0 13-11 24-24 24s-24-11-24-24v-16c0-8 4-16 12-21l57-34.9c7-4 11-10.1 11-18.1 0-12-10.9-22-23.8-22h-51.1c-12 0-21.1 10-21.1 22 0 13-11 24-24 24s-24-11-24-24c0-39 31-70 70-70zm18 208c17.1 0 32 14 32 32s-14.9 32-32 32c-18.9 0-32-14-32-32s14-32 32-32z"/></svg>',
                user: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><!--! Font Awesome Pro 6.2.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path d="M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0S96 57.3 96 128s57.3 128 128 128zm-45.7 48C79.8 304 0 383.8 0 482.3C0 498.7 13.3 512 29.7 512H418.3c16.4 0 29.7-13.3 29.7-29.7C448 383.8 368.2 304 269.7 304H178.3z"/></svg>',
                orga: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!--! Font Awesome Pro 6.2.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path d="M243.4 2.6l-224 96c-14 6-21.8 21-18.7 35.8S16.8 160 32 160v8c0 13.3 10.7 24 24 24H456c13.3 0 24-10.7 24-24v-8c15.2 0 28.3-10.7 31.3-25.6s-4.8-29.9-18.7-35.8l-224-96c-8.1-3.4-17.2-3.4-25.2 0zM128 224H64V420.3c-.6 .3-1.2 .7-1.8 1.1l-48 32c-11.7 7.8-17 22.4-12.9 35.9S17.9 512 32 512H480c14.1 0 26.5-9.2 30.6-22.7s-1.1-28.1-12.9-35.9l-48-32c-.6-.4-1.2-.7-1.8-1.1V224H384V416H344V224H280V416H232V224H168V416H128V224zm128-96c-17.7 0-32-14.3-32-32s14.3-32 32-32s32 14.3 32 32s-14.3 32-32 32z"/></svg>',
                coffee: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><!--! Font Awesome Pro 6.2.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path d="M96 64c0-17.7 14.3-32 32-32H448h64c70.7 0 128 57.3 128 128s-57.3 128-128 128H480c0 53-43 96-96 96H192c-53 0-96-43-96-96V64zM480 224h32c35.3 0 64-28.7 64-64s-28.7-64-64-64H480V224zM32 416H544c17.7 0 32 14.3 32 32s-14.3 32-32 32H32c-17.7 0-32-14.3-32-32s14.3-32 32-32z"/></svg>',
                handshake: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><!--! Font Awesome Pro 6.2.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path d="M323.4 85.2l-96.8 78.4c-16.1 13-19.2 36.4-7 53.1c12.9 17.8 38 21.3 55.3 7.8l99.3-77.2c7-5.4 17-4.2 22.5 2.8s4.2 17-2.8 22.5l-20.9 16.2L550.2 352H592c26.5 0 48-21.5 48-48V176c0-26.5-21.5-48-48-48H516h-4-.7l-3.9-2.5L434.8 79c-15.3-9.8-33.2-15-51.4-15c-21.8 0-43 7.5-60 21.2zm22.8 124.4l-51.7 40.2C263 274.4 217.3 268 193.7 235.6c-22.2-30.5-16.6-73.1 12.7-96.8l83.2-67.3c-11.6-4.9-24.1-7.4-36.8-7.4C234 64 215.7 69.6 200 80l-72 48H48c-26.5 0-48 21.5-48 48V304c0 26.5 21.5 48 48 48H156.2l91.4 83.4c19.6 17.9 49.9 16.5 67.8-3.1c5.5-6.1 9.2-13.2 11.1-20.6l17 15.6c19.5 17.9 49.9 16.6 67.8-2.9c4.5-4.9 7.8-10.6 9.9-16.5c19.4 13 45.8 10.3 62.1-7.5c17.9-19.5 16.6-49.9-2.9-67.8l-134.2-123z"/></svg>',
            },
            labels,
            placeholders,
            status: {
                help: {
                    domain: false,
                },
            },
            topics,
            txt: {
                data: {
                    info: 'Information sur la collecte des données', // btn visually-hidden
                    why: 'Pourquoi avons-nous besoin de cette information ?', // popover title
                    identity: `Ce nom ou pseudo pourra être utilisé, lorsque nous vous contactons,
            ou pour nous aider à identifier des doublons ou autre raison.`,
                    email: `<ol>
            <li>pour vous recontacter pour vous informer de l’ouverture de votre compte</li>
            <li>pour vous envoyer des informations techniques utiles au bon fonctionnement du service</li>
            <li>pour échanger avec vous (par exemple en cas d’abus)</li>
          </ol>`,
                    manager: `Pour éviter les doublons et les litiges, nous souhaitons que
            les personnes qui demandent l’ouverture d'un framaspace aient un rôle
            représentatif dans l’organisation.`,
                    nextcloud: `Dans la phase de bêta test, nous souhaitons répartir les
            organisations entre celles dont les membres disposent déjà d’une
            expérience avec Nextcloud, et d’autres non.`,
                    nbFSAccount: `Framaspace n’est pas adapté pour de grosses organisations.
            Nous préférons connaître l’estimation de votre usage en amont,
            plutôt que vous décevoir en aval.`,
                    identifier: `Cela permettra de préremplir automatiquement une partie des champs ci-dessous.
            Si vous ne les connaissez pas, vous pouvez les trouver 🏗️ici🏗️.
            Si vous n'en possédez pas, cela diminue vos chances de voir votre
            préinscription validée.`,
                    name: `Le nom de votre organisation nous permet évidemment de vous
            identifier rapidement, mais servira aussi de base pour déterminer
            « l’identifiant framaspace » de votre compte
            (ex: « https://squat_lepic.frama.space »)`,
                    domain: `Il servira à déterminer l’adresse de votre framaspace.
            Par exemple, l’identifiant « robinsdesvilles » servira à la fois à
            vous identifier sur votre framaspace 🤔il me semble que l’user par défaut c’est juste « admin »🤔, mais sera aussi l’adresse de
            votre framaspace (https://robinsdesvilles.frama.space)`,
                    type: `Nous avons besoin de cette information à la fois à des fins
            statistiques, mais aussi parce que, dans sa phase beta, Frama.space
            est reservé à certains types d’organisations.`,
                    country: `Nous avons besoin de cette information à la fois à des fins
            statistiques, mais aussi parce que, dans sa phase beta, Frama.space
            est reservé aux organisations francophones.`,
                    object: `Elle nous servira à déterminer si vous rentrez dans le
            cadre de notre phase de « beta test ».`,
                    mainActions: `L’objet social peut être parfois très vague (« Association
            de théâtre »). Toujours dans le but de mieux vous connaître,
            nous pourrions avoir envie d'en savoir plus sur vos actions.
            Qui elles touchent ? Dans quels buts ? Avec quelles valeurs ?`,
                    topics: `Essentiellement à des fins statistiques, pour nous permettre
            de nous assurer que différents champs d’intervention sont bien
            soutenus par l’initiative framaspace.`,
                },
                help: {
                    domain: `L’identifiant doit être en minuscule et ne contenir aucun
            espace, accent ou caractère spécial.`,
                    object: `Faites l’effort de donner suffisamment de détails
            (« Association culturelle », c’est un peu léger par exemple),
            mais tout en restant concis (pas la peine de faire un roman).
            Pour rappel, l’objet social est souvent indiqué dans les statuts
            d’une association.`,
                    mainActions: `Indiquez ici, avec vos propres mots, les actions
            principales de l’organisation.`,
                    topics: `Vous pouvez indiquez un ou plusieurs domaines d’intervention
            de votre structure.`,
                    nbBeneficiaries: `Selon vous, votre organisation « touche » combien de
            personnes chaque année ? NB : ce critère n’est pas discriminatoire.
            Nous pouvons parfaitement sélectionner pour la phase de test de Framasoft
            des structures en formation qui ne touchent pas encore de public.`,
                    nbEmployees: `Indiquez le nombre (approximatif) de salariés en
            Équivalent Temps Plein.`,
                    cgu: `Nos Conditions Générales d’Utilisations se trouvent à l’adresse
            suivante : <a href="https://framasoft.org/fr/cgu/">framasoft.org/cgu/</a>.
            Si vous ne respectez pas la loi, nous nous réservons évidemment le
            droit de suspendre votre compte.`,
                    csu: `Nos Conditions Spécifiques d’Utilisations se trouvent à l’adresse
            suivante : <a href="https://mypads.framapad.org/p/conditions-specifiques-d-utilisation-l548w762">frama.space/csu/</a>.
            Si vous ne respectez pas ces conditions, nous nous réservons évidemment
            le droit de suspendre ou supprimer votre compte.`,
                },
                warning: {
                    email: `<p><strong>Attention, ce champ est très important !</strong>
            Nous vous enverrons un courriel de confirmation dès la fin de votre
            préinscription. Vérifiez qu’il arrive bien dans votre boite mail
            <strong>pour valider votre demande</strong>.</p>
            <p>Si nous validons votre pré-inscription, vous pourrez ensuite
            modifier cette adresse mail.</p>`,
                    nbFSAccount: // if nb > 51
                        `<strong>Attention :</strong> Framaspace n’est pas
            adapté pour de grosses organisations.`,
                    type: // only for collectives, assos and syndicates
                        `Nous n’ouvrons pas de compte Framaspace pour ce type d’organisation pour le moment.
            <a href="#faq">Voir notre FAQ pour plus d’information.</a>`,
                    country: // only for french speakers
                        `Nous n’ouvrons pas de compte Framaspace pour les pays non francophones pour le moment.
            <a href="#faq">Voir notre FAQ pour plus d’information.</a>`,

                },
            },
        };
    },

    watch: {
        'form.domain'(newValue, oldValue) {
            // Show help if needed
            if (!this.status.help.domain && !/^([a-z0-9-]+)$/.test(newValue)) {
                this.status.help.domain = true;
            }
            if (newValue === '') {
                this.status.help.domain = false;
            }
            // Self-cleaning
            if (oldValue !== newValue) {
                this.form.domain = this.sanitize(newValue);
            }
        },

        'form.name'(newValue, oldValue) {
            // idFs pre-filled if null or domain ≈ old name
            if (this.form.domain === ''
                || this.form.domain === this.sanitize(oldValue)) {
                this.form.domain = this.sanitize(newValue);
            }
        },
    },

    mounted() {
        // Infos data/gpdr popover
        const popoverTriggerList = document.querySelectorAll('.btn-link[data-bs-content]');
        [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl, { // eslint-disable-line
            html: true,
            placement: 'left',
            toggle: 'popover',
            trigger: 'hover focus',
        }));
    },

    methods: {
        sanitize(text) {
            return text.toLowerCase().trim()
                // TODO replace diacritics
                .replace(/[ '’._]/g, '-')
                .replace(/[^a-z0-9-]/g, '');
        },
    },

    template: `
    <div id="intro" class="row mx-0 my-4 p-0">
      <h2><span class="badge rounded-pill bg-secondary">1</span> Prérequis</h2>
      <hr />
      <div class="col-lg-8 mx-auto">
        <p>
          Framaspace est un espace <strong>dédié aux collectifs, associations, syndicats,</strong> etc,
          qui agissent pour <strong>changer le monde</strong>. Nous n'ouvrirons donc pas de
          compte framaspace pour des particuliers.
        </p>
        <p>Ce formulaire vous permettra d’effectuer une demande de création de Framaspace pour votre organisation.</p>
        <p>Attention :</p>
        <ul>
          <li>
            <p>
              <strong>Framaspace est en phase de bêta test.</strong>
              Cela signifie que votre pré-inscription ne nous engage pas à vous créer un compte.
            </p>
            <p>
              En effet, nous souhaitons « monter en puissance » sur plusieurs années.
              Nous allons donc effectuer une sélection « manuelle » (et humaine)
              des structures qui seront les pionnières sur Framaspace.
            </p>
            <p>
              Être une structure pionnière n’est pas nécessairement un avantage !
              Les ressources d’accompagnement (tutoriels, vidéos, formations, etc)
              seront mises en place au fur et à mesure.
              Les premiers arrivés seront donc plongés dans le grand bain,
              sans bouée, avec essentiellement <a href="#forum">le forum</a> comme
              possibilité d’entraide pour les membres inscrits à Framaspace.
            </p>
            <p>
              <strong>Peut-être avez-vous intérêt à attendre quelques mois avant de demander
              la création d’un compte</strong> plutôt que de faire partie des structures
              qui essuieront inévitablement quelques plâtres techniques et communicationnels.
            </p>
          </li>
          <li>
            <p>
              <strong>La demande de création d’un espace ne signifie pas que
              Framasoft vous ouvrira un compte.</strong>
              Framaspace est réservé à certains types de publics, et non à toutes et tous !
            </p>
            <p>
              C’est pourquoi nous avons besoin d’en savoir plus sur la structure
              qui utilisera ce Framaspace.
            </p>

          </li>
          <li>
            <strong>Le service Framaspace n’est pas payant (0€),
            mais cela ne signifie pas qu’il soit gratuit.</strong>
          </li>
        </ul>
        <p>
          <a href="#faq">Prenez le temps nécessaire pour lire la FAQ</a> et les informations
          concernant <a href="#personal-data">la vie privée</a>, avant de vous lancer dans la création
          d’un compte si vous ne voulez pas être déçu⋅e !
        </p>
      </div>
    </div>

    <div id="admin" class="row mx-0 my-4 p-0">
      <h2><span class="badge rounded-pill bg-secondary">2</span> Compte administrateur·rice</h2>
      <hr />
      <div class="row m-0 p-0">
        <div class="col-lg-4 text-muted small bg-light p-4">
          <p class="text-center">
            <span v-html="icons.user"></span>
          </p>
          <p>Le compte administrateur du framaspace pourra :</p>
          <ul>
            <li>créer de nouveaux comptes sur votre framaspace (dans les limites fixées par Framasoft)</li>
            <li>supprimer des comptes</li>
            <li>partager l’administration avec d’autres comptes</li>
            <li>choisir les applications proposées sur votre framaspace (dans les limites fixées par Framasoft)</li>
          </ul>
        </div>
        <div class="col-lg-8 ps-lg-4 px-0 py-2">
          <div class="mb-5">
            <div class="form-label d-flex justify-content-between align-items-end">
              <label
                class="required"
                for="email"
                v-html="labels.email"
              ></label>
              <button
                class="btn btn-sm btn-link"
                :data-bs-content="txt.data.email"
                :data-bs-title="txt.data.why"
              >
                <span v-html="icons.shield"></span>
                <span
                  class="visually-hidden"
                  v-html="txt.data.info"
                ></span>
              </button>
            </div>
            <input
              id="email"
              class="form-control"
              :placeholder="placeholders.email"
              required="required"
              type="text"
              v-model="form.email"
            />
            <div
              class="alert alert-warning mt-2 py-2 small"
              v-html="txt.warning.email"
            ></div>
          </div>

          <div class="mb-5">
            <div class="form-label d-flex justify-content-between align-items-end">
              <label
                class="required"
                for="identity"
                v-html="labels.identity"
              ></label>
              <button
                class="btn btn-sm btn-link"
                :data-bs-content="txt.data.identity"
                :data-bs-title="txt.data.why"
              >
                <span v-html="icons.shield"></span>
                <span
                  class="visually-hidden"
                  v-html="txt.data.info"
                ></span>
              </button>
            </div>
            <input
              id="identity"
              class="form-control"
              :placeholder="placeholders.identity"
              required="required"
              type="text"
              v-model="form.identity"
            />
          </div>

          <div class="mb-5">
            <div class="form-label d-flex justify-content-between align-items-end">
              <label v-html="labels.manager"></label>
              <button
                class="btn btn-sm btn-link"
                :data-bs-content="txt.data.manager"
                :data-bs-title="txt.data.why"
              >
                <span v-html="icons.shield"></span>
                <span
                  class="visually-hidden"
                  v-html="txt.data.info"
                ></span>
              </button>
            </div>
            <ul class="nav nav-pills nav-fill py-2 rounded">
              <li class="nav-item">
                <input
                  id="manager-yes"
                  autocomplete="off"
                  class="btn-check"
                  name="manager"
                  type="radio"
                  value="yes"
                  v-model="form.manager"
                />
                <label
                  class="btn btn-sm d-block mx-1 btn-outline-secondary"
                  for="manager-yes"
                >Oui</label>
              </li>

              <li class="nav-item">
                <input
                  id="manager-no"
                  autocomplete="off"
                  class="btn-check"
                  name="manager"
                  type="radio"
                  value="no"
                  v-model="form.manager"
                />
                <label
                  class="btn btn-sm d-block mx-1 btn-outline-secondary"
                  for="manager-no"
                >Non</label>
              </li>

              <li class="nav-item">
                <input
                  id="manager-idk"
                  autocomplete="off"
                  class="btn-check"
                  name="manager"
                  type="radio"
                  value="idk"
                  v-model="form.manager"
                />
                <label
                  class="btn btn-sm d-block mx-1 btn-outline-secondary"
                  for="manager-idk"
                >Je ne suis pas sûr⋅e</label>
              </li>
            </ul>
            <p
              v-show="/(no|idk)/.test(form.manager)"
              class="alert alert-info mt-2 py-2 small"
              v-html="txt.data.manager"
            ></p>
          </div>

          <div class="mb-5">
            <div class="form-label d-flex justify-content-between align-items-end">
              <label v-html="labels.nextcloud"></label>
              <button
                class="btn btn-sm btn-link"
                :data-bs-content="txt.data.nextcloud"
                :data-bs-title="txt.data.why"
              >
                <span v-html="icons.shield"></span>
                <span
                  class="visually-hidden"
                  v-html="txt.data.info"
                ></span>
              </button>
            </div>
            <div class="p-2 border rounded">
              <div class="form-check">
                <input
                  id="ncAdmin"
                  type="checkbox"
                  class="form-check-input"
                  v-model="form.nextcloud[0]"
                />
                <label
                  class="form-check-label"
                  for="ncAdmin"
                >En tant qu’administrateur⋅rice</label>
              </div>

              <div class="form-check">
                <input
                  id="ncUser"
                  type="checkbox"
                  class="form-check-input"
                  v-model="form.nextcloud[1]"
                />
                <label
                  class="form-check-label"
                  for="ncUser"
                >En tant qu’utilisateur⋅rice</label>
              </div>

              <div class="form-check">
                <input
                  id="ncChatons"
                  type="checkbox"
                  class="form-check-input"
                  v-model="form.nextcloud[2]"
                />
                <label
                  class="form-check-label"
                  for="ncChatons"
                >Chez un hébergeur du collectif <a href="https://chatons.org">CHATONS</a></label>
              </div>

              <div class="form-check">
                <input
                  id="ncUnknown"
                  type="checkbox"
                  class="form-check-input"
                  v-model="form.nextcloud[3]"
                />
                <label
                  class="form-check-label"
                  for="ncUnknown"
                >Je ne connais pas Nextcloud</label>
              </div>
            </div>
          </div>

          <div class="mb-5">
            <div class="form-label d-flex justify-content-between align-items-end">
              <label
                for="nbFSAccount"
                v-html="labels.nbFSAccount"
              ></label>
              <button
                class="btn btn-sm btn-link"
                :data-bs-content="txt.data.nbFSAccount"
                :data-bs-title="txt.data.why"
              >
                <span v-html="icons.shield"></span>
                <span
                  class="visually-hidden"
                  v-html="txt.data.info"
                ></span>
              </button>
            </div>
            <select
              id="nbFSAccount"
              class="form-select"
              v-model="form.nbFSAccount"
            >
              <option value="" selected="selected">- Aucun -</option>
              <option value="1-5">1 à 5</option>
              <option value="6-10">6 à 10</option>
              <option value="11-20">11 à 20</option>
              <option value="21-30">21 à 30</option>
              <option value="31-40">31 à 40</option>
              <option value="41-50">41 à 50</option>
              <option value="51-100">51 à 100</option>
              <option value="101-200">101 à 200</option>
              <option value="201-500">201 à 500</option>
              <option value="+500">+ de 500</option>
            </select>
            <p
              v-show="form.nbFSAccount === ''"
              class="text-muted small mt-2"
            >
              Indiquez le nombre de comptes Framaspace dont vous estimerez
              avoir besoin au bout d’un an. Ce nombre peut être largement
              inférieur au nombre de membres (par exemple, une association
              de 100 adhérent⋅es pourrait n’avoir besoin que d'une quinzaine
              de comptes pour ses salarié⋅es et son Conseil d’administration)
            </p>
            <p
              v-show="/00$/.test(form.nbFSAccount)"
              class="alert alert-danger small mt-2 py-2"
              v-html="txt.warning.nbFSAccount"
            ></p>
          </div>
        </div>
      </div>
    </div>

    <div id="orga" class="row mx-0 my-4 p-0">
      <h2><span class="badge rounded-pill bg-secondary">3</span> Organisation</h2>
      <hr />
      <div class="row m-0 p-0">
        <div class="col-lg-4 text-muted small bg-light p-4">
          <p class="text-center">
            <span v-html="icons.orga"></span>
          </p>
        </div>
        <div class=" col-lg-8 ps-lg-4 px-0 py-2">
          <div class="mb-5">
            <div class="form-label d-flex justify-content-between align-items-end">
              <label
                for="identifier"
                v-html="labels.identifier"
              ></label>
              <button
                class="btn btn-sm btn-link"
                :data-bs-content="txt.data.identifier"
                :data-bs-title="txt.data.why"
              >
                <span v-html="icons.shield"></span>
                <span
                  class="visually-hidden"
                  v-html="txt.data.info"
                ></span>
              </button>
            </div>
            <input
              id="identifier"
              class="form-control"
              :placeholder="placeholders.identifier"
              type="text"
              v-model="form.identifier"
            />
          </div>

          <div class="mb-5">
            <div class="form-label d-flex justify-content-between align-items-end">
              <label
                class="required"
                for="name"
                v-html="labels.name"
              ></label>
              <button
                class="btn btn-sm btn-link"
                :data-bs-content="txt.data.name"
                :data-bs-title="txt.data.why"
              >
                <span v-html="icons.shield"></span>
                <span
                  class="visually-hidden"
                  v-html="txt.data.info"
                ></span>
              </button>
            </div>
            <input
              id="name"
              class="form-control"
              :placeholder="placeholders.name"
              required="required"
              type="text"
              v-model="form.name"
            />
          </div>

          <div class="mb-5">
            <div class="form-label d-flex justify-content-between align-items-end">
              <label
                class="required"
                for="domain"
                v-html="labels.domain"
              ></label>
              <button
                class="btn btn-sm btn-link"
                :data-bs-content="txt.data.domain"
                :data-bs-title="txt.data.why"
              >
                <span v-html="icons.shield"></span>
                <span
                  class="visually-hidden"
                  v-html="txt.data.info"
                ></span>
              </button>
            </div>
            <div class="input-group mb-3">
              <input
                id="domain"
                class="form-control"
                :placeholder="placeholders.domain"
                required="required"
                type="text"
                v-model="form.domain"
              />
              <span class="input-group-text">.frama.space</span>
            </div>
            <p
              v-show="status.help.domain"
              class="text-muted small mt-2"
              v-html="txt.help.domain"
            ></p>
          </div>

          <div class="mb-5">
            <div class="form-label d-flex justify-content-between align-items-end">
              <label
                for="type"
                v-html="labels.type"
              ></label>
              <button
                class="btn btn-sm btn-link"
                :data-bs-content="txt.data.type"
                :data-bs-title="txt.data.why"
              >
                <span v-html="icons.shield"></span>
                <span
                  class="visually-hidden"
                  v-html="txt.data.info"
                ></span>
              </button>
            </div>
            <select
              id="type"
              class="form-select"
              v-model="form.type"
            >
              <option value="collective" selected="selected">Association de fait (collectif informel)</option>
              <option value="association_1901">Association loi 1901</option>
              <option value="association_1907">Association loi 1907</option>
              <option value="syndicat">Syndicat</option>
              <option value="scop">SCOP</option>
              <option value="scic">SCIC</option>
              <option value="entreprise">Entreprise (SA, SARL, SAS)</option>
              <option value="public">Institution publique</option>
              <option value="other">Autre</option>
            </select>
            <p
              v-show="!/^(coll|asso|synd)/.test(form.type)"
              class="alert alert-danger small mt-2 py-2"
              v-html="txt.warning.type"
            ></p>
          </div>

          <div class="mb-5">
            <div class="form-label d-flex justify-content-between align-items-end">
              <label
                class="required"
                for="name"
                v-html="labels.creationYear"
              ></label>
              <button
                class="btn btn-sm btn-link"
                :data-bs-content="txt.data.creationYear"
                :data-bs-title="txt.data.why"
              >
                <span v-html="icons.shield"></span>
                <span
                  class="visually-hidden"
                  v-html="txt.data.info"
                ></span>
              </button>
            </div>
            <input
              id="creationYear"
              class="form-control"
              min="1900"
              max="2030"
              step="1"
              :placeholder="placeholders.creationYear"
              required="required"
              type="number"
              v-model="form.creationYear"
            />
          </div>

          <div class="mb-5">
            <div class="form-label d-flex justify-content-between align-items-end">
              <label
                class="required"
                for="country"
                v-html="labels.country"
              ></label>
              <button
                class="btn btn-sm btn-link"
                :data-bs-content="txt.data.country"
                :data-bs-title="txt.data.why"
              >
                <span v-html="icons.shield"></span>
                <span
                  class="visually-hidden"
                  v-html="txt.data.info"
                ></span>
              </button>
            </div>
            <select
              id="country"
              class="form-select"
              v-model="form.country"
            >
              <option value="BE">🇧🇪 Belgique</option>
              <option value="CH">🇨🇭 Suisse</option>
              <option value="FR" selected="selected">🇫🇷 France</option>
              <option value=" ">Autre</option>
            </select>
            <p
              v-show="form.country === ' '"
              class="alert alert-danger small mt-2 py-2"
              v-html="txt.warning.country"
            ></p>
          </div>

          <div class="mb-5">
            <div class="form-label d-flex justify-content-between align-items-end">
              <label
                class="required"
                for="object"
                v-html="labels.object"
              ></label>
              <button
                class="btn btn-sm btn-link"
                :data-bs-content="txt.data.object"
                :data-bs-title="txt.data.why"
              >
                <span v-html="icons.shield"></span>
                <span
                  class="visually-hidden"
                  v-html="txt.data.info"
                ></span>
              </button>
            </div>
            <textarea
              id="object"
              class="form-control"
              :placeholder="placeholders.object"
              required="required"
              type="text"
              v-model="form.object"
            ></textarea>
            <p
              class="text-muted small mt-2"
              v-html="txt.help.object"
            ></p>
          </div>

          <div class="mb-5">
            <div class="form-label d-flex justify-content-between align-items-end">
              <label
                class="required"
                for="mainActions"
                v-html="labels.mainActions"
              ></label>
              <button
                class="btn btn-sm btn-link"
                :data-bs-content="txt.data.mainActions"
                :data-bs-title="txt.data.why"
              >
                <span v-html="icons.shield"></span>
                <span
                  class="visually-hidden"
                  v-html="txt.data.info"
                ></span>
              </button>
            </div>
            <textarea
              id="object"
              class="form-control"
              :placeholder="placeholders.mainActions"
              required="required"
              type="text"
              v-model="form.mainActions"
            ></textarea>
            <p
              class="text-muted small mt-2"
              v-html="txt.help.mainActions"
            ></p>
          </div>
        </div>
      </div>
    </div>

    <div id="know-you" class="row mx-0 my-4 p-0">
      <h2><span class="badge rounded-pill bg-secondary">4</span> Mieux vous connaître</h2>
      <hr />
      <div class="row m-0 p-0">
        <div class="col-lg-4 text-muted small bg-light p-4">
          <p class="text-center">
            <span v-html="icons.coffee"></span>
          </p>
          <p>
            <strong>Les informations suivantes sont facultatives</strong>
            mais elles nous aiderons à départager les candidatures.
          </p>
          <p>
            Nous les demandons essentiellement à des fins statistiques et pour
            nous permettre de mieux comprendre vos besoins,
            et de voir si ces derniers correspondent à ce que nous sommes en
            capacité de proposer.
          </p>
          <p><strong>Nous vous recommandons chaudement de les remplir.</strong></p>
        </div>

        <div class=" col-lg-8 ps-lg-4 px-0 py-2">
          <div class="mb-5">
            <div class="form-label d-flex justify-content-between align-items-end">
              <label v-html="labels.topics"></label>
              <button
                class="btn btn-sm btn-link"
                :data-bs-content="txt.data.topics"
                :data-bs-title="txt.data.why"
              >
                <span
                  class="visually-hidden"
                  v-html="txt.data.info"
                ></span>
              </button>
            </div>
            <div class="row mx-0 p-2 border rounded align-items-center">
              <div
                v-for="(topic, index) in topics"
                :key="topic"
                class="form-check col-md-6"
              >
                <input
                  :id="\`topic_\${index}\`"
                  type="checkbox"
                  class="form-check-input"
                  v-model="form.topics[index]"
                />
                <label
                  class="form-check-label"
                  :for="\`topic_\${index}\`"
                  v-html="labels[\`topics_\${index}\`]"
                ></label>
              </div>
              <div class="col-md-6 my-1 p-1">
                <div class="input-group input-group-sm">
                  <label
                    class="input-group-text"
                    for="topics_others"
                    v-html="labels.topics_others"
                  ></label>
                  <input
                    id="topics_others"
                    class="form-control form-control-sm"
                    :placeholder="placeholders.topics_others"
                    type="text"
                    v-model="form.topics_others"
                  />
                </div>
              </div>
            </div>
          </div>

          <div class="mb-5">
            <div class="form-label d-flex justify-content-between align-items-end">
              <label
                for="reasons"
                v-html="labels.reasons"
              ></label>
            </div>
            <textarea
              id="object"
              class="form-control"
              :placeholder="placeholders.reasons"
              type="text"
              v-model="form.reasons"
            ></textarea>
          </div>

          <div class="mb-5">
            <div class="form-label d-flex justify-content-between align-items-end">
              <label
                for="website"
                v-html="labels.website"
              ></label>
            </div>
            <input
              id="website"
              class="form-control"
              :placeholder="placeholders.website"
              type="text"
              v-model="form.website"
            />
          </div>

          <div class="mb-5">
            <div class="form-label d-flex justify-content-between align-items-end">
              <label
                for="nbBeneficiaries"
                v-html="labels.nbBeneficiaries"
              ></label>
            </div>
            <input
              id="nbBeneficiaries"
              class="form-control"
              :placeholder="placeholders.nbBeneficiaries"
              type="text"
              v-model="form.nbBeneficiaries"
            />
            <p
              class="text-muted small mt-2"
              v-html="txt.help.nbBeneficiaries"
            ></p>
          </div>

          <div class="mb-5">
            <div class="form-label d-flex justify-content-between align-items-end">
              <label
                for="nbEmployees"
                v-html="labels.nbEmployees"
              ></label>
            </div>
            <input
              id="nbEmployees"
              class="form-control"
              :placeholder="placeholders.nbEmployees"
              type="text"
              v-model="form.nbEmployees"
            />
            <p
              class="text-muted small mt-2"
              v-html="txt.help.nbEmployees"
            ></p>
          </div>

          <div class="mb-5">
            <div class="form-label d-flex justify-content-between align-items-end">
              <label
                for="nbMembers"
                v-html="labels.nbMembers"
              ></label>
            </div>
            <input
              id="nbMembers"
              class="form-control"
              :placeholder="placeholders.nbMembers"
              type="text"
              v-model="form.nbMembers"
            />
          </div>
        </div>
      </div>
    </div>

    <div id="frama-you" class="row mx-0 my-4 p-0">
      <h2><span class="badge rounded-pill bg-secondary">5</span> Framasoft et vous</h2>
      <hr />
      <div class="row m-0 p-0">
        <div class="col-lg-4 text-muted small bg-light p-4">
          <p class="text-center">
            <span v-html="icons.handshake"></span>
          </p>
          <p>
            Nous devons obtenir <strong>votre consentement</strong> pour que nous puissions
            vous recontacter par email à des fins « administratives »
            au titre de la gestion de votre compte.
          </p>
          <p>
            Nous devons également nous assurer que vous avez bien compris que
            le service <strong>Framaspace sera en amélioration constante,
            sur la base de vos retours</strong>.
            Nous ferons de notre mieux pour fournir la meilleure qualité de
            service possible mais des dysfonctionnements pourraient intervenir.
            Pendant cette phase, Framasoft a besoin de pouvoir
          </p>
          <ul>
            <li>
              vous communiquer des informations sur l’état d’avancement de la
              plateforme Framaspace (ex : envois de lettres d'information)
            </li>
            <li>
              et recueillir vos avis pour améliorer la plateforme
              (ex : envoi de questionnaires).
            </li>
          </ul>
          <p>
            Enfin, le service <strong>Framaspace est soumis à des conditions
            d’utilisation</strong>.
            Si vous refusez de vous y conformer, nous ne pourrons pas vous ouvrir
            de compte.
          </p>
        </div>

        <div class=" col-lg-8 ps-lg-4 px-0 py-4">
          <div class="mb-5">
            <div class="form-check">
              <input
                id="acceptContact"
                class="form-check-input"
                required="required"
                type="checkbox"
                v-model="form.acceptContact"
              />
              <label
                class="form-check-label required"
                for="acceptContact"
                v-html="labels.acceptContact"
              ></label>
            </div>
          </div>

          <div class="mb-5">
            <div class="form-check">
              <input
                id="betaWarning"
                class="form-check-input"
                required="required"
                type="checkbox"
                v-model="form.betaWarning"
              />
              <label
                class="form-check-label required"
                for="betaWarning"
                v-html="labels.betaWarning"
              ></label>
            </div>
          </div>

          <div class="mb-5">
            <div class="form-check">
              <input
                id="acceptCSU"
                aria-describedby="acceptCGU_help"
                class="form-check-input"
                required="required"
                type="checkbox"
                v-model="form.acceptCGU"
              />
              <label
                class="form-check-label required"
                for="acceptCGU"
                v-html="labels.acceptCGU"
              ></label>
            </div>
            <p
              id="acceptCGU_help"
              class="text-muted small mt-2"
              v-html="txt.help.cgu"
            ></p>
          </div>

          <div class="mb-5">
            <div class="form-check">
              <input
                id="acceptCSU"
                aria-describedby="acceptCSU_help"
                class="form-check-input"
                required="required"
                type="checkbox"
                v-model="form.acceptCSU"
              />
              <label
                class="form-check-label required"
                for="acceptCSU"
                v-html="labels.acceptCSU"
              ></label>
            </div>
            <p
              id="acceptCSU_help"
              class="text-muted small mt-2"
              v-html="txt.help.csu"
            ></p>
          </div>
        </div>
      </div>
    </div>
  `,
}).mount('#app');
