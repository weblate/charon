## API

### POST /api/v1/searchOrganization

#### Paramètres
* `search` une chaîne de caractères à rechercher. Du texte brut ou bien ou bien un identifiant RNA ou un numéro SIRET/SIREN.

#### Sortie
Liste d'éléments composés de . En dehors de `name`, toutes les clés sont optionnelles.

* `name` Le nom de l'organisation
* `creationYear` L'année d'enregistrement de l'organisation
* `rna` Le numéro national d'identitifcation (ex-Waldec), si une association
* `object` Objet de l'organisation
* `nature` Nature de l'association. Valeurs observées : `R`, `D`, sans savoir leur signification (???)
* `isUnion` Booléen indiquant si l'organisation est une union
* `isFederation` Booléen indiquant si l'organisation est une fédération
* `siren` Le numéro SIREN de l'organisation
* `categorieEntreprise` La catégorie de l'entreprise (`PME`, `ETI`, `GE`)
* `isESS` booléan indiquant si l'entreprise appartient au champ de l'économie sociale et solidaire
* `categorieJuridique` Catégorie juridique de l'organisation
* `trancheEffectifs` Tranche des effectifs. Correspondance sur https://www.sirene.fr/sirene/public/variable/trancheEffectifsUniteLegale
* `source` Source des informations