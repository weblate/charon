<?php

namespace App\MessageHandler;

use App\Message\DiscoursePostAction;
use App\Repository\SubmissionRepository;
use App\Services\DiscourseService;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class DiscoursePostActionHandler
{
    private SubmissionRepository $submissionRepository;
    private DiscourseService $discourseService;

    public function __construct(SubmissionRepository $submissionRepository, DiscourseService $discourseService)
    {
        $this->submissionRepository = $submissionRepository;
        $this->discourseService = $discourseService;
    }

    public function __invoke(DiscoursePostAction $discoursePostAction): void
    {
        $submission = $this->submissionRepository->find($discoursePostAction->getSubmissionId());
        $this->discourseService->postNewSubmission($submission);
    }
}
