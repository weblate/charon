<?php

namespace App\Entity;

use App\Entity\Traits\TimeStampableTrait;
use App\Repository\JobRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Ignore;
use OpenApi\Attributes as OA;

#[ORM\Entity(repositoryClass: JobRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Job
{
    use TimeStampableTrait;
    public const JOB_TYPE_CREATE_SPACE = 0;
    public const JOB_TYPE_UPDATE_SPACE = 10;
    public const JOB_TYPE_CHANGE_OFFICE = 15;
    public const JOB_TYPE_DISABLE_SPACE = 20;
    public const JOB_TYPE_REENABLE_SPACE = 30;
    public const JOB_TYPE_DELETE_SPACE = 50;

    public const JOB_TYPES = [self::JOB_TYPE_CREATE_SPACE, self::JOB_TYPE_UPDATE_SPACE, self::JOB_TYPE_CHANGE_OFFICE, self::JOB_TYPE_DISABLE_SPACE, self::JOB_TYPE_DISABLE_SPACE, self::JOB_TYPE_REENABLE_SPACE, self::JOB_TYPE_DELETE_SPACE];

    public const JOB_STATUS_PENDING = 0;
    public const JOB_STATUS_ONGOING = 10;
    public const JOB_STATUS_ERROR = 50;
    public const JOB_STATUS_DONE = 100;

    public const JOB_STATUSES = [self::JOB_STATUS_PENDING, self::JOB_STATUS_ONGOING, self::JOB_STATUS_ERROR, self::JOB_STATUS_DONE];

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'integer')]
    private ?int $type;

    #[Ignore]
    #[ORM\ManyToOne(targetEntity: Space::class, inversedBy: 'jobs')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Space $space;

    #[ORM\Column(type: 'json')]
    #[OA\Property(ref: '#/components/schemas/SpaceDetails')]
    private array $data = [];

    #[ORM\Column]
    private ?int $status = self::JOB_STATUS_PENDING;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $body = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getSpace(): ?Space
    {
        return $this->space;
    }

    public function setSpace(?Space $space): self
    {
        $this->space = $space;

        return $this;
    }

    public function getData(): ?array
    {
        return $this->data;
    }

    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getBody(): ?string
    {
        return $this->body;
    }

    public function setBody(?string $body): self
    {
        $this->body = $body;

        return $this;
    }
}
