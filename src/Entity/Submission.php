<?php

namespace App\Entity;

use App\Entity\Traits\TimeStampableTrait;
use App\Repository\SubmissionRepository;
use App\Validator as AppValidator;
use DateTimeImmutable;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\Ignore;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Attributes as OA;

#[ORM\Entity(repositoryClass: SubmissionRepository::class)]
#[UniqueEntity('domain')]
#[ORM\HasLifecycleCallbacks]
class Submission
{
    use TimeStampableTrait;
    public const SUBMISSION_TYPE_COLLECTIVE = 'collective';
    public const SUBMISSION_TYPE_ASSOCIATION_1901 = 'association_1901';
    public const SUBMISSION_TYPE_ASSOCIATION_1907 = 'association_1907';
    public const SUBMISSION_TYPE_SYNDICATE = 'syndicat';
    public const SUBMISSION_TYPE_SCOP = 'scop';
    public const SUBMISSION_TYPE_SCIC = 'scic';
    public const SUBMISSION_TYPE_ENTREPRISE = 'entreprise';
    public const SUBMISSION_TYPE_PUBLIC = 'public';
    public const SUBMISSION_TYPE_OTHER = 'other';

    public const SUBMISSION_TYPES = [self::SUBMISSION_TYPE_COLLECTIVE, self::SUBMISSION_TYPE_ASSOCIATION_1901, self::SUBMISSION_TYPE_ASSOCIATION_1907, self::SUBMISSION_TYPE_SYNDICATE, self::SUBMISSION_TYPE_SCOP, self::SUBMISSION_TYPE_SCIC, self::SUBMISSION_TYPE_ENTREPRISE, self::SUBMISSION_TYPE_PUBLIC, self::SUBMISSION_TYPE_OTHER];

    public const SUBMISSION_STATUS_PENDING = 0;
    public const SUBMISSION_STATUS_APPROVED = 10;
    public const SUBMISSION_STATUS_REJECTED = 20;

    public const SUBMISSION_STATUSES = [self::SUBMISSION_STATUS_PENDING, self::SUBMISSION_STATUS_APPROVED, self::SUBMISSION_STATUS_REJECTED];

    public const SUBMISSION_ALREADY_USED_AS_USER = 'user';
    public const SUBMISSION_ALREADY_USED_AS_ADMIN = 'admin';
    public const SUBMISSION_ALREADY_USED_AT_CHATONS = 'chatons';
    public const SUBMISSION_ALREADY_USED_NOT_KNOWN = 'not_known';

    public const SUBMISSION_ALREADY_USED_VALUES = [self::SUBMISSION_ALREADY_USED_AS_USER, self::SUBMISSION_ALREADY_USED_AS_ADMIN, self::SUBMISSION_ALREADY_USED_AT_CHATONS, self::SUBMISSION_ALREADY_USED_NOT_KNOWN];

    public const SUBMISSION_NOT_RESPONSIBLE = 0;
    public const SUBMISSION_RESPONSIBLE = 1;
    public const SUBMISSION_RESPONSIBLE_UNKNOWN = 2;

    public const SUBMISSION_RESPONSIBLE_VALUES = [self::SUBMISSION_NOT_RESPONSIBLE, self::SUBMISSION_RESPONSIBLE, self::SUBMISSION_RESPONSIBLE_UNKNOWN];

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    #[Groups('submission')]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Groups('submission')]
    #[Assert\NotBlank]
    #[ORM\Column(type: 'string', length: 255)]
    private ?string $type;

    #[Groups('submission')]
    #[Assert\NotBlank]
    #[ORM\Column(type: 'string', length: 255)]
    private ?string $name;

    #[Groups('submission')]
    #[Assert\NotBlank]
    #[ORM\Column(type: 'text')]
    private ?string $object;

    #[Groups('submission')]
    #[ORM\Column(type: 'json')]
    #[OA\Property(type: 'array', items: new OA\Items(type: 'string'))]
    private array $topics = [];

    #[Groups('submission')]
    #[Assert\NotBlank]
    #[ORM\Column(type: 'text')]
    private ?string $mainActions;

    #[Groups('submission')]
    #[Assert\Range(min: 0, max: 2100)]
    #[ORM\Column(type: 'integer', length: 255, nullable: true)]
    private ?string $creationYear = null;

    #[Groups('submission')]
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $identifier;

    #[Groups('submission')]
    #[Assert\AtLeastOneOf([
        new Assert\Hostname(),
        new Assert\Url()
    ])]
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private ?string $website = null;

    #[Groups('submission')]
    #[Assert\PositiveOrZero]
    #[ORM\Column(type: Types::BIGINT, nullable: true)]
    private ?int $nbEmployees = null;

    #[Groups('submission')]
    #[Assert\PositiveOrZero]
    #[ORM\Column(type: Types::BIGINT, nullable: true)]
    private ?int $nbMembers = null;

    #[Groups('submission')]
    #[Assert\PositiveOrZero]
    #[ORM\Column(type: Types::BIGINT, nullable: true)]
    private ?int $nbBeneficiaries = null;

    #[Groups('submission')]
    #[Assert\PositiveOrZero]
    #[ORM\Column(type: Types::BIGINT, length: 255, nullable: true)]
    private ?int $budget = null;

    #[Groups('submission')]
    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $reasons = null;

    #[Groups('submission')]
    #[Assert\NotBlank]
    #[Assert\Regex('/^([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])$/')]
    #[AppValidator\ReservedDomains]
    #[ORM\Column(type: 'string', length: 255, unique: true)]
    private ?string $domain;

    #[Groups('submission')]
    #[Assert\NotBlank]
    #[ORM\Column(type: 'string', length: 255)]
    private ?string $identity;

    #[Groups('submission')]
    #[Assert\NotBlank]
    #[Assert\Email]
    #[ORM\Column(type: 'string', length: 255)]
    private ?string $email;

    #[Groups('submission')]
    #[Assert\NotBlank]
    #[ORM\Column(type: 'integer', options: ['default' => 0])]
    private int $status = 0;

    #[Groups('submission')]
    #[ORM\Column(type: 'string', length: 255)]
    private ?string $locale = 'en';

    #[Ignore]
    #[ORM\OneToOne(mappedBy: 'submission', cascade: ['persist', 'remove'])]
    private ?Space $space = null;

    #[Groups('submission')]
    #[ORM\Column(type: 'json')]
    #[OA\Property(type: 'array', items: new OA\Items(type: 'string'))]
    private array $alreadyUsed = [];

    #[Groups('submission')]
    #[Assert\NotBlank]
    #[ORM\Column(type: 'string', length: 255)]
    private ?string $accountsNeeded;

    #[Groups('submission')]
    #[ORM\Column(length: 255, nullable: true)]
    private ?string $country = null;

    #[Groups('submission')]
    #[Assert\NotBlank]
    #[Assert\Range(min: 0, max: 2)]
    #[ORM\Column]
    private ?int $responsible = null;

    #[Groups('submission')]
    #[ORM\Column(length: 255, nullable: true)]
    private ?string $organisationLanguage = null;

    #[Groups('submission')]
    #[ORM\Column(options: ['default' => '[]'])]
    private array $metadata = [];

    #[ORM\OneToOne(mappedBy: 'submission', cascade: ['persist'])]
    private ?Invite $invite = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getObject(): ?string
    {
        return $this->object;
    }

    public function setObject(string $object): self
    {
        $this->object = $object;

        return $this;
    }

    public function getTopics(): ?array
    {
        return $this->topics;
    }

    public function setTopics(array $topics): self
    {
        $this->topics = $topics;

        return $this;
    }

    public function getMainActions(): ?string
    {
        return $this->mainActions;
    }

    public function setMainActions(string $mainActions): self
    {
        $this->mainActions = $mainActions;

        return $this;
    }

    public function getCreationYear(): ?string
    {
        return $this->creationYear;
    }

    public function setCreationYear(?string $creationYear): self
    {
        $this->creationYear = $creationYear;

        return $this;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(?string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getNbEmployees(): ?int
    {
        return $this->nbEmployees;
    }

    public function setNbEmployees(?int $nbEmployees): self
    {
        $this->nbEmployees = $nbEmployees;

        return $this;
    }

    public function getNbMembers(): ?int
    {
        return $this->nbMembers;
    }

    public function setNbMembers(?int $nbMembers): self
    {
        $this->nbMembers = $nbMembers;

        return $this;
    }

    public function getNbBeneficiaries(): ?int
    {
        return $this->nbBeneficiaries;
    }

    public function setNbBeneficiaries(?int $nbBeneficiaries): self
    {
        $this->nbBeneficiaries = $nbBeneficiaries;

        return $this;
    }

    public function getReasons(): ?string
    {
        return $this->reasons;
    }

    public function setReasons(?string $reasons): self
    {
        $this->reasons = $reasons;

        return $this;
    }

    public function getDomain(): ?string
    {
        return $this->domain;
    }

    public function setDomain(string $domain): self
    {
        $this->domain = $domain;

        return $this;
    }

    public function getIdentity(): ?string
    {
        return $this->identity;
    }

    public function setIdentity(string $identity): self
    {
        $this->identity = $identity;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public const MAX_SCORE = 990;
    public const MIN_SCORE = -2650;
    public const TOTAL_MAX_SCORE = self::MAX_SCORE - self::MIN_SCORE;

    public function getScore(): float
    {
        $score = 0;

        $scoreResponsible = match ($this->responsible) {
            Submission::SUBMISSION_NOT_RESPONSIBLE => 0,
            Submission::SUBMISSION_RESPONSIBLE => 10,
            Submission::SUBMISSION_RESPONSIBLE_UNKNOWN => -10
        };

        $score += $scoreResponsible;

        if (in_array(Submission::SUBMISSION_ALREADY_USED_AS_ADMIN, $this->alreadyUsed)) {
            $score += 100;
        }

        if (in_array(Submission::SUBMISSION_ALREADY_USED_AS_USER, $this->alreadyUsed)) {
            $score += 50;
        }

        if (in_array(Submission::SUBMISSION_ALREADY_USED_AT_CHATONS, $this->alreadyUsed)) {
            $score -= 100;
        }

        $accountsNeededScore = match ($this->accountsNeeded) {
            "1-5", "6-10" => 30,
            "11-20", "21-30" => 20,
            "31-40" => 10,
            "41-50" => 0,
            default => -500,
        };
        $score += $accountsNeededScore;

        $scoreType = match ($this->type) {
            Submission::SUBMISSION_TYPE_ASSOCIATION_1901, Submission::SUBMISSION_TYPE_ASSOCIATION_1907 => 100,
            Submission::SUBMISSION_TYPE_SYNDICATE => 0,
            default => -1000,
        };
        $score += $scoreType;

        $objectSocialSize = mb_strlen($this->object);
        if ($objectSocialSize < 50) {
            $score -= 100;
        } elseif ($objectSocialSize < 200) {
            $score += 50;
        } elseif ($objectSocialSize < 1000) {
            $score += 100;
        }

        $mainActionsSize = mb_strlen($this->mainActions);
        if ($mainActionsSize < 50) {
            $score -= 100;
        } elseif ($mainActionsSize < 200) {
            $score += 50;
        } elseif ($mainActionsSize < 1000) {
            $score += 100;
        }

        $reasonsForAccountSize = mb_strlen($this->reasons ?? '');
        if ($reasonsForAccountSize < 50) {
            $score -= 300;
        } elseif ($reasonsForAccountSize < 200) {
            $score += 50;
        } elseif ($reasonsForAccountSize < 2000) {
            $score += 100;
        }

        if (trim($this->website ?? '') === '' || $this->website === null) {
            $score -= 50;
        }

        if ($this->nbEmployees ?? 0 >= 1 && $this->nbEmployees <= 5) {
            $score += 100;
        } elseif ($this->nbEmployees ?? 0 > 10) {
            $score -= 100;
        }

        if ($this->nbMembers ?? 0 >= 1 && $this->nbMembers <= 50) {
            $score += 100;
        } elseif ($this->nbMembers ?? 0 <= 100) {
            $score -= 50;
        } elseif ($this->nbMembers ?? 0 > 500) {
            $score -= 100;
        }

        if ($this->budget ?? 0 <= 1000) {
            $score += 200;
        } elseif ($this->budget ?? 0 <= 10000) {
            $score += 100;
        } elseif ($this->budget ?? 0 <= 50000) {
            $score += 50;
        } elseif ($this->budget ?? 0 > 100000 && $this->budget <= 500000) {
            $score -= 100;
        } elseif ($this->budget ?? 0 > 500000) {
            $score -= 300;
        }

        if ($this->invite !== null) {
            $score += 200;
        }

        // Put the minimum at zero
        $score -= self::MIN_SCORE;

        // Security
        $score = min($score, self::TOTAL_MAX_SCORE);
        $score = max($score, 0);
        return round($score * 100 / self::TOTAL_MAX_SCORE);
    }

    public function getSpace(): ?Space
    {
        return $this->space;
    }

    public function setSpace(Space $space): self
    {
        // set the owning side of the relation if necessary
        if ($space->getSubmission() !== $this) {
            $space->setSubmission($this);
        }

        $this->space = $space;

        return $this;
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public static function submissionTypesToNames(): array
    {
        return [
            self::SUBMISSION_TYPE_COLLECTIVE => 'Association de fait (collectif informel)',
            self::SUBMISSION_TYPE_ASSOCIATION_1901 => 'Association loi 1901',
            self::SUBMISSION_TYPE_ASSOCIATION_1907 => 'Association loi 1907',
            self::SUBMISSION_TYPE_SYNDICATE => 'Syndicat',
            self::SUBMISSION_TYPE_SCOP => 'SCOP',
            self::SUBMISSION_TYPE_SCIC => 'SCIC',
            self::SUBMISSION_TYPE_ENTREPRISE => 'Entreprise (SA, SARL, SAS)',
            self::SUBMISSION_TYPE_PUBLIC => 'Institution publique',
            self::SUBMISSION_TYPE_OTHER => 'Autre'
        ];
    }

    public function getBudget(): ?int
    {
        return $this->budget;
    }

    public function setBudget(int $budget): self
    {
        $this->budget = $budget;

        return $this;
    }

    public function getAlreadyUsed(): array
    {
        return $this->alreadyUsed;
    }

    public function setAlreadyUsed(array $alreadyUsed): self
    {
        $this->alreadyUsed = $alreadyUsed;

        return $this;
    }

    public function getAccountsNeeded(): ?string
    {
        return $this->accountsNeeded;
    }

    public function setAccountsNeeded(string $accountsNeeded): self
    {
        $this->accountsNeeded = $accountsNeeded;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getResponsible(): ?int
    {
        return $this->responsible;
    }

    public function setResponsible(int $responsible): self
    {
        $this->responsible = $responsible;

        return $this;
    }

    public function getOrganisationLanguage(): ?string
    {
        return $this->organisationLanguage;
    }

    public function setOrganisationLanguage(?string $organisationLanguage): self
    {
        $this->organisationLanguage = $organisationLanguage;

        return $this;
    }

    public function getMetadata(): array
    {
        return $this->metadata;
    }

    public function setMetadata(array $metadata): self
    {
        $this->metadata = $metadata;

        return $this;
    }

    public function getInvite(): ?Invite
    {
        return $this->invite;
    }

    public function setInvite(?Invite $invite): self
    {
        // unset the owning side of the relation if necessary
        if ($invite === null && $this->invite !== null) {
            $this->invite->setSubmission(null);
        }

        // set the owning side of the relation if necessary
        if ($invite !== null && $invite->getSubmission() !== $this) {
            $invite->setSubmission($this);
        }

        $this->invite = $invite;

        return $this;
    }
}
