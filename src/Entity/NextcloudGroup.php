<?php

namespace App\Entity;

class NextcloudGroup
{
    private string $groupId;

    /**
     * @return string
     */
    public function getGroupId(): string
    {
        return $this->groupId;
    }

    /**
     * @param string $groupId
     * @return NextcloudGroup
     */
    public function setGroupId(string $groupId): NextcloudGroup
    {
        $this->groupId = $groupId;
        return $this;
    }
}
