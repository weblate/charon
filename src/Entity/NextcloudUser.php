<?php

namespace App\Entity;

class NextcloudUser
{
    private string $userId;
    private ?string $displayName;
    private string $email;
    private ?array $groups;
    private ?array $subadmin;
    private ?string $language;
    private ?string $quota;

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     * @return NextcloudUser
     */
    public function setUserId(string $userId): NextcloudUser
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDisplayName(): ?string
    {
        return $this->displayName;
    }

    /**
     * @param string|null $displayName
     * @return NextcloudUser
     */
    public function setDisplayName(?string $displayName): NextcloudUser
    {
        $this->displayName = $displayName;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return NextcloudUser
     */
    public function setEmail(string $email): NextcloudUser
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getGroups(): ?array
    {
        return $this->groups;
    }

    /**
     * @param array|null $groups
     * @return NextcloudUser
     */
    public function setGroups(?array $groups): NextcloudUser
    {
        $this->groups = $groups;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getSubadmin(): ?array
    {
        return $this->subadmin;
    }

    /**
     * @param array|null $subadmin
     * @return NextcloudUser
     */
    public function setSubadmin(?array $subadmin): NextcloudUser
    {
        $this->subadmin = $subadmin;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLanguage(): ?string
    {
        return $this->language;
    }

    /**
     * @param string|null $language
     * @return NextcloudUser
     */
    public function setLanguage(?string $language): NextcloudUser
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getQuota(): ?string
    {
        return $this->quota;
    }

    /**
     * @param string|null $quota
     * @return NextcloudUser
     */
    public function setQuota(?string $quota): NextcloudUser
    {
        $this->quota = $quota;
        return $this;
    }
}
