<?php

namespace App\Entity;

use App\Entity\Traits\TimeStampableTrait;
use App\Repository\SpaceRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use OpenApi\Attributes as OA;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\Ignore;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: SpaceRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Space implements UserInterface
{
    use TimeStampableTrait;
    public const SPACE_STATUS_PENDING = 0;
    public const SPACE_STATUS_CREATED = 10;
    public const SPACE_STATUSES = [self::SPACE_STATUS_PENDING, self::SPACE_STATUS_CREATED];

    public const SPACE_OFFICE_TYPE_COLLABORA_OFFICE = 10;
    public const SPACE_OFFICE_TYPE_ONLYOFFICE = 20;
    public const SPACE_OFFICE_TYPE_DEFAULT = self::SPACE_OFFICE_TYPE_COLLABORA_OFFICE;

    public const SPACE_OFFICE_TYPES = [self::SPACE_OFFICE_TYPE_COLLABORA_OFFICE, self::SPACE_OFFICE_TYPE_ONLYOFFICE];

    #[Groups('space')]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Groups('space')]
    #[ORM\Column(type: 'string', length: 255, unique: true)]
    #[Assert\NotBlank]
    #[Assert\Regex("/.+(?<!-coo)(?<!-oo)$/")]
    private ?string $domain;

    #[Ignore]
    #[ORM\OneToMany(mappedBy: 'space', targetEntity: Job::class, orphanRemoval: true)]
    private Collection $jobs;

    #[Groups('space')]
    #[ORM\Column(type: 'json')]
    #[OA\Property(ref: '#/components/schemas/SpaceDetails')]
    private array $details = [];

    #[Groups('space')]
    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Email]
    #[Assert\NotBlank]
    private ?string $contactEmail;

    #[Groups('space')]
    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    private ?string $contactName;

    #[Groups('space')]
    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Locale]
    private ?string $locale = 'en';

    #[Groups('space')]
    #[ORM\Column(type: 'integer', options: ['default' => 0])]
    private ?int $status = self::SPACE_STATUS_PENDING;

    #[Groups('space')]
    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank]
    private ?string $organizationName;

    #[Groups('space')]
    #[ORM\Column]
    private ?bool $enabled = true;

    #[Groups('space')]
    #[ORM\Column]
    private ?int $office = self::SPACE_OFFICE_TYPE_DEFAULT;

    #[Ignore]
    #[ORM\OneToOne(inversedBy: 'space', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Submission $submission = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $deletedAt = null;

    public function __construct()
    {
        $this->jobs = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDomain(): ?string
    {
        return $this->domain;
    }

    public function setDomain(string $domain): self
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * @return Collection<int, Job>
     */
    public function getJobs(): Collection
    {
        return $this->jobs;
    }

    public function addJob(Job $job): self
    {
        if (!$this->jobs->contains($job)) {
            $this->jobs[] = $job;
            $job->setSpace($this);
        }

        return $this;
    }

    public function removeJob(Job $job): self
    {
        if ($this->jobs->removeElement($job)) {
            // set the owning side to null (unless already changed)
            if ($job->getSpace() === $this) {
                $job->setSpace(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDetails(): ?array
    {
        return $this->details;
    }

    public function setDetails(array $details): self
    {
        $this->details = $details;

        return $this;
    }

    public function getContactEmail(): ?string
    {
        return $this->contactEmail;
    }

    public function setContactEmail(string $contactEmail): self
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    public function getContactName(): ?string
    {
        return $this->contactName;
    }

    public function setContactName(string $contactName): self
    {
        $this->contactName = $contactName;

        return $this;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getOrganizationName(): ?string
    {
        return $this->organizationName;
    }

    public function setOrganizationName(string $organizationName): self
    {
        $this->organizationName = $organizationName;

        return $this;
    }

    #[Ignore]
    public function getRoles(): array
    {
        return ['ROLE_USER'];
    }

    /**
     * @return void
     */
    public function eraseCredentials()
    {
        // Nothing to do
    }

    public function getUserIdentifier(): string
    {
        return $this->contactEmail;
    }

    public function isEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getOffice(): ?int
    {
        return $this->office;
    }

    public function setOffice(int $office): self
    {
        $this->office = $office;

        return $this;
    }

    public function __toString(): string
    {
        return $this->domain;
    }

    public function getSubmission(): ?Submission
    {
        return $this->submission;
    }

    public function setSubmission(Submission $submission): self
    {
        $this->submission = $submission;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeImmutable
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeImmutable $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }
}
