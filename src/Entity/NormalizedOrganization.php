<?php

namespace App\Entity;

use DateTimeImmutable;

class NormalizedOrganization
{
    private string $name;
    private string $source;
    private ?string $object;
    private ?string $siren;
    private ?string $siret;
    private ?string $rna;
    private ?string $creationYear;
    private int $nbEmployees;
    private ?string $categorieEntreprise;
    private ?bool $isESS;
    private ?string $categorieJuridique;
    private ?string $trancheEffectifs;
    private ?string $nature;
    private ?bool $isUnion;
    private ?bool $isFederation;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return NormalizedOrganization
     */
    public function setName(string $name): NormalizedOrganization
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSiren(): ?string
    {
        return $this->siren;
    }

    /**
     * @param string|null $siren
     * @return NormalizedOrganization
     */
    public function setSiren(?string $siren): NormalizedOrganization
    {
        $this->siren = $siren;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCreationYear(): ?string
    {
        return $this->creationYear;
    }

    /**
     * @param string|null $creationYear
     * @return NormalizedOrganization
     */
    public function setCreationYear(?string $creationYear): NormalizedOrganization
    {
        $this->creationYear = $creationYear;
        return $this;
    }

    /**
     * @return int
     */
    public function getNbEmployees(): int
    {
        return $this->nbEmployees;
    }

    /**
     * @param int $nbEmployees
     * @return NormalizedOrganization
     */
    public function setNbEmployees(int $nbEmployees): NormalizedOrganization
    {
        $this->nbEmployees = $nbEmployees;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCategorieEntreprise(): ?string
    {
        return $this->categorieEntreprise;
    }

    /**
     * @param string|null $categorieEntreprise
     * @return NormalizedOrganization
     */
    public function setCategorieEntreprise(?string $categorieEntreprise): NormalizedOrganization
    {
        $this->categorieEntreprise = $categorieEntreprise;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsESS(): ?bool
    {
        return $this->isESS;
    }

    /**
     * @param bool|null $isESS
     * @return NormalizedOrganization
     */
    public function setIsESS(?bool $isESS): NormalizedOrganization
    {
        $this->isESS = $isESS;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSiret(): ?string
    {
        return $this->siret;
    }

    /**
     * @param string|null $siret
     * @return NormalizedOrganization
     */
    public function setSiret(?string $siret): NormalizedOrganization
    {
        $this->siret = $siret;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCategorieJuridique(): ?string
    {
        return $this->categorieJuridique;
    }

    /**
     * @param string|null $categorieJuridique
     * @return NormalizedOrganization
     */
    public function setCategorieJuridique(?string $categorieJuridique): NormalizedOrganization
    {
        $this->categorieJuridique = $categorieJuridique;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTrancheEffectifs(): ?string
    {
        return $this->trancheEffectifs;
    }

    /**
     * @param string|null $trancheEffectifs
     * @return NormalizedOrganization
     */
    public function setTrancheEffectifs(?string $trancheEffectifs): NormalizedOrganization
    {
        $this->trancheEffectifs = $trancheEffectifs;
        return $this;
    }

    /**
     * @param string|null $rna
     * @return NormalizedOrganization
     */
    public function setRna(?string $rna): NormalizedOrganization
    {
        $this->rna = $rna;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRna(): ?string
    {
        return $this->rna;
    }

    /**
     * @param string|null $object
     * @return NormalizedOrganization
     */
    public function setObject(?string $object): NormalizedOrganization
    {
        $this->object = $object;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getObject(): ?string
    {
        return $this->object;
    }

    /**
     * @param string|null $nature
     * @return NormalizedOrganization
     */
    public function setNature(?string $nature): NormalizedOrganization
    {
        $this->nature = $nature;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNature(): ?string
    {
        return $this->nature;
    }

    /**
     * @param bool|null $isUnion
     * @return NormalizedOrganization
     */
    public function setIsUnion(?bool $isUnion): NormalizedOrganization
    {
        $this->isUnion = $isUnion;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsUnion(): ?bool
    {
        return $this->isUnion;
    }

    /**
     * @param bool|null $isFederation
     * @return NormalizedOrganization
     */
    public function setIsFederation(?bool $isFederation): NormalizedOrganization
    {
        $this->isFederation = $isFederation;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsFederation(): ?bool
    {
        return $this->isFederation;
    }

    /**
     * @param string $source
     * @return NormalizedOrganization
     */
    public function setSource(string $source): NormalizedOrganization
    {
        $this->source = $source;
        return $this;
    }

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }
}
