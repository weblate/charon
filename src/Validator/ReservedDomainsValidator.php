<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class ReservedDomainsValidator extends ConstraintValidator
{
    public const RESERVED_DOMAINS = [
        'admin',
        'frama',
        'framasoft',
        'inscription',
        'minio',
        'minio-admin',
        'minio-rep-admin',
        'office',
        's3',
        's3-api',
        'tech',
        'support',
        'webmaster',
        'www',
    ];

    public function validate(mixed $value, Constraint $constraint)
    {
        if (!$constraint instanceof ReservedDomains) {
            throw new UnexpectedTypeException($constraint, ReservedDomains::class);
        }

        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) to take care of that
        if (null === $value || '' === $value) {
            return;
        }

        if (!is_string($value)) {
            // throw this exception if your validator cannot handle the passed type so that it can be marked as invalid
            throw new UnexpectedValueException($value, 'string');
        }

        if (in_array(strtolower($value), self::RESERVED_DOMAINS)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ domain }}', $value)
                ->addViolation();
        }

        if (preg_match('/^office-\d+/i', $value)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ domain }}', $value)
                ->addViolation();
        }
    }
}
