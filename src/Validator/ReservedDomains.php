<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

#[\Attribute]
class ReservedDomains extends Constraint
{
    public string $message = 'The domain {{ domain }} is reserved. Please pick another.';

    public function validatedBy(): string
    {
        return static::class.'Validator';
    }
}
