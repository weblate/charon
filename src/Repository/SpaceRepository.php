<?php

namespace App\Repository;

use App\Entity\Space;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Space>
 *
 * @method Space|null find($id, $lockMode = null, $lockVersion = null)
 * @method Space|null findOneBy(array $criteria, array $orderBy = null)
 * @method Space[]    findAll()
 * @method Space[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpaceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Space::class);
    }

    public function findBiggestPortForOfficeServer(string $officeServer): int
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $query = $qb
            ->select("MAX(coalesce(CAST(JSON_GET_TEXT(s.details, 'office_port') AS integer), 10000)) AS port")
            ->from(Space::class, 's')
            ->where("JSON_GET_TEXT(s.details, 'office_server') = :office_server")
            ->setParameter('office_server', $officeServer)
            ->getQuery();
        return $query->getResult()[0]['port'] ?? 10000;
    }

    public function findBiggestNcHpBackendPort(): int
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $query = $qb
            ->select("MAX(coalesce(CAST(JSON_GET_TEXT(s.details, 'nc_hp_backend') AS integer), 10000)) AS port")
            ->from(Space::class, 's')
            ->getQuery();
        return $query->getResult()[0]['port'] ?? 10000;
    }

    public function countNewSpacesByMonths(): array
    {
        $qb = $this->createQueryBuilder('s');

        $qb->where($qb->expr()->eq('s.status', ':status'))
            ->andWhere($qb->expr()->eq('s.enabled', ':enabled'))
            ->setParameter('status', Space::SPACE_STATUS_CREATED)
            ->setParameter('enabled', true)
            ->select([$qb->expr()->count('s.id') . ' AS spaceCount', 'date_trunc(\'month\', s.createdAt) AS createdAtMonth'])
            ->groupBy('createdAtMonth');
        $query = $qb->getQuery();
        return $query->execute();
    }

    public function mesureOfficeTypeDistribution(): array
    {
        $qb = $this->createQueryBuilder('s');

        $qb->where($qb->expr()->eq('s.status', ':status'))
            ->andWhere($qb->expr()->eq('s.enabled', ':enabled'))
            ->setParameter('status', Space::SPACE_STATUS_CREATED)
            ->setParameter('enabled', true)
            ->select([$qb->expr()->count('s.id') . ' AS spaceCount', 's.office'])
            ->groupBy('s.office');
        $query = $qb->getQuery();
        return $query->execute();
    }

    public function findAllQueryWithFilterQueryBuilder(?bool $enabled = null, ?int $status = null): QueryBuilder
    {
        $qb = $this->createQueryBuilder('s');
        if ($enabled !== null) {
            $qb->andWhere($qb->expr()->eq('s.enabled', ':enabled'))
                ->setParameter('enabled', $enabled);
        }
        if (in_array($status, Space::SPACE_STATUSES, true)) {
            $qb->andWhere($qb->expr()->eq('s.status', ':status'))
                ->setParameter('status', $status);
        }
        return $qb;
    }
}
