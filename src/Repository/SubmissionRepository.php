<?php

namespace App\Repository;

use App\Entity\Submission;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Parameter;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Submission>
 *
 * @method Submission|null find($id, $lockMode = null, $lockVersion = null)
 * @method Submission|null findOneBy(array $criteria, array $orderBy = null)
 * @method Submission[]    findAll()
 * @method Submission[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubmissionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Submission::class);
    }

    public function countApprovedSubmissionsByType(): array
    {
        $qb = $this->createQueryBuilder('s');

        $qb->where($qb->expr()->eq('s.status', ':status'))
            ->setParameter('status', Submission::SUBMISSION_STATUS_APPROVED)
            ->select([$qb->expr()->count('s.id') . ' AS submissionCount', 's.type'])
            ->groupBy('s.type');
        $query = $qb->getQuery();
        return $query->execute();
    }
}
