<?php

namespace App\Services\Nextcloud;

use LengthException;

use function random_int;
use function strlen;

class Generator
{
    public function generateInstanceId(): string
    {
        return 'oc' . $this->generate(10, self::CHAR_LOWER.self::CHAR_DIGITS);
    }

    public function generatePasswordSalt(): string
    {
        return $this->generate(30);
    }
    public function generateSecret(): string
    {
        return $this->generate(48);
    }


    public function generateDatabasePassword(): string
    {
        return $this->generate(30, self::CHAR_ALPHANUMERIC);
    }

    public function generateDatabaseUser(string $domain): string
    {
        return $domain . $this->generate(4, self::CHAR_ALPHANUMERIC);
    }

    public function generateNextcloudPassword(): string
    {
        return $this->generate(30, self::CHAR_EVERYTHING);
    }

    /**
     * Implementation from OCP/Security/ISecureRandom
     */

    /**
     * Flags for characters that can be used for <code>generate($length, $characters)</code>
     */
    public const CHAR_UPPER = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    public const CHAR_LOWER = 'abcdefghijklmnopqrstuvwxyz';
    public const CHAR_DIGITS = '0123456789';
    public const CHAR_SYMBOLS = '!\"#$%&\\\'()*+,-./:;<=>?@[\]^_`{|}~';
    public const CHAR_ALPHANUMERIC = self::CHAR_UPPER . self::CHAR_LOWER . self::CHAR_DIGITS;
    public const CHAR_EVERYTHING = self::CHAR_ALPHANUMERIC . self::CHAR_SYMBOLS;

    /**
     * Characters that can be used for <code>generate($length, $characters)</code>, to
     * generate human readable random strings. Lower- and upper-case characters and digits
     * are included. Characters which are ambiguous are excluded, such as I, l, and 1 and so on.
     */
    public const CHAR_HUMAN_READABLE = 'abcdefgijkmnopqrstwxyzABCDEFGHJKLMNPQRSTWXYZ23456789';

    /**
     * Generate a secure random string of specified length.
     * @param int $length The length of the generated string
     * @param string $characters An optional list of characters to use if no character list is
     * 							specified all valid base64 characters are used.
     * @return string
     * @throws LengthException if an invalid length is requested
     */
    private function generate(
        int $length,
        string $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    ): string {
        if ($length <= 0) {
            throw new LengthException('Invalid length specified: ' . $length . ' must be bigger than 0');
        }

        $maxCharIndex = strlen($characters) - 1;
        $randomString = '';

        while ($length > 0) {
            $randomNumber = random_int(0, $maxCharIndex);
            $randomString .= $characters[$randomNumber];
            $length--;
        }
        return $randomString;
    }
}
