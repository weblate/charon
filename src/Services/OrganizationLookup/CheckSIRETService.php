<?php

namespace App\Services\OrganizationLookup;

use App\Entity\NormalizedOrganization;
use League\Csv\Exception;
use League\Csv\Reader;
use League\Csv\Statement;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CheckSIRETService
{
    private HttpClientInterface $client;
    private ContainerBagInterface $params;
    private LoggerInterface $logger;
    private ?string $accessToken = null;

    public function __construct(HttpClientInterface $client, ContainerBagInterface $params, LoggerInterface $logger)
    {
        $this->client = $client;
        $this->params = $params;
        $this->logger = $logger;
    }

    /**
     * @param string $text
     * @return NormalizedOrganization[]
     * @throws ClientExceptionInterface
     * @throws ContainerExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws Exception
     * @throws NotFoundExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getInfosFromText(string $text): array
    {
        if (preg_match('/\d{14}/', $text)) {
            return $this->getSIRETInfos($text);
        }
        if (preg_match('/\d{9}/', $text)) {
            return $this->getSIRENInfos($text);
        }
        return $this->getInfosFromSearch($text);
    }

    /**
     * @return NormalizedOrganization[]
     * @throws ContainerExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ServerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getSIRETInfos(string $siret): array
    {
        $result = $this->doRequest('https://api.insee.fr/entreprises/sirene/V3/siret/' . $siret);
        return [Normalizer::normalizeSIRETResult($result["etablissement"])];
    }

    /**
     * @return NormalizedOrganization[]
     * @throws ContainerExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ServerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getSIRENInfos(string $siren): array
    {
        return $this->doRequest('https://api.insee.fr/entreprises/sirene/V3/siren/' . $siren);
    }

    /**
     * @return NormalizedOrganization[]
     * @throws ContainerExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ServerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws TransportExceptionInterface|Exception
     */
    public function getInfosFromSearch(string $search): array
    {
        $response = $this->doRequest('https://api.insee.fr/entreprises/sirene/V3/siren?q=periode(denominationUniteLegale:"' . $search . '")');
        $data = $response['unitesLegales'] ?? [];
        return array_map(function (array $result) {
            $currentPeriod = Normalizer::currentPeriodFromResult($result);
            $this->logger->debug("Finding more data for " . $currentPeriod['denominationUniteLegale']);
            $nafInfos = $this->getNAFInformation($currentPeriod["activitePrincipaleUniteLegale"]);
            $categorieJuridique = $this->getCategorieJuridique($currentPeriod['categorieJuridiqueUniteLegale']);
            return Normalizer::normalizeSIRETSearchResult([
                ...$result,
                'currentPeriod' => $currentPeriod,
                'nafInfos' => $nafInfos,
                'categorieJuridique' => $categorieJuridique
            ]);
        }, $data);
    }

    /**
     * @param string $code
     * @return array
     * @throws ClientExceptionInterface
     * @throws ContainerExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getNAFInformation(string $code): array
    {
        return $this->doRequest('https://api.insee.fr/metadonnees/V1/codes/nafr2/sousClasse/' . rawurlencode($code));
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ServerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function doRequest(string $url, int $tentative = 0): array
    {
        if (!$this->accessToken) {
            $this->getAccessToken();
        }
        $response = $this->client->request('GET', $url, [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->accessToken,
                'Accept' => 'application/json'
            ]
        ]);
        $statusCode = $response->getStatusCode();
        if ($statusCode === 401 && $tentative < 1) {
            $this->logger->debug('Found 401 response from Insee API, getting an access token and retrying.');
            $this->getAccessToken();
            return $this->doRequest($url, $tentative + 1);
        }
        if ($statusCode === 200) {
            $data = $response->toArray();
            $this->logger->info('Found NAF information', [$data]);
            return $data;
        }
        return [];
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     * @throws ServerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function getAccessToken(): void
    {
        $this->logger->debug('Getting an access token for the INSEE API');
        $consumerKey = $this->params->get('app.siret.insee_key');
        $consumerSecret = $this->params->get('app.siret.insee_secret');

        $response = $this->client->request('POST', 'https://api.insee.fr/token', [
            'headers' => [
                'Authorization' => 'Basic ' . base64_encode($consumerKey . ':' . $consumerSecret),
            ],
            // 'auth_basic' => base64_encode($consumerKey . ':' . $consumerSecret),
            'body' => ['grant_type' => 'client_credentials']
        ]);
        $content = $response->toArray();
        $this->accessToken = $content['access_token'];
        $this->logger->debug('Successfully got an access token for the INSEE API');
    }

    /**
     * @throws Exception
     */
    private function getCategorieJuridique(string $code): ?string
    {
        $finder = new Finder();
        $finder->files()
            ->in(__DIR__ . '/../../../assets/')
            ->name('cj_juillet_2020.csv');
        foreach ($finder as $file) {
            $path = $file;
        }
        if (!isset($path)) {
            return null;
        }
        $csv = Reader::createFromPath($path->getRealPath(), 'r');
        //$csv->setHeaderOffset(1);
        $stmt = Statement::create()
            ->offset(1)
            ->limit(1000)
        ;

        $records = $stmt->process($csv);
        foreach ($records as $record) {
            if ($record[0] === $code) {
                return $record[1];
            }
        }
        return null;
    }
}
