<?php

namespace App\Services\OrganizationLookup;

use App\Entity\NormalizedOrganization;

class Normalizer
{
    public static function normalizeSIRETSearchResult(array $result): NormalizedOrganization
    {
        return (new NormalizedOrganization())
            ->setSource('INSEE')
            ->setName($result['currentPeriod']["denominationUniteLegale"])
            ->setCreationYear((new \DateTimeImmutable($result["dateCreationUniteLegale"]))->format('Y'))
            ->setCategorieEntreprise($result["categorieEntreprise"])
            ->setIsESS($result['currentPeriod']["economieSocialeSolidaireUniteLegale"] === "O")
            ->setSiren($result['siren'])
            ->setObject($result['nafInfos']['intitule'] ?? null)
            ->setCategorieJuridique(trim($result['categorieJuridique']))
            ->setTrancheEffectifs($result['trancheEffectifsUniteLegale'])
        ;
    }

    public static function normalizeSIRETResult(array $result): NormalizedOrganization
    {
        return (new NormalizedOrganization())
            ->setSource('INSEE')
            ->setName($result['uniteLegale']['denominationUniteLegale'])
            ->setCreationYear((new \DateTimeImmutable($result["dateCreationEtablissement"]))->format('Y'))
            ->setCategorieEntreprise($result['uniteLegale']['categorieEntreprise'])
            ->setIsESS(null)
            ->setSiren($result['siret'])
            ->setObject(null)
            ->setCategorieJuridique($result['uniteLegale']['categorieJuridiqueUniteLegale'])
            ->setTrancheEffectifs($result['trancheEffectifsEtablissement'])
        ;
    }

    public static function normalizeRNAResult(array $result): NormalizedOrganization
    {
        return (new NormalizedOrganization())
            ->setSource('RNA')
            ->setName($result['titre'])
            ->setObject($result['objet'])
            ->setRna($result['id_association'])
            ->setCreationYear((new \DateTimeImmutable($result['date_creation']))->format('Y'))
            ->setNature($result['nature'])
            ->setIsFederation($result['groupement'] === 'F')
            ->setIsUnion($result['groupement'] === 'U')
        ;
    }

    public static function currentPeriodFromResult(array $result): array
    {
        return current(array_filter($result['periodesUniteLegale'], fn ($periode) => $periode["dateFin"] === null));
    }
}
