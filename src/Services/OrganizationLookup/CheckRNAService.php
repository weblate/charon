<?php

namespace App\Services\OrganizationLookup;

use App\Entity\NormalizedOrganization;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CheckRNAService
{
    private HttpClientInterface $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function getInfosFromText(string $text): array
    {
        if (preg_match("/^W[0-9]{9}$/", $text)) {
            return $this->getInfosFromRNA($text);
        }
        if (preg_match('/\d{14}/', $text)) {
            return $this->getInfosFromSiret($text);
        }
        return $this->getInfosFromSearch($text);
    }

    public function getInfosFromRNA(string $rna): array
    {
        return $this->doRequest('https://entreprise.data.gouv.fr/api/rna/v1/id/' . $rna);
    }

    public function getInfosFromSiret(string $siret): array
    {
        return $this->doRequest('https://entreprise.data.gouv.fr/api/rna/v1/siret/' . $siret);
    }

    /**
     * @param string $search
     * @return NormalizedOrganization[]
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getInfosFromSearch(string $search): array
    {
        $data = $this->doRequest('https://entreprise.data.gouv.fr/api/rna/v1/full_text/' . $search)['association'] ?? [];
        return array_map(fn (array $association) => Normalizer::normalizeRNAResult($association), $this->returnUniqueProperty($data, 'id_association'));
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    private function doRequest(string $url): array
    {
        $response = $this->client->request('GET', $url);
        $statusCode = $response->getStatusCode();

        if ($statusCode === 200) {
            return $response->toArray();
        }
        return [];
    }

    private function returnUniqueProperty(array $array, string $property): array
    {
        $tempArray = array_unique(array_column($array, $property));
        return array_values(array_intersect_key($array, $tempArray));
    }
}
