<?php

namespace App\Services;

use App\Entity\Submission;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class DiscourseService
{
    private HttpClientInterface $client;
    private ContainerBagInterface $params;
    private Environment $twig;
    private LoggerInterface $logger;
    private TranslatorInterface $translator;
    private AdminUrlGenerator $adminUrlGenerator;
    private ManagerRegistry $doctrine;
    private string $discourseEndpoint;
    private string $discourseUsername;
    private string $discourseAPIKey;
    private int $discourseCategory;

    public function __construct(HttpClientInterface $client, ContainerBagInterface $params, Environment $twig, LoggerInterface $logger, TranslatorInterface $translator, AdminUrlGenerator $adminUrlGenerator, ManagerRegistry $doctrine)
    {
        $this->client = $client;
        $this->params = $params;
        $this->twig = $twig;
        $this->logger = $logger;
        $this->translator = $translator;
        $this->adminUrlGenerator = $adminUrlGenerator;
        $this->doctrine = $doctrine;

        $this->discourseEndpoint = $this->params->get('app.discourse.endpoint');
        $this->discourseUsername = $this->params->get('app.discourse.username');
        $this->discourseAPIKey = $this->params->get('app.discourse.api_key');
        $this->discourseCategory = $this->params->get('app.discourse.category');
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     */
    public function postNewSubmission(Submission $submission): bool
    {
        $json = [
            'title' => $this->translator->trans("New submission: {name}", ['name' => $submission->getName()]),
            'raw' => $this->buildPostBody($submission),
            'category' => $this->discourseCategory,
        ];

        $tags = $this->buildPostTags($submission);

        if (count($tags)) {
            $json['tags'] = $tags;
        }

        $response = $this->client->request('POST', trim($this->discourseEndpoint, '/') . '/posts', [
            'headers' => [
                'Api-Key' => $this->discourseAPIKey,
                'Api-Username' => $this->discourseUsername
            ],
            'json' => $json
        ]);
        $submissionName = $submission->getName();
        $submissionId = $submission->getId();
        if ($response->getStatusCode() === 200) {
            $content = $response->toArray();
            $topicId = $content['topic_id'];
            $topicURL = $this->discourseEndpoint . '/t/' . $content['topic_slug'] . '/' . $topicId;
            $this->logger->info("Created topic for submission n°$submissionId ($submissionName) at $topicURL");

            $em = $this->doctrine->getManager();
            $submission->setMetadata([...$submission->getMetadata(), 'discourse_topic_id' => $topicId]);
            $em->persist($submission);
            $em->flush();

            return true;
        }
        $this->logger->warning("Unable to create topic for submission n°$submissionId ($submissionName)", ['response' => $response->getContent(false)]);
        return false;
    }

    private function buildPostBody(Submission $submission): string
    {
        return $this->twig->render('space/submission/discourse.md.twig', ['submission' => $submission, 'alreadyUsed' => $this->alreadyUsed($submission), 'backendAdminURL' => $this->adminUrlGenerator->backendAdminURL($submission)]);
    }

    /**
     * @param Submission $submission
     * @return string[]
     */
    private function buildPostTags(Submission $submission): array
    {
        $tags = [];
        if ($submission->getType() === Submission::SUBMISSION_TYPE_ASSOCIATION_1901) {
            $tags[] = 'association';
        }
        return $tags;
    }

    private function alreadyUsed(Submission $submission): bool
    {
        return count(array_intersect($submission->getAlreadyUsed(), [Submission::SUBMISSION_ALREADY_USED_AS_USER, Submission::SUBMISSION_ALREADY_USED_AS_ADMIN, Submission::SUBMISSION_ALREADY_USED_AT_CHATONS])) > 1;
    }
}
