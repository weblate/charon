<?php

namespace App\Services\Notifications;

use App\Entity\Invite;
use App\Entity\Space;
use App\Entity\Submission;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Security\Http\LoginLink\LoginLinkDetails;
use Symfony\Component\Translation\LocaleSwitcher;
use Symfony\Contracts\Translation\TranslatorInterface;

class Mailer
{
    private MailerInterface $mailer;
    private LocaleSwitcher $localeSwitcher;
    private TranslatorInterface $translator;
    private ?Address $from;
    private string $logEmailAddress;
    private bool $canSendEmailsToUsers;

    public function __construct(MailerInterface $mailer, LocaleSwitcher $localeSwitcher, ContainerBagInterface $params, TranslatorInterface $translator)
    {
        $this->mailer = $mailer;
        $this->localeSwitcher = $localeSwitcher;
        $this->translator = $translator;
        $this->from = new Address('noreply@frama.space', 'Frama.space');
        $this->logEmailAddress = $params->get('app.logging.email_address');
        $this->canSendEmailsToUsers = $params->get('app.email.users');
    }

    public function sendLoggingMail(string $subject, string $message): void
    {
        $email = (new Email())
            ->from($this->from)
            ->to($this->logEmailAddress)
            ->subject($subject)
            ->text($message);
        $this->mailer->send($email);
    }

    public function sendPendingEmail(Submission $submission): void
    {
        $this->localeSwitcher->setLocale($submission->getLocale());
        $email = (new TemplatedEmail())
            ->from($this->from)
            ->to(new Address($submission->getEmail(), $submission->getName()))
            ->subject($this->translator->trans('Your Frama.space registration request is pending'))
            ->htmlTemplate('emails/pending.html.twig')
        ;
        $this->send($email);
    }

    public function sendConfirmationSuccessEmail(Space $space): void
    {
        $this->localeSwitcher->setLocale($space->getLocale());
        $email = (new TemplatedEmail())
            ->from(new Address('noreply@frama.space', 'Frama.space'))
            ->to(new Address($space->getContactEmail(), $space->getContactName()))
            ->subject($this->translator->trans('Your Frama.space registration request has been approved'))
            ->htmlTemplate('emails/confirmed.html.twig')
        ;
        $this->send($email);
    }

    public function sendRejectionEmail(Submission $submission): void
    {
        $this->localeSwitcher->setLocale($submission->getLocale());
        $email = (new TemplatedEmail())
            ->from('noreply@frama.space')
            ->to(new Address($submission->getEmail(), $submission->getName()))
            ->subject($this->translator->trans('Your Frama.space registration request has been rejected'))
            ->htmlTemplate('emails/rejected.html.twig')
            ->context(['domain' => $submission->getDomain(), 'organization' => $submission->getName()])
        ;
        $this->send($email);
    }

    public function sendCreationSuccessEmail(Space $space): void
    {
        $this->localeSwitcher->setLocale($space->getLocale());
        $email = (new TemplatedEmail())
            ->from('noreply@frama.space')
            ->to(new Address($space->getContactEmail(), $space->getContactName()))
            ->subject($this->translator->trans('Your Frama.space account has been created'))
            ->htmlTemplate('emails/created.html.twig')
            ->context([
                'space' => $space,
                'organization' => $space->getOrganizationName()
            ])
        ;
        $this->send($email);
    }

    public function sendDeleteConfirmation(Space $space): void
    {
        $this->localeSwitcher->setLocale($space->getLocale());
        $email = (new TemplatedEmail())
            ->from('noreply@frama.space')
            ->to(new Address($space->getContactEmail(), $space->getContactName()))
            ->subject($this->translator->trans('Your Frama.space account has been deleted'))
            ->htmlTemplate('emails/deleted.html.twig')
            ->context([
                'space' => $space,
                'domain' => $space->getDomain() . '.frama.space',
                'complete_deletion_date' => $space->getDeletedAt()->add(new \DateInterval('P30D'))
            ])
        ;
        $this->send($email);
    }

    public function sendReactivationConfirmation(Space $space): void
    {
        $this->localeSwitcher->setLocale($space->getLocale());
        $email = (new TemplatedEmail())
            ->from('noreply@frama.space')
            ->to(new Address($space->getContactEmail(), $space->getContactName()))
            ->subject($this->translator->trans('Your Frama.space account has been reactivated'))
            ->htmlTemplate('emails/reactivation.html.twig')
            ->context([
                'space' => $space,
                'domain' => $space->getDomain() . '.frama.space',
            ])
        ;
        $this->send($email);
    }

    public function sendLoginByEmailToken(LoginLinkDetails $details, Space $space): void
    {
        $this->localeSwitcher->setLocale($space->getLocale());

        $duration = $details->getExpiresAt()->diff(new \DateTimeImmutable());

        $email = (new TemplatedEmail())
            ->from('noreply@frama.space')
            ->to(new Address($space->getContactEmail(), $space->getContactName()))
            ->subject($this->translator->trans('Sign-in on Frama.space'))
            ->htmlTemplate('emails/login.html.twig')
            ->context([
                'space' => $space,
                'organization' => $space->getOrganizationName(),
                'link' => $details->getUrl(),
                'duration' => $duration->format('%i')
            ])
        ;
        $this->send($email);
    }

    public function sendInvitation(Invite $invite): void
    {
        $email = (new TemplatedEmail())
            ->from('noreply@frama.space')
            ->to(new Address($invite->getEmail()))
            ->subject($this->translator->trans('Invitation to sign-up on Frama.space'))
            ->htmlTemplate('emails/invite.html.twig')
            ->context([
                'invite' => $invite,
            ])
        ;
        $this->send($email);
    }

    private function send(Email $email): void
    {
        if ($this->canSendEmailsToUsers) {
            $this->mailer->send($email);
        }
    }
}
