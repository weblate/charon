<?php

namespace App\Services;

use App\Controller\Admin\SubmissionCrudController;
use App\Entity\Submission;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator as RootAdminURLGenerator;

class AdminUrlGenerator
{
    private RootAdminURLGenerator $adminUrlGenerator;

    public function __construct(RootAdminURLGenerator $adminUrlGenerator)
    {
        $this->adminUrlGenerator = $adminUrlGenerator;
    }

    public function backendAdminURL(Submission $submission): string
    {
        return $this->adminUrlGenerator
            ->setController(SubmissionCrudController::class)
            ->setAction(Action::DETAIL)
            ->setEntityId($submission->getId())
            ->generateUrl();
    }
}
