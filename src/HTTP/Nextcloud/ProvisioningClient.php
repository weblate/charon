<?php

namespace App\HTTP\Nextcloud;

use App\Entity\NextcloudGroup;
use App\Entity\NextcloudUser;
use App\Entity\Space;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ProvisioningClient
{
    private HttpClientInterface $client;

    private const ENDPOINT = '/ocs/v1.php/cloud';

    public function __construct(HttpClientInterface $nextcloudProvisioningAPI)
    {
        $this->client = $nextcloudProvisioningAPI;
    }

    public function addUser(Space $space, NextcloudUser $nextcloudUser): void
    {
        $endpoint = $space->getDomain() . '.frama.space' . self::ENDPOINT;
        $response = $this->client->request('POST', $endpoint . '/users', [
            'body' => [
                'userid' => $nextcloudUser->getUserId(),
                'displayName' => $nextcloudUser->getDisplayName(),
                'email' => $nextcloudUser->getEmail(),
                'groups' => $nextcloudUser->getGroups(),
                'subadmin' => $nextcloudUser->getSubadmin(),
                'language' => $nextcloudUser->getLanguage(),
                'quota' => $nextcloudUser->getQuota()
            ]
        ]);
        $content = $response->getContent();
        $data = simplexml_load_string($content);
    }

    public function addGroup(Space $space, NextcloudGroup $nextcloudGroup): void
    {
        $endpoint = $space->getDomain() . '.frama.space' . self::ENDPOINT;
        $this->client->request('POST', $endpoint . '/groups', [
            'body' => [
                'groupid' => $nextcloudGroup->getGroupId()
            ]
        ]);
    }
}
