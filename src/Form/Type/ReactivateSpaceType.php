<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Translation\TranslatableMessage;
use Symfony\Component\Validator\Constraints\IsTrue;

class ReactivateSpaceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('confirm', CheckboxType::class, [
                'label' => new TranslatableMessage("I want to reactivate my Frama.space"),
                'required' => true,
                'constraints' => [new IsTrue()]
            ])
            ->add('save', SubmitType::class, [
                'label' => new TranslatableMessage('Reactivate my space'),
                'attr' => [
                    'class' => 'btn btn-success'
                ]
            ])
        ;
    }
}
