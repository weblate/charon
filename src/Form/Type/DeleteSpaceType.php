<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatableMessage;
use Symfony\Component\Validator\Constraints\EqualTo;
use Symfony\Component\Validator\Constraints\IsTrue;

class DeleteSpaceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('understoodConsequences', CheckboxType::class, [
                'label' => new TranslatableMessage("I've understood the consequences of my actions"),
                'required' => true,
                'constraints' => [new IsTrue()]
            ])
            ->add('madeBackups', CheckboxType::class, [
                'label' => new TranslatableMessage("I've made sure me and users from my space have retrieved everything they needed"),
                'required' => true,
                'constraints' => [new IsTrue()]
            ])
            ->add('disconnectedSyncClients', CheckboxType::class, [
                'label' => new TranslatableMessage("I've made sure me and users from my space have logged off all sync apps"),
                'required' => true,
                'constraints' => [new IsTrue()]
            ])
            ->add('subdomain', TextType::class, [
                'label' => new TranslatableMessage('Please enter the subdomain of your space to confirm the action'),
                'required' => true,
                'attr' => ['pattern' => $options['subdomain_compare'], 'placeholder' => $options['subdomain_compare']],
                'constraints' => [new EqualTo(['value' => $options['subdomain_compare']])]
            ])
            ->add('save', SubmitType::class, [
                'label' => new TranslatableMessage('Delete my space'),
                'attr' => [
                    'class' => 'btn btn-danger'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'subdomain_compare' => ''
        ]);
        $resolver->setAllowedTypes('subdomain_compare', 'string');
    }
}
