<?php

namespace App\Form\Type;

use App\Entity\Submission;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\LanguageType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Intl\Countries;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatableMessage;
use Symfony\Component\Validator\Constraints\IsTrue;

class SubmissionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            // Identification
            ->add('identity', TextType::class, [
                'label' => new TranslatableMessage("Your name (or nickname)"),
                'attr' => ['placeholder' => new TranslatableMessage('ex: « Camille »')]
            ])
            ->add('email', EmailType::class, [
                'label' => new TranslatableMessage("Administrator's account email"),
                'attr' => [
                    'placeholder' => new TranslatableMessage('ex: « camille@mon-asso.fr »'),
                    'autocomplete' => 'email'
                ]
            ])
            ->add('responsible', ChoiceType::class, [
                'label' => new TranslatableMessage('I am one of the people in charge of the organisation'),
                'multiple' => false,
                'expanded' => true,
                'choices' => [
                    'Yes' => Submission::SUBMISSION_RESPONSIBLE,
                    'No' => Submission::SUBMISSION_NOT_RESPONSIBLE,
                    "I'm not sure" => Submission::SUBMISSION_RESPONSIBLE_UNKNOWN
                ]
            ])
            ->add('alreadyUsed', ChoiceType::class, [
                'label' => new TranslatableMessage('I have already used the free software Nextcloud'),
                'label_html' => true,
                'multiple' => true,
                'expanded' => true,
                'attr' => ['class' => 'p-2 border rounded'],
                'choices' => [
                    'As an administrator' => Submission::SUBMISSION_ALREADY_USED_AS_ADMIN,
                    'As an user' => Submission::SUBMISSION_ALREADY_USED_AS_USER,
                    'At a host of the <a href="https://chatons.org">CHATONS</a> collective' => Submission::SUBMISSION_ALREADY_USED_AT_CHATONS,
                    "I am not familiar with Nextcloud" => Submission::SUBMISSION_ALREADY_USED_NOT_KNOWN,
                ]
            ])
            ->add('accountsNeeded', ChoiceType::class, [
                'label' => new TranslatableMessage('Estimated number of Framaspace accounts needed'),
                'help' => " Indiquez le nombre de comptes Framaspace dont vous estimerez avoir besoin au bout d’un an. Ce nombre peut être largement inférieur au nombre de membres (par exemple, une association de 100 adhérent⋅es pourrait n’avoir besoin que d'une quinzaine de comptes pour ses salarié⋅es et son Conseil d’administration)",
                'choices' => [
                    "- Aucun -" => "",
                    "1 à 5" => "1-5",
                    "6 à 10" => "6-10",
                    "11 à 20" => "11-20",
                    "21 à 30" => "21-30",
                    "31 à 40" => "31-40",
                    "41 à 50" => "41-50",
                    "51 à 100" => "51-100",
                    "101 à 200" => "101-200",
                    "201 à 500" => "201-500",
                    "+ de 500" => "+500"
                ]
            ])
            // Organisation
            ->add('identifier', TextType::class, [
                'label' => new TranslatableMessage('Identification number (SIREN / RNA)'),
                'help' => new TranslatableMessage("If you are a company, indicate your SIRET. If you are an association, indicate your RNA."),
                'required' => false,
                'attr' => ['placeholder' => 'ex: « 500715776 » ou « W751179246 »']
            ])
            ->add('name', TextType::class, [
                'label' => new TranslatableMessage("Name of the organization"),
                'attr' => ['placeholder' => new TranslatableMessage('For example "Framasoft" or "Graine Auvergne Rhône Alpes" or "Squat de la rue Lepic".')],
                'required' => true
            ])
            ->add('domain', TextType::class, [
                'label' => new TranslatableMessage('Framaspace identifier'),
                'required' => true
            ])
            ->add('type', ChoiceType::class, [
                'choices' => array_flip(Submission::submissionTypesToNames()),
                'label' => new TranslatableMessage("Organization type"),
                'required' => true
            ])
            ->add('extraType', TextType::class, [
                'label' => new TranslatableMessage('Please specify the type of your organisation'),
                'mapped' => false,
                'required' => false,
                'row_attr' => ['class' => 'd-none'],
                'label_attr' => ['class' => 'fw-normal fs-6 mt-2']
            ])
            ->add('country', CountryType::class, [
                'label' => new TranslatableMessage('Country of the organisation'),
                'choices' => [...array_flip(array_filter(Countries::getNames(), fn ($code) => in_array($code, ['FR', 'BE', 'CH', 'CA']), ARRAY_FILTER_USE_KEY)), 'Autre' => 'other'],
                'choice_loader' => null
            ])
            ->add('organisationLanguage', LanguageType::class, [
                'label' => new TranslatableMessage('Main language of the organisation'),
                'preferred_choices' => [
                    'Français' => 'fr',
                    'English' => 'en'
                ]
            ])
            ->add('object', TextareaType::class, [
                'label' => new TranslatableMessage("Purpose or mission"),
                'attr' => ['placeholder' => "Ex: « Framasoft est une association d'éducation populaire aux enjeux du numérique »", 'rows' => 5],
                'help' => new TranslatableMessage("Make an effort to give sufficient details (\"Cultural association\" is a bit light, for example), but remain concise (no need to write a novel). As a reminder, the purpose of an association is often indicated in its statutes.")
            ])
            ->add('topics', ChoiceType::class, [
                'choices' => [
                    'Culture' => 'culture',
                    'Sport' => 'sport',
                    'Loisirs' => 'loisirs',
                    'Social' => 'social',
                    'Amicales / Entraide' => 'amicale',
                    'Éducation / Formation' => 'education',
                    'Économie' => 'economie',
                    'Santé' => 'sante',
                    'Environnement' => 'environnement',
                    'Défense des droits fondamentaux' => 'droits',
                    'Activités politiques' => 'politique',
                    'Activités religieuses, spirituelles, philosophiques' => 'religious_philo',
                    'Recherches' => 'recherches',
                    'Tourisme' => 'tourisme',
                    'Justice' => 'justice',
                ],
                'expanded' => true,
                'multiple' => true,
                'label' => new TranslatableMessage("Fields of intervention"),
                // 'help' => "Vous pouvez indiquez un ou plusieurs domaines d'intervention de votre structure. N'hésitez pas à cocher \"Autres\" pour en indiquer d'autres.",
                'mapped' => false,
                'attr' => [
                    'class' => 'row mx-0 p-2 border rounded align-items-center',
                    'data_choice_row_class' => 'col-md-6'
                ]
            ])
            ->add('extraTopics', TextType::class, [
                'label' => new TranslatableMessage('Other'),
                'row_attr' => [
                    'class' => 'input-group input-group-sm',
                ],
                'attr' => ['placeholder' => new TranslatableMessage('e.g. "civil protection, finance, legal aid, ergonomics, etc."')],
                'mapped' => false,
                'required' => false
            ])
            ->add('mainActions', TextareaType::class, [
                'attr' => ['rows' => 5],
                'help' => "Indiquez ici, avec vos propres mots, les actions principales de l'organisation."
            ])
            ->add('creationYear', TextType::class, [
                'label' => new TranslatableMessage("Year of creation of the organization"),
                'required' => false,
                'attr' => ['placeholder' => 'ex: « 2001 »']
            ])
            ->add('website', UrlType::class, [
                'label' => new TranslatableMessage('Website'),
                'required' => false,
                'help' => 'Adresse de votre site web (si vous en avez un).',
                'attr' => ['placeholder' => new TranslatableMessage('e.g. "my-asso.com"')]
            ])
            ->add('nbBeneficiaries', IntegerType::class, [
                'label' => new TranslatableMessage('Number of beneficiaries'),
                'help' => "Selon vous, votre structure \"touche\" combien de personnes chaque année ? NB&nbsp;: ce critère n'est pas discriminatoire. Nous pouvons parfaitement sélectionner pour la phase de test de Framasoft des structures en formation qui ne touchent pas encore de public.",
                'help_html' => true,
                'required' => false
            ])
            ->add('nbEmployees', IntegerType::class, [
                'label' => new TranslatableMessage('Number of employees'),
                'help' => "Indiquez le nombre de salariés en Équivalent Temps Plein (le chiffre peut être approximatif, hein. Inutile de partir dans des calculs trop complexes)",
                'required' => false
            ])
            ->add('nbMembers', IntegerType::class, [
                'label' => new TranslatableMessage('Number of members'),
                'help' => new TranslatableMessage("If your organisation is an association or collective, indicate the number of members in your organisation."),
                'required' => false
            ])
            ->add('budget', MoneyType::class, [
                'label' => new TranslatableMessage('Annual budget'),
                'help' => new TranslatableMessage("If your organisation has an annual budget, please indicate it here (even approximately). If the organisation is a company, please indicate the \"accounting\" amount of the income, of your profit and loss account"),
                'required' => false,
                'html5' => true
            ])

            // Raisons
            ->add('reasons', TextareaType::class, [
                'label' => new TranslatableMessage("Reasons for needing a frama.space account"),
                'attr' => ['placeholder' => "En tant que responsable de mon organisation, je souhaite ouvrir un compte Framaspace parce que…", 'rows' => 3],
                'required' => false
            ])
            ->add('acceptContact', CheckboxType::class, [
                'required' => true,
                'mapped' => false,
                'label' => new TranslatableMessage("I agree to be contacted by Framasoft"),
                'constraints' => [new IsTrue()]
            ])
            ->add('betaWarning', CheckboxType::class, [
                'required' => true,
                'mapped' => false,
                'label' => "J'accepte d'être contacté au titre de la phase de beta test",
                'constraints' => [new IsTrue()]
            ])
            ->add('acceptCGU', CheckboxType::class, [
                'required' => true,
                'mapped' => false,
                'label' => new TranslatableMessage("I have read and accept the ToS of the Framasoft association"),
                'help' => "Nos Conditions Générales d'Utilisations se trouvent à l'adresse suivante : <a href=\"https://framasoft.org/fr/cgu/\" target='_blank' rel='noopener noreferrer'>https://framasoft.org/fr/cgu/</a>. Si vous ne respectez pas la loi, nous nous réservons évidemment le droit de suspendre votre compte.",
                'help_html' => true,
                'constraints' => [new IsTrue()]
            ])
            ->add('acceptCSU', CheckboxType::class, [
                'required' => true,
                'mapped' => false,
                'label' => new TranslatableMessage("I have read and accept the STU of the Framaspace service"),
                'help' => "Nos Conditions Spécifiques d'Utilisation se trouvent à l'adresse suivante : <a href=\"https://mypads.framapad.org/p/conditions-specifiques-d-utilisation-l548w762\" target='_blank' rel='noopener noreferrer'>https://mypads.framapad.org/p/conditions-specifiques-d-utilisation-l548w762</a> Si vous ne respectez pas ces conditions, nous nous réservons évidemment le droit de suspendre ou supprimer votre compte.",
                'help_html' => true,
                'constraints' => [new IsTrue()]
            ])
            ->add('save', SubmitType::class, [
                'attr' => ['class' => 'btn btn-primary btn-lg text-uppercase d-inline-flex'],
                'label' => new TranslatableMessage('Send application')
            ]);
    }

//    public function getBlockPrefix(): string
//    {
//        return 'createSubmission';
//    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Submission::class,
            'attr' => ['class' => 'new-submission-form']
        ]);
    }
}
