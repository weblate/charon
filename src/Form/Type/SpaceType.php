<?php

namespace App\Form\Type;

use App\Entity\Space;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatableMessage;
use Twig\Environment;

class SpaceType extends AbstractType
{
    private Environment $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            // Identification
            ->add('contactName', TextType::class, [
                'label' => new TranslatableMessage("Your name (or nickname)")
            ])
            ->add('contactEmail', EmailType::class, [
                'label' => new TranslatableMessage("Your email")
            ])
            // Organisation
            ->add('organizationName', TextType::class, [
                'label' => new TranslatableMessage("Name of the organization"),
            ])
            ->add('office', ChoiceType::class, [
                'label' => new TranslatableMessage('Office solution type'),
                'row_attr' => ['class' => 'mb-3'],
                'expanded' => true,
                'multiple' => false,
                'choices' => [
                    'OnlyOffice' => Space::SPACE_OFFICE_TYPE_ONLYOFFICE,
                    'CollaboraOffice' => Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE
                ],
                'label_html' => true,
                'choice_label' => function (int $choice) {
                    return match ($choice) {
                        Space::SPACE_OFFICE_TYPE_ONLYOFFICE => $this->twig->render('space/office/onlyoffice.html.twig'),
                        Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE => $this->twig->render('space/office/collabora_online.html.twig'),
                        default => null,
                    };
                },
                'help' => new TranslatableMessage('We give more details and how to choose between the two solutions <a target="_blank" rel="noopener noreferrer" href="https://www.frama.space/abc/faq#q-text-editor">in our FAQ</a>.'),
                'help_html' => true
            ])
            ->add('save', SubmitType::class, ['label' => new TranslatableMessage('Save')])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Space::class,
        ]);
    }
}
