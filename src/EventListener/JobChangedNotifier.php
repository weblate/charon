<?php

namespace App\EventListener;

use App\Entity\Job;
use App\SpaceManager;

class JobChangedNotifier
{
    protected SpaceManager $spaceManager;

    public function __construct(SpaceManager $spaceManager)
    {
        $this->spaceManager = $spaceManager;
    }

    public function postUpdate(Job $job): void
    {
        switch ($job->getStatus()) {
            case Job::JOB_STATUS_DONE:
                $this->handleJobDone($job);
                break;
            case Job::JOB_STATUS_ERROR:
                $this->handleJobError($job);
                break;
        }
    }

    private function handleJobDone(Job $job): void
    {
        switch ($job->getType()) {
            case Job::JOB_TYPE_DELETE_SPACE:
                $this->spaceManager->deleteSpace($job->getSpace());
                break;
            case Job::JOB_TYPE_DISABLE_SPACE:
                $this->spaceManager->postDisableSpace($job->getSpace());
                break;
            case Job::JOB_TYPE_REENABLE_SPACE:
                $this->spaceManager->postReenableSpace($job->getSpace());
                break;
            case Job::JOB_TYPE_CHANGE_OFFICE:
                $this->spaceManager->postChangeOfficeType($job->getSpace());
                break;
        }
    }

    private function handleJobError(Job $job): void
    {
        // send mail
    }
}
