<?php

namespace App\EventListener\EasyAdmin;

use App\Entity\Invite;
use App\Services\Notifications\Mailer;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Translation\TranslatableMessage;

#[AsEventListener(event: AfterEntityPersistedEvent::class, method: 'handleInvitePersisted')]
final class BeforeEntityPersistedEventListener
{
    private Mailer $mailer;
    private ObjectManager $em;
    private ?FlashBagInterface $flashBag = null;
    private UrlGeneratorInterface $router;

    public function __construct(Mailer $mailer, ManagerRegistry $doctrine, RequestStack $requestStack, UrlGeneratorInterface $router)
    {
        $this->mailer = $mailer;
        $this->em = $doctrine->getManager();
        $session = $requestStack->getSession();
        if ($session instanceof Session) {
            $this->flashBag = $session->getFlashBag();
        }
        $this->router = $router;
    }

    public function handleInvitePersisted(AfterEntityPersistedEvent $event): void
    {
        $invite = $event->getEntityInstance();

        if (!$invite instanceof Invite || $this->flashBag === null) {
            return;
        }

        $url = $this->router->generate('app_index_newsubmission', ['inviteToken' => $invite->getToken()], UrlGeneratorInterface::ABSOLUTE_URL);

        /** @var Invite $invite */
        if ($invite->getSentAt() === null && $invite->getEmail() !== null) {
            $this->flashBag->add('success', new TranslatableMessage('The following invite link has been sent to {mail}: {link}. You may also forward the link directly to the organisation you wish to invite.', ['mail' => $invite->getEmail(), 'link' => '<a target="_blank" href="'  . $url . '">' . $url . '</a>']));
            $this->mailer->sendInvitation($invite);
            $this->em->persist($invite);
            $this->em->flush();
        } else {
            $this->flashBag->add('success', new TranslatableMessage('The following invite link has been created: {link}. You may forward it to the organisation you wish to invite.', ['link' => '<a target="_blank" href="'  . $url . '">' . $url . '</a>']));
        }
    }
}
