<?php

namespace App\EventListener;

use App\Events\SubmissionCreatedEvent;
use App\Services\AdminUrlGenerator;
use App\Services\Notifications\Mailer;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;

#[AsEventListener(event: SubmissionCreatedEvent::class, method: 'handleSubmissionCreated')]
final class SubmissionChangedListener
{
    private Mailer $mailer;
    private AdminUrlGenerator $adminUrlGenerator;

    public function __construct(Mailer $mailer, AdminUrlGenerator $adminUrlGenerator)
    {
        $this->mailer = $mailer;
        $this->adminUrlGenerator = $adminUrlGenerator;
    }

    public function handleSubmissionCreated(SubmissionCreatedEvent $event): void
    {
        $submission = $event->getSubmission();
        $this->mailer->sendPendingEmail($submission);
        $adminBackendURL = $this->adminUrlGenerator->backendAdminURL($submission);
        $this->mailer->sendLoggingMail('Submission for ' . $submission->getName() . 'was created', $submission->getIdentity() . "created submission request n°" . $submission->getId() . " for organisation " . $submission->getName() . "\n\nDetails of the submission can be found on " . $adminBackendURL . "\n\nCheers,\n\nCharon, for the Framaspace team.");
    }
}
