<?php

namespace App;

use App\Entity\Job;
use App\Entity\Space;
use App\Entity\Submission;
use App\Repository\SpaceRepository;
use App\Services\Nextcloud\Generator;
use App\Services\Notifications\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

class SpaceManager
{
    private EntityManagerInterface $entityManager;
    private SpaceRepository $spaceRepository;
    private ContainerBagInterface $params;
    private Generator $generator;
    private Mailer $mailer;

    public const DEFAULT_ADMIN_USERNAME = 'admin';
    public const DEFAULT_REMOTE_ADMIN_USERNAME = 'framadmin';

    public function __construct(EntityManagerInterface $entityManager, SpaceRepository $spaceRepository, ContainerBagInterface $params, Generator $generator, Mailer $mailer)
    {
        $this->entityManager = $entityManager;
        $this->spaceRepository = $spaceRepository;
        $this->params = $params;
        $this->generator = $generator;
        $this->mailer = $mailer;
    }

    public function createSpaceFromSubmission(Submission $submission): Space
    {
        $officeServer = $this->params->get('app.office.default_server');

        $details = [
            // Nextcloud details
            'nc_instanceid' => $this->generator->generateInstanceId(),
            'nc_admin_username' => self::DEFAULT_ADMIN_USERNAME,
            'nc_admin_email' => $submission->getEmail(),
            'nc_remote_admin_username' => self::DEFAULT_REMOTE_ADMIN_USERNAME,
            'nc_remote_admin_password' => $this->generator->generateNextcloudPassword(),
            'nc_passwordsalt' => $this->generator->generatePasswordSalt(),
            'nc_secret' => $this->generator->generateSecret(),

            // Common office details
            'office_port' => $this->spaceRepository->findBiggestPortForOfficeServer($officeServer),
            'office_server' => $officeServer,

            // OnlyOffice details
            'oo_secret' => $this->generator->generatePasswordSalt(),

            // High performance notify push server
            'nc_hp_backend' => $this->spaceRepository->findBiggestNcHpBackendPort() + 1,

        ];

        $space = (new Space())
            ->setDomain($submission->getDomain())
            ->setCreatedAt(new \DateTimeImmutable())
            ->setUpdatedAt(new \DateTimeImmutable())
            ->setContactEmail($submission->getEmail())
            ->setContactName($submission->getName())
            ->setLocale($submission->getLocale())
            ->setDetails($details)
            ->setOrganizationName($submission->getName())
            ->setSubmission($submission)
        ;

        $this->entityManager->persist($space);

        $job = (new Job())
            ->setCreatedAt(new \DateTimeImmutable())
            ->setUpdatedAt(new \DateTimeImmutable())
            ->setSpace($space)
            ->setType(Job::JOB_TYPE_CREATE_SPACE)
            ->setData([...$details, 'domain' => $submission->getDomain(), 'office_type' => $space->getOffice()]);

        $this->entityManager->persist($job);

        $this->entityManager->flush();

        $this->mailer->sendConfirmationSuccessEmail($space);
        return $space;
    }

    public function postChangeOfficeType(Space $space): void
    {
        // send emails?
    }

    public function preDisableSpace(Space $space): void
    {
        $space->setEnabled(false);
        $space->setDeletedAt(new \DateTimeImmutable());
        $this->entityManager->persist($space);
    }

    public function postDisableSpace(Space $space): void
    {
        $this->mailer->sendDeleteConfirmation($space);
    }

    public function preReenableSpace(Space $space): void
    {
        $space->setEnabled(true);
        $space->setDeletedAt(null);
        $this->entityManager->persist($space);
    }

    public function postReenableSpace(Space $space): void
    {
        // Send mails
    }

    public function deleteSpace(Space $space): void
    {
        // TODO: probably not delete, just remove informations and keep the domain name
        $this->entityManager->remove($space);
        // Send mails
    }
}
