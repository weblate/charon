<?php

namespace App\Components;

use Symfony\Component\Form\FormView;
use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent('section', template: "space/submission/section.html.twig")]
class SubmissionFormSectionComponent
{
    public int $step;
    public string $id;
    public string $title;
    public string $tooltip;
}
