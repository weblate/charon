<?php

namespace App\Components;

use Symfony\Component\Form\FormView;
use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent('label', template: "space/submission/label.html.twig")]
class LabelComponent
{
    public FormView $field;
    public ?string $tooltip;
    public bool $inline_tooltip = false;
}
