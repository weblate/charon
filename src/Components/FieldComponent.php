<?php

namespace App\Components;

use Symfony\Component\Form\FormView;
use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent('field', template: "space/submission/field.html.twig")]
class FieldComponent
{
    public FormView $field;
    public string $tooltip;
    public ?string $inputGroup = null;
    public bool $inline_tooltip = false;
}
