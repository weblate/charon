<?php

namespace App\Controller;

use App\Entity\Invite;
use App\Entity\Submission;
use App\Events\SubmissionCreatedEvent;
use App\Form\Type\SubmissionType;
use DateTimeImmutable;
use Doctrine\Persistence\ManagerRegistry;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class IndexController extends AbstractController
{
    #[Route('/')]
    public function index(): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('manage_space');
        }
        return $this->render('index.html.twig', [
        ]);
    }

    #[Route('/new')]
    public function newSubmission(Request $request, ManagerRegistry $doctrine, EventDispatcherInterface $dispatcher, ContainerBagInterface $params): Response
    {
        $submission = new Submission();
        $submission->setCreatedAt(new DateTimeImmutable());
        $submission->setUpdatedAt(new DateTimeImmutable());
        $submission->setLocale($request->getLocale());
        $form = $this->createForm(SubmissionType::class, $submission);

        $inviteToken = $request->query->get('inviteToken');
        $invite = $doctrine->getRepository(Invite::class)->findOneBy(['token' => $inviteToken]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $submission->setTopics([...$form->get('topics')->getData(), $form->get('extraTopics')->getData()]);
            if ($form->get('type')->getData() === Submission::SUBMISSION_TYPE_OTHER) {
                $submission->setType($form->get('extraType')->getData());
            }

            $entityManager = $doctrine->getManager();


            /** @var Invite $invite */
            if ($invite && $invite->getUsedAt() === null) {
                $invite->setUsedAt(new \DateTimeImmutable());
                $entityManager->persist($invite);
                $submission->setInvite($invite);
            }

            $entityManager->persist($submission);
            $entityManager->flush();
            $dispatcher->dispatch(new SubmissionCreatedEvent($submission));
            return $this->redirectToRoute('app_index_after_new_submission');
        }

        /** @var bool $submissionsOpened */
        $submissionsOpened = $params->get('app.submissions.opened');
        if ($submissionsOpened) {
            return $this->renderForm('space/new.html.twig', ['form' => $form, 'invite' => $invite]);
        }
        return $this->redirect($request->getBaseUrl() . '/abc');
    }

    #[Route('/new/done', name: 'app_index_after_new_submission')]
    public function afterNewSubmission(): Response
    {
        return $this->render('space/submission/done.html.twig');
    }
}
