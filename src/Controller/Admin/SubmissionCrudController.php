<?php

namespace App\Controller\Admin;

use App\Admin\Field\InviteField;
use App\Admin\Field\ScoreField;
use App\Entity\Submission;
use App\Message\DiscoursePostAction;
use App\Services\Notifications\Mailer;
use App\SpaceManager;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\BatchActionDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\LanguageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\LocaleField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Translation\TranslatableMessage;

use function Symfony\Component\Translation\t;

class SubmissionCrudController extends AbstractCrudController
{
    private SpaceManager $spaceManager;
    private Mailer $mailer;
    private MessageBusInterface $bus;

    public function __construct(SpaceManager $spaceManager, Mailer $mailer, MessageBusInterface $bus)
    {
        $this->spaceManager = $spaceManager;
        $this->mailer = $mailer;
        $this->bus = $bus;
    }

    public static function getEntityFqcn(): string
    {
        return Submission::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return parent::configureCrud($crud)
            ->setPageTitle('index', new TranslatableMessage('Submissions'))
            ->setDefaultSort(['createdAt' => 'DESC'])
            ->setEntityLabelInSingular(new TranslatableMessage('Submission'))
            ->setEntityLabelInSingular(new TranslatableMessage('Submissions'))
        ;
    }

    public function configureActions(Actions $actions): Actions
    {
        $approveAction = Action::new('approveSubmission', new TranslatableMessage('Approve Submission'), 'fa-solid fa-thumbs-up')
            ->linkToCrudAction('approveSubmission')
            ->setCssClass('text-success')
            ->displayIf(static function (Submission $entity) {
                return $entity->getStatus() !== Submission::SUBMISSION_STATUS_APPROVED;
            })
        ;

        $rejectAction = Action::new('rejectSubmission', new TranslatableMessage('Reject Submission'), 'fa-solid fa-thumbs-down')
            ->linkToCrudAction('rejectSubmission')
            ->setCssClass('text-danger')
            ->displayIf(static function (Submission $entity) {
                return $entity->getStatus() === Submission::SUBMISSION_STATUS_PENDING;
            })
        ;

        $createDiscussionAction = Action::new('createDiscussionOnDiscourse', new TranslatableMessage('Create discussion'), 'fa-solid fa-comment')
            ->linkToCrudAction('createDiscussionOnDiscourse')
            ->setCssClass('text-info')
            ->displayIf(static function (Submission $entity) {
                return $entity->getStatus() === Submission::SUBMISSION_STATUS_PENDING;
            })
        ;

        $approveMultipleAction = Action::new('approveMultipleSubmission', new TranslatableMessage('Approve submissions'), 'fa-solid fa-thumbs-up')
            ->linkToCrudAction('approveMultipleSubmission')
            ->setCssClass('btn btn-success')
            ->displayIf(static function (Submission $entity) {
                return $entity->getStatus() !== Submission::SUBMISSION_STATUS_APPROVED;
            })
        ;

        $rejectMultipleAction = Action::new('rejectMultipleSubmission', new TranslatableMessage('Reject submissions'), 'fa-solid fa-thumbs-down')
            ->linkToCrudAction('rejectMultipleSubmission')
            ->setCssClass('btn btn-warning')
            ->displayIf(static function (Submission $entity) {
                return $entity->getStatus() === Submission::SUBMISSION_STATUS_PENDING;
            })
        ;

        $createMultipleDiscussionsAction = Action::new('createMultipleDiscussionsOnDiscourse', new TranslatableMessage('Create discussions'), 'fa-solid fa-comment')
            ->linkToCrudAction('createMultipleDiscussionsOnDiscourse')
            ->setCssClass('btn btn-info')
            ->displayIf(static function (Submission $entity) {
                return $entity->getStatus() === Submission::SUBMISSION_STATUS_PENDING;
            })
        ;

        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_INDEX, $approveAction)
            ->add(Crud::PAGE_EDIT, $approveAction)
            ->add(Crud::PAGE_DETAIL, $approveAction)
            ->add(Crud::PAGE_INDEX, $rejectAction)
            ->add(Crud::PAGE_EDIT, $rejectAction)
            ->add(Crud::PAGE_DETAIL, $rejectAction)
            ->add(Crud::PAGE_INDEX, $createDiscussionAction)
            ->add(Crud::PAGE_EDIT, $createDiscussionAction)
            ->add(Crud::PAGE_DETAIL, $createDiscussionAction)
            ->addBatchAction($approveMultipleAction)
            ->addBatchAction($rejectMultipleAction)
            ->addBatchAction($createMultipleDiscussionsAction)
            ->reorder(
                Crud::PAGE_INDEX,
                [
                Action::DETAIL,
                'approveSubmission',
                'rejectSubmission',
                'createDiscussionOnDiscourse',
                Action::EDIT,
                Action::DELETE,
                'approveMultipleSubmission',
                'rejectMultipleSubmission',
                'createMultipleDiscussionsOnDiscourse',
                Action::BATCH_DELETE]
            )
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            ScoreField::new('score')->setLabel(new TranslatableMessage('Score'))
                ->hideOnForm(),
            InviteField::new('invite')->setLabel(new TranslatableMessage('Invite'))->onlyOnIndex(),
            EmailField::new('email')->hideOnIndex()->setLabel(new TranslatableMessage('Email')),
            TextField::new('identity')->setLabel(new TranslatableMessage('Identity')),
            ChoiceField::new('responsible')->setLabel(new TranslatableMessage('Responsible'))
                ->renderAsBadges([
                    Submission::SUBMISSION_NOT_RESPONSIBLE => 'warning',
                    Submission::SUBMISSION_RESPONSIBLE => 'success',
                    Submission::SUBMISSION_RESPONSIBLE_UNKNOWN => 'warning',
                ])
                ->allowMultipleChoices(false)
                ->renderExpanded()
                ->setTranslatableChoices([
                    Submission::SUBMISSION_RESPONSIBLE => new TranslatableMessage('Yes'),
                    Submission::SUBMISSION_NOT_RESPONSIBLE => new TranslatableMessage('No'),
                    Submission::SUBMISSION_RESPONSIBLE_UNKNOWN => new TranslatableMessage("I'm not sure")
                ]),
            ChoiceField::new('alreadyUsed')->setLabel(new TranslatableMessage('Already used Nextcloud'))
                ->renderAsBadges([
                    Submission::SUBMISSION_ALREADY_USED_AS_USER => 'info',
                    Submission::SUBMISSION_ALREADY_USED_AS_ADMIN => 'success',
                    Submission::SUBMISSION_ALREADY_USED_AT_CHATONS => 'warning',
                    Submission::SUBMISSION_ALREADY_USED_NOT_KNOWN => 'info',
                ])
                ->allowMultipleChoices()
                ->renderExpanded()
                ->setTranslatableChoices([
                    Submission::SUBMISSION_ALREADY_USED_AS_USER => new TranslatableMessage('As an user'),
                    Submission::SUBMISSION_ALREADY_USED_AS_ADMIN => new TranslatableMessage('As an administrator'),
                    Submission::SUBMISSION_ALREADY_USED_AT_CHATONS => new TranslatableMessage('At a CHATONS'),
                    Submission::SUBMISSION_ALREADY_USED_NOT_KNOWN => new TranslatableMessage('I am not familiar with Nextcloud')
                ]),
            ChoiceField::new('accountsNeeded')->setLabel(new TranslatableMessage('Accounts needed'))
                ->renderAsBadges([
                    '1-5' => 'success',
                    '6-10' => 'success',
                    '11-20' => 'info',
                    '21-30' => 'info',
                    '31-40' => 'info',
                    '41-50' => 'warning',
                    '51-100' => 'danger',
                    '101-200' => 'danger',
                    '201-500' => 'danger',
                    '+500' => 'danger'
                ])
                ->setChoices([
                    "- Aucun -" => "",
                    "1 à 5" => "1-5",
                    "6 à 10" => "6-10",
                    "11 à 20" => "11-20",
                    "21 à 30" => "21-30",
                    "31 à 40" => "31-40",
                    "41 à 50" => "41-50",
                    "51 à 100" => "51-100",
                    "101 à 200" => "101-200",
                    "201 à 500" => "201-500",
                    "+ de 500" => "+500"
                ]),
            TextField::new('identifier')->hideOnIndex()->setLabel(new TranslatableMessage('Identifier')),
            TextField::new('name')->setLabel(new TranslatableMessage('Name')),
            TextField::new('domain')->setLabel(new TranslatableMessage('Framaspace identifier')),
            ChoiceField::new('type')->setLabel(new TranslatableMessage('Organization type'))
                ->renderAsBadges([
                    Submission::SUBMISSION_TYPE_COLLECTIVE => 'info',
                    Submission::SUBMISSION_TYPE_ASSOCIATION_1901 => 'success',
                    Submission::SUBMISSION_TYPE_ASSOCIATION_1907 => 'info',
                    Submission::SUBMISSION_TYPE_SYNDICATE => 'info',
                    Submission::SUBMISSION_TYPE_SCOP => 'warning',
                    Submission::SUBMISSION_TYPE_SCIC => 'warning',
                    Submission::SUBMISSION_TYPE_ENTREPRISE => 'danger',
                    Submission::SUBMISSION_TYPE_PUBLIC => 'danger',
                    Submission::SUBMISSION_TYPE_OTHER => 'info'
                ])
                ->allowMultipleChoices(false)
                ->renderExpanded()
                ->setTranslatableChoices(Submission::submissionTypesToNames()),
            TextField::new('creationYear')->hideOnIndex()->setLabel(new TranslatableMessage('Creation year')),
            LanguageField::new('organisationLanguage')->setLabel(new TranslatableMessage('Main language of the organisation'))->hideOnIndex(),
            TextEditorField::new('object')->setLabel(new TranslatableMessage('Purpose or mission'))->setTemplatePath('bundles/EasyAdminBundle/text_editor.twig'),
            TextEditorField::new('mainActions')->setLabel(new TranslatableMessage('Main actions'))->setTemplatePath('bundles/EasyAdminBundle/text_editor.twig'),
            ArrayField::new('topics')->setLabel(new TranslatableMessage('Topics'))->setRequired(false),
            TextEditorField::new('reasons')->setLabel(new TranslatableMessage('Reasons for needing a frama.space account'))->setTemplatePath('bundles/EasyAdminBundle/text_editor.twig')->hideOnIndex(),
            UrlField::new('website')->setLabel(new TranslatableMessage('Website')),
            IntegerField::new('nbBeneficiaries')->hideOnIndex()->setLabel(new TranslatableMessage('Number of beneficiaries')),
            IntegerField::new('nbEmployees')->hideOnIndex()->setLabel(new TranslatableMessage('Number of employees')),
            IntegerField::new('nbMembers')->hideOnIndex()->setLabel(new TranslatableMessage('Number of members')),
            LocaleField::new('locale')->setLabel(new TranslatableMessage('Locale'))->setRequired(false)->hideOnIndex(),
            MoneyField::new('budget')->setLabel(new TranslatableMessage('Annual budget'))->setCurrency('EUR'),

            AssociationField::new('space')->setLabel(new TranslatableMessage('Space'))->hideOnForm()->hideOnIndex(),
            ChoiceField::new('status')->setLabel(new TranslatableMessage('Status'))
                ->renderExpanded()
                ->renderAsBadges([
                    Submission::SUBMISSION_STATUS_PENDING => 'info',
                    Submission::SUBMISSION_STATUS_APPROVED => 'success',
                    Submission::SUBMISSION_STATUS_REJECTED => 'danger'
                ])
                ->setTranslatableChoices([
                    Submission::SUBMISSION_STATUS_PENDING => t('Pending'),
                    Submission::SUBMISSION_STATUS_APPROVED => t('Approved'),
                    Submission::SUBMISSION_STATUS_REJECTED => t('Rejected')
            ]),
            AssociationField::new('invite')->setLabel(new TranslatableMessage('Invite'))->hideOnIndex(),
            DateTimeField::new('createdAt')->setLabel(new TranslatableMessage('Created at'))->onlyOnDetail()->onlyOnIndex(),
            DateTimeField::new('updatedAt')->setLabel(new TranslatableMessage('Updated at'))->onlyOnDetail()
        ];
    }

    public function approveSubmission(AdminContext $context): RedirectResponse
    {
        $entity = $context->getEntity();
        $entityManager = $this->container->get('doctrine')->getManagerForClass($entity->getFqcn());
        /** @var Submission $submission */
        $submission = $entity->getInstance();

        $submission->setStatus(Submission::SUBMISSION_STATUS_APPROVED);

        $entityManager->persist($submission);

        $space = $this->spaceManager->createSpaceFromSubmission($submission);

        $url = $this->container->get(AdminUrlGenerator::class)
            ->setController(SpaceCrudController::class)
            ->setAction(Action::DETAIL)
            ->setEntityId($space->getId())
            ->generateUrl();

        return $this->redirect($url);
    }

    public function rejectSubmission(AdminContext $context): RedirectResponse
    {
        $entity = $context->getEntity();
        $entityManager = $this->container->get('doctrine')->getManagerForClass($entity->getFqcn());
        /** @var Submission $submission */
        $submission = $entity->getInstance();

        $submission->setStatus(Submission::SUBMISSION_STATUS_REJECTED);
        $entityManager->persist($submission);

        $this->mailer->sendRejectionEmail($submission);

        $url = $this->container->get(AdminUrlGenerator::class)
            ->setController(SubmissionCrudController::class)
            ->setAction(Action::DETAIL)
            ->setEntityId($submission->getId())
            ->generateUrl();

        return $this->redirect($url);
    }

    public function createDiscussionOnDiscourse(AdminContext $context): RedirectResponse
    {
        $entity = $context->getEntity();
        /** @var Submission $submission */
        $submission = $entity->getInstance();

        $this->bus->dispatch(new DiscoursePostAction($submission->getId()));

        $url = $this->container->get(AdminUrlGenerator::class)
            ->setController(SubmissionCrudController::class)
            ->setAction(Action::DETAIL)
            ->setEntityId($submission->getId())
            ->generateUrl();

        return $this->redirect($url);
    }

    public function approveMultipleSubmission(BatchActionDto $batchActionDto): RedirectResponse
    {
        $className = $batchActionDto->getEntityFqcn();
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->container->get('doctrine')->getManagerForClass($className);
        foreach ($batchActionDto->getEntityIds() as $id) {
            /** @var Submission $submission */
            $submission = $entityManager->find($className, $id);
            $submission->setStatus(Submission::SUBMISSION_STATUS_APPROVED);
            $entityManager->persist($submission);
            $this->spaceManager->createSpaceFromSubmission($submission);
        }

        $entityManager->flush();

        return $this->redirect($batchActionDto->getReferrerUrl());
    }

    public function rejectMultipleSubmission(BatchActionDto $batchActionDto): RedirectResponse
    {
        $className = $batchActionDto->getEntityFqcn();
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->container->get('doctrine')->getManagerForClass($className);
        foreach ($batchActionDto->getEntityIds() as $id) {
            /** @var Submission $submission */
            $submission = $entityManager->find($className, $id);
            $submission->setStatus(Submission::SUBMISSION_STATUS_REJECTED);
            $entityManager->persist($submission);
            $this->mailer->sendRejectionEmail($submission);
        }

        $entityManager->flush();

        return $this->redirect($batchActionDto->getReferrerUrl());
    }

    public function createMultipleDiscussionsOnDiscourse(BatchActionDto $batchActionDto): RedirectResponse
    {
        $className = $batchActionDto->getEntityFqcn();
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->container->get('doctrine')->getManagerForClass($className);
        foreach ($batchActionDto->getEntityIds() as $id) {
            $this->bus->dispatch(new DiscoursePostAction($id));
        }

        $entityManager->flush();

        return $this->redirect($batchActionDto->getReferrerUrl());
    }
}
