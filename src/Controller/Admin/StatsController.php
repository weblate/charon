<?php

namespace App\Controller\Admin;

use App\Entity\Space;
use App\Entity\Submission;
use App\Repository\SpaceRepository;
use App\Repository\SubmissionRepository;
use IntlDateFormatter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

class StatsController extends AbstractController
{
    private SpaceRepository $spaceRepository;
    private ChartBuilderInterface $chartBuilder;
    private SubmissionRepository $submissionRepository;
    private TranslatorInterface $translator;

    public function __construct(ChartBuilderInterface $chartBuilder, SubmissionRepository $submissionRepository, SpaceRepository $spaceRepository, TranslatorInterface $translator)
    {
        $this->chartBuilder = $chartBuilder;
        $this->submissionRepository = $submissionRepository;
        $this->spaceRepository = $spaceRepository;
        $this->translator = $translator;
    }

    #[Route('/admin/stats', name: 'admin_stats')]
    public function index(): Response
    {
        return $this->render('admin/stats.html.twig', [
            'approved_submissions_by_type' => $this->generateApprovedSubmissionByTypeChart(),
            'new_spaces_by_month' => $this->generateNewSpacesByMonth(),
            'space_office_type' => $this->generateSpaceOfficeTypeChart(),
        ]);
    }

    private function generateApprovedSubmissionByTypeChart(): Chart
    {
        $data = array_reduce($this->submissionRepository->countApprovedSubmissionsByType(), function ($acc, $result) {
            $acc[$result['type']] = $result['submissionCount'];
            return $acc;
        }, []);
        $chart = $this->chartBuilder->createChart(Chart::TYPE_DOUGHNUT);

        $chart->setData([
            'labels' => array_map(fn ($elem): string => Submission::submissionTypesToNames()[$elem], array_keys($data)),
            'datasets' => [
                [
                    'label' => 'My First dataset',
                    'backgroundColor' => [
                        'rgb(255, 99, 132)',
                        'rgb(54, 162, 235)',
                        'rgb(255, 205, 86)'
                    ],
                    // 'borderColor' => 'rgb(255, 99, 132)',
                    'data' => array_values($data),
                ],
            ],
        ]);

        $chart->setOptions([
            'plugins' => [
                'title' => [
                    'display' => true,
                    'text' => $this->translator->trans('Approved submissions by structure type')
                ]
            ]
        ]);
        return $chart;
    }

    private function generateNewSpacesByMonth(): Chart
    {
        $data = array_reduce($this->spaceRepository->countNewSpacesByMonths(), function ($acc, $result) {
            $acc[$result['createdAtMonth']] = $result['spaceCount'];
            return $acc;
        }, []);
        $chart = $this->chartBuilder->createChart(Chart::TYPE_BAR);

        $monthFormatter = new IntlDateFormatter('fr_FR', IntlDateFormatter::FULL, IntlDateFormatter::FULL, 'Europe/Paris', IntlDateFormatter::GREGORIAN, 'LLLL y');

        $chart->setData([
            'labels' => array_map(fn ($elem) => $monthFormatter->format(new \DateTime($elem)), array_keys($data)),
            'datasets' => [
                [
                    'label' => 'My First dataset',
                    'backgroundColor' => [
                        'rgb(255, 99, 132)',
                        'rgb(54, 162, 235)',
                        'rgb(255, 205, 86)'
                    ],
                    // 'borderColor' => 'rgb(255, 99, 132)',
                    'data' => array_values($data),
                ],
            ],
        ]);

        $chart->setOptions([
            'plugins' => [
                'title' => [
                    'display' => true,
                    'text' => $this->translator->trans('Number of new spaces by month')
                ]
            ]
        ]);
        return $chart;
    }

    private function generateSpaceOfficeTypeChart(): Chart
    {
        $data = array_reduce($this->spaceRepository->mesureOfficeTypeDistribution(), function ($acc, $result) {
            $acc[$result['office']] = $result['spaceCount'];
            return $acc;
        }, []);
        $chart = $this->chartBuilder->createChart(Chart::TYPE_PIE);

        $chart->setData([
            'labels' => array_map(fn ($elem) => match ($elem) {
                Space::SPACE_OFFICE_TYPE_ONLYOFFICE => 'OnlyOffice', Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE => 'Collabora Office'
            }, array_keys($data)),
            'datasets' => [
                [
                    'label' => 'My First dataset',
                    'backgroundColor' => [
                        'rgb(255, 99, 132)',
                        'rgb(54, 162, 235)',
                        'rgb(255, 205, 86)'
                    ],
                    // 'borderColor' => 'rgb(255, 99, 132)',
                    'data' => array_values($data),
                ],
            ],
        ]);

        $chart->setOptions([
            'plugins' => [
                'title' => [
                    'display' => true,
                    'text' => $this->translator->trans('Office type distribution')
                ]
            ]
        ]);
        return $chart;
    }
}
