<?php

namespace App\Controller\Admin;

use App\Entity\Space;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Translation\TranslatableMessage;

use function Symfony\Component\Translation\t;

class SpaceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Space::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('domain')->hideOnForm()->setLabel(new TranslatableMessage('Domain')),
            TextField::new('organizationName')->setLabel(new TranslatableMessage('Organization name')),
            EmailField::new('contactEmail')->setLabel(new TranslatableMessage('Contact email')),
            TextField::new('contactName')->setLabel(new TranslatableMessage('Contact name')),
            ChoiceField::new('status')->setLabel(new TranslatableMessage('Status'))->renderAsBadges([
                Space::SPACE_STATUS_PENDING => 'info',
                Space::SPACE_STATUS_CREATED => 'success',
            ])
                ->allowMultipleChoices(false)
                ->renderExpanded()
                ->setTranslatableChoices([
                    Space::SPACE_STATUS_PENDING => t('Pending'),
                    Space::SPACE_STATUS_CREATED => t('Created'),
                ]),
            ChoiceField::new('office')->setLabel(new TranslatableMessage('Office solution'))->renderAsBadges([
                Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE => 'info',
                Space::SPACE_OFFICE_TYPE_ONLYOFFICE => 'info',
            ])
                ->allowMultipleChoices(false)
                ->renderExpanded()
                ->setChoices([
                    'Collabora Office' => Space::SPACE_OFFICE_TYPE_COLLABORA_OFFICE,
                    'OnlyOffice' => Space::SPACE_OFFICE_TYPE_ONLYOFFICE,
                ]),
            AssociationField::new('submission')->setLabel(new TranslatableMessage('Submission'))
        ];
    }
}
