<?php

namespace App\Controller\Admin;

use App\Entity\Job;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use Symfony\Component\Translation\TranslatableMessage;

use function Symfony\Component\Translation\t;

class JobCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Job::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            ChoiceField::new('type')->setLabel(new TranslatableMessage('Type'))
                ->renderAsBadges([
                    Job::JOB_TYPE_CREATE_SPACE => 'success',
                    Job::JOB_TYPE_UPDATE_SPACE => 'info',
                    Job::JOB_TYPE_CHANGE_OFFICE => 'info',
                    Job::JOB_TYPE_DISABLE_SPACE => 'warning',
                    Job::JOB_TYPE_REENABLE_SPACE => 'success',
                    Job::JOB_TYPE_DELETE_SPACE => 'danger',
                ])
                ->allowMultipleChoices(false)
                ->renderExpanded()
                ->setTranslatableChoices([
                    Job::JOB_TYPE_CREATE_SPACE => t('Create space'),
                    Job::JOB_TYPE_UPDATE_SPACE => t('Update space'),
                    Job::JOB_TYPE_CHANGE_OFFICE => t('Change office suite'),
                    Job::JOB_TYPE_DISABLE_SPACE => t('Disable space'),
                    Job::JOB_TYPE_REENABLE_SPACE => t('Reenable space'),
                    Job::JOB_TYPE_DELETE_SPACE => t('Delete space')
                ]),
            ChoiceField::new('status')
                ->setLabel(new TranslatableMessage('Status'))
                ->renderAsBadges([
                    Job::JOB_STATUS_PENDING => 'info',
                    Job::JOB_STATUS_ONGOING => 'info',
                    Job::JOB_STATUS_ERROR => 'danger',
                    Job::JOB_STATUS_DONE => 'success'
                ])
                ->allowMultipleChoices(false)
                ->renderExpanded()
                ->setTranslatableChoices([
                    Job::JOB_STATUS_PENDING => t('Pending'),
                    Job::JOB_STATUS_ONGOING => t('Ongoing'),
                    Job::JOB_STATUS_ERROR => t('Error'),
                    Job::JOB_STATUS_DONE => t('Success')
                ]),
            AssociationField::new('space')->setLabel(new TranslatableMessage('Space')),
            DateTimeField::new('createdAt')->setLabel(new TranslatableMessage('Created at')),
            DateTimeField::new('updatedAt')->setLabel(new TranslatableMessage('Updated at'))
        ];
    }
}
