<?php

namespace App\Controller\Admin;

use App\Entity\Invite;
use App\Entity\Job;
use App\Entity\Space;
use App\Entity\Submission;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Ldap\Security\LdapUser;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Translation\TranslatableMessage;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        /** @var AdminUrlGenerator $adminUrlGenerator */
        $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        return $this->redirect($adminUrlGenerator->setController(SubmissionCrudController::class)->generateUrl());
        // return parent::index();

        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        // $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        // return $this->redirect($adminUrlGenerator->setController(OneOfYourCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        // return $this->render('some/path/my-dashboard.html.twig');
    }

    public function configureUserMenu(UserInterface $user): UserMenu
    {
        /** @var LdapUser $user */
        $displayName = $user->getEntry()->getAttribute('displayName')[0] ?? $user->getUserIdentifier();
        $email = $user->getEntry()->getAttribute('mail')[0] ?? '';
        $userMenu = parent::configureUserMenu($user)
            // use the given $user object to get the user name
            ->setName($displayName);

        $avatarData = $user->getEntry()->getAttribute('jpegPhoto')[0] ?? false;
        if ($avatarData) {
            $userMenu->setAvatarUrl('data:image/jpeg;base64,' . base64_encode($avatarData));
        } else {
            $userMenu->setGravatarEmail($email);
        }
        return $userMenu;
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Charon');
    }

    public function configureMenuItems(): iterable
    {
        // yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud(new TranslatableMessage('Submissions'), 'fa-solid fa-envelope', Submission::class);
        yield MenuItem::linkToCrud(new TranslatableMessage('Spaces'), 'fa-solid fa-cloud', Space::class);
        yield MenuItem::linkToCrud(new TranslatableMessage('Jobs'), 'fa-solid fa-right-left', Job::class);
        yield MenuItem::linkToCrud(new TranslatableMessage('Invites'), 'fa-solid fa-paper-plane', Invite::class);

        yield MenuItem::section(new TranslatableMessage('Links'));
        yield MenuItem::linkToRoute(new TranslatableMessage('Statistics'), 'fa-solid fa-chart-line', 'admin_stats');
        yield MenuItem::linkToUrl(new TranslatableMessage('Documentation'), 'fa-solid fa-book', 'https://asso.framasoft.org/wiki/doku.php/contributions/framaspace')->setLinkTarget('_blank');
        yield MenuItem::linkToUrl(new TranslatableMessage('Technical'), 'fa-solid fa-wrench', 'https://tech.framasoft.org/doku.php/services:framaspace:charon')->setLinkTarget('_blank');
    }
}
