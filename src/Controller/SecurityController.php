<?php

namespace App\Controller;

use App\Entity\Space;
use App\Repository\SpaceRepository;
use App\Services\Notifications\Mailer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Http\LoginLink\LoginLinkHandlerInterface;

class SecurityController extends AbstractController
{
    #[Route('/login', name: 'login')]
    public function requestLoginLink(LoginLinkHandlerInterface $loginLinkHandler, AuthenticationUtils $authenticationUtils, SpaceRepository $spaceRepository, Mailer $mailer, Request $request): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // check if login form is submitted
        if ($request->isMethod('POST')) {
            // load the user in some way (e.g. using the form input)
            $email = $request->request->get('email');
            $space = $spaceRepository->findOneBy(['contactEmail' => $email]);

            if ($space) {
                // create a login link for $user this returns an instance
                // of LoginLinkDetails
                $loginLinkDetails = $loginLinkHandler->createLoginLink($space);

                $mailer->sendLoginByEmailToken($loginLinkDetails, $space);
            }

            // ... send the link and return a response (see next section)
            return $this->redirectToRoute('login_link_sent', ['email' => $email]);
        }

        // if it's not submitted, render the "login" form
        return $this->renderForm('security/login.html.twig', ['error' => $error]);
    }

    #[Route('/login/sent/{email}', name: 'login_link_sent')]
    public function loginLinkSent(string $email): Response
    {
        return $this->render('security/login_link_sent.html.twig', ['email' => $email]);
    }

    /**
     * @return never
     */
    #[Route('/login_check', name: 'login_check')]
    public function check()
    {
        throw new \LogicException('This code should never be reached');
    }

    #[Route('/logout', name: 'logout', methods: ['GET'])]
    public function logout(): void
    {
        // controller can be blank: it will never be called!
    }
}
