<?php

namespace App\Controller;

use App\Entity\Job;
use App\Entity\Space;
use App\Repository\JobRepository;
use App\Repository\SpaceRepository;
use App\Repository\SubmissionRepository;
use App\Services\Notifications\Mailer;
use App\Services\OrganizationLookup\CheckRNAService;
use App\Services\OrganizationLookup\CheckSIRETService;
use App\Validator\ReservedDomains;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Attributes as OA;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;
use Symfony\Component\RateLimiter\RateLimiterFactory;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\Context\Normalizer\ObjectNormalizerContextBuilder;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class APIController extends AbstractController
{
    private SerializerInterface $serializer;
    private LoggerInterface $logger;
    private ObjectManager $em;

    public function __construct(SerializerInterface $serializer, LoggerInterface $logger, ManagerRegistry $doctrine)
    {
        $this->serializer = $serializer;
        $this->logger = $logger;
        $this->em = $doctrine->getManager();
    }

    #[Route('/api/v1/searchOrganization', methods: ['POST'])]
    #[OA\Response(response: 200, description: 'Returns organization results in INSEE and DataGouv datasets matching search text', content: new OA\JsonContent(type: 'array', items: new OA\Items()))]
    #[OA\Response(response: 400, description: 'Returns 400 if the search term is missing')]
    #[OA\Response(response: 404, description: 'Returns 409 conflict if no results were found')]
    #[OA\RequestBody(description: 'The text to search', required: true, content: [new OA\MediaType(mediaType: 'application/x-www-form-urlencoded', schema: new OA\Schema(required: ['search'], properties: [new OA\Property(property: 'search', type: 'string')], type: 'object'))])]
    public function searchOrganization(Request $request, CheckRNAService $rnaService, CheckSIRETService $siretService, SerializerInterface $serializer, RateLimiterFactory $anonymousApiLimiter): JsonResponse
    {
        $limiter = $anonymousApiLimiter->create($request->getClientIp());
        $limit = $limiter->consume(1);

        $headers = [
            'X-RateLimit-Remaining' => $limit->getRemainingTokens(),
            'X-RateLimit-Retry-After' => $limit->getRetryAfter()->getTimestamp(),
            'X-RateLimit-Limit' => $limit->getLimit(),
        ];

        if (false === $limit->isAccepted()) {
            throw new TooManyRequestsHttpException();
        }

        $text = (string) $request->request->get('search');
        $text = trim($text);
        if (!$text) {
            $this->logger->debug("Called searchOrganization without search request input");
            return new JsonResponse(['error' => 'No search term passed'], 400);
        }
        try {
            if (preg_match("/^W[0-9]{9}$/", $text)) {
                $elements = $rnaService->getInfosFromText($text);
            } else {
                $elements = $siretService->getInfosFromText($text);
            }
        } catch (\Exception $e) {
            return new JsonResponse(['error' => 'Unable to get details from data providers', 'details' => $e->getMessage()], 404);
        }
        if (empty($elements)) {
            return new JsonResponse(['error' => 'No results found'], 404);
        }
        return new JsonResponse($serializer->serialize($elements, 'json'), 200, $headers, true);
    }

    #[Route('/api/v1/isAvailable', methods: ['POST'])]
    #[OA\Response(response: 200, description: 'Returns 200 if the subdomain is available')]
    #[OA\Response(response: 400, description: 'Returns 400 if the search term is missing')]
    #[OA\Response(response: 409, description: 'Returns 409 conflict if the subdomain is NOT available')]
    #[OA\RequestBody(description: 'The subdomain to search availability for', required: true, content: [new OA\MediaType(mediaType: 'application/x-www-form-urlencoded', schema: new OA\Schema(required: ['subdomain'], properties: [new OA\Property(property: 'subdomain', type: 'string')], type: 'object'))])]
    public function isAvailable(Request $request, SubmissionRepository $submissionRepository, RateLimiterFactory $anonymousApiLimiter, ValidatorInterface $validator): Response
    {
        $limiter = $anonymousApiLimiter->create($request->getClientIp());
        $limit = $limiter->consume(1);

        $headers = [
            'X-RateLimit-Remaining' => $limit->getRemainingTokens(),
            'X-RateLimit-Retry-After' => $limit->getRetryAfter()->getTimestamp(),
            'X-RateLimit-Limit' => $limit->getLimit(),
        ];

        if (false === $limit->isAccepted()) {
            throw new TooManyRequestsHttpException();
        }

        $text = (string) $request->request->get('subdomain');
        $text = trim($text);

        $errors = $validator->validate($text, new ReservedDomains());

        if ($errors->count()) {
            return new Response(null, 409, $headers);
        }

        if (!$text) {
            $this->logger->debug("Called isAvailable without subdomain request input");
            return new Response(null, 400, $headers);
        }
        if ($submissionRepository->findOneBy(['domain' => $text]) === null) {
            return new Response(null, 200, $headers);
        } else {
            return new Response(null, 409, $headers);
        }
    }

    #[Route('/api/v1/jobs', methods: ['GET'])]
    #[OA\Response(response: 200, description: 'Returns the list of jobs', content: new OA\JsonContent(schema: 'PaginatedResult', allOf: [new OA\Schema('#/components/schemas/PaginatedResult'), new OA\Schema(properties: ['items' => new OA\Property(property: 'items', type: 'array', items: new OA\Items(ref: new Model(type: Job::class)))], type: 'object')]))]
    #[OA\Response(ref: '#/components/responses/401', response: '401')]
    #[OA\Parameter(parameter: 'status', name: 'status', description: "Filter by job status\n\nStatuses:\n * `0`: Status pending\n * `10`: Status ongoing\n * `50`: Status error\n * `100`: Status done", in: 'query', schema: new OA\Schema(type: 'integer', enum: [0, 10, 50, 100]))]
    #[OA\Parameter(ref: '#/components/parameters/page')]
    #[OA\Parameter(ref: '#/components/parameters/perPage')]
    #[Security(name: 'httpAuth')]
    public function getJobs(Request $request, JobRepository $jobRepository): JsonResponse
    {
        $page = (int) $request->query->get('page', '1');
        $maxPerPage = (int) $request->query->get('perPage', '10');

        $query = $jobRepository->findAllQueryWithFilterQueryBuilder((int) $request->query->get('status'));

        $pagerFanta = new Pagerfanta(new QueryAdapter($query));
        $pagerFanta->setCurrentPage($page);
        $pagerFanta->setMaxPerPage($maxPerPage);

        return $this->json($pagerFanta);
    }

    #[Route('/api/v1/jobs/{job}', methods: ['PUT'])]
    #[OA\Parameter(name: 'job', in: 'path', schema: new OA\Schema(type: 'integer'))]
    #[OA\Response(response: 200, description: "Returns the job that was updated (status is now ongoing)", content: new OA\JsonContent(ref: new Model(type: Job::class)))]
    #[OA\Response(response: 404, description: "Returns 404 if not found")]
    #[OA\Response(ref: '#/components/responses/401', response: '401')]
    #[Security(name: 'httpAuth')]
    public function updateJob(Job $job): JsonResponse
    {
        $job->setStatus(JOB::JOB_STATUS_ONGOING);
        $this->em->persist($job);
        $this->em->flush();
        return new JsonResponse($this->serializer->serialize($job, 'json'), 200, [], true);
    }

    #[Route('/api/v1/jobs/{job}', methods: ['POST'])]
    #[OA\Response(response: 200, description: "Returns the job that was finalized (status is now done or error)", content: new OA\JsonContent(ref: new Model(type: Job::class)))]
    #[OA\Parameter(name: 'job', in: 'path', schema: new OA\Schema(type: 'integer'))]
    #[OA\RequestBody(description: 'The job result', required: true, content: new OA\JsonContent(required: ['success', 'payload'], properties: [new OA\Property(property: 'success', type: 'boolean'), new OA\Property(property: 'payload', type: 'string')]))]
    #[OA\Response(ref: '#/components/responses/401', response: '401')]
    #[Security(name: 'httpAuth')]
    public function finalizeJob(Request $request, Job $job): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $status = ($data['success'] ?? false) === true ? JOB::JOB_STATUS_DONE : Job::JOB_STATUS_ERROR;
        $job->setStatus($status);
        $job->setBody($data['payload'] ?? null);
        $this->em->persist($job);
        $this->em->flush();
        return new JsonResponse($this->serializer->serialize($job, 'json'), 200, [], true);
    }

    #[Route('/api/v1/spaces', methods: ['GET'])]
    #[OA\Parameter(name: 'enabled', description: 'Filter by enabled status', in: 'query', schema: new OA\Schema(type: 'string', enum: ['true', 'false']))]
    #[OA\Parameter(name: 'status', description: "Filter by status.\n\nStatuses:\n * `0`: Status pending\n * `10`: Status created", in: 'query', schema: new OA\Schema(type: 'integer', enum: [0, 10]))]
    #[OA\Parameter(ref: '#/components/parameters/page')]
    #[OA\Parameter(ref: '#/components/parameters/perPage')]
    #[OA\Response(response: 200, description: 'Returns the list of spaces', content: new OA\JsonContent(schema: 'PaginatedResult', allOf: [new OA\Schema('#/components/schemas/PaginatedResult'), new OA\Schema(properties: ['items' => new OA\Property(property: 'items', type: 'array', items: new OA\Items(ref: new Model(type: Space::class)))], type: 'object')]))]
    #[OA\Response(ref: '#/components/responses/401', response: '401')]
    #[Security(name: 'httpAuth')]
    public function listSpaces(Request $request, SpaceRepository $spaceRepository): JsonResponse
    {
        $enabled = $request->query->get('enabled');
        $query = $spaceRepository->findAllQueryWithFilterQueryBuilder($enabled !== null ? filter_var($enabled, FILTER_VALIDATE_BOOLEAN) : null, (int) $request->query->get('status'));
        $page = (int) $request->query->get('page', '1');
        $maxPerPage = (int) $request->query->get('perPage', '10');

        $pagerFanta = new Pagerfanta(new QueryAdapter($query));
        $pagerFanta->setCurrentPage($page);
        $pagerFanta->setMaxPerPage($maxPerPage);

        return $this->json($pagerFanta);
    }

    #[Route('/api/v1/spaces/{domain}', methods: ['GET'])]
    #[OA\Response(response: 200, description: "Returns a space", content: new OA\JsonContent(ref: new Model(type: Space::class)))]
    #[OA\Response(ref: '#/components/responses/401', response: '401')]
    #[Security(name: 'httpAuth')]
    public function getSpace(Space $space): JsonResponse
    {
        $context = (new ObjectNormalizerContextBuilder())
            ->withGroups('space')
            ->toArray();
        return new JsonResponse($this->serializer->serialize($space, 'json', $context), 200, [], true);
    }

    #[Route('/api/v1/spaces/{domain}', methods: ['POST'])]
    #[OA\Response(response: 200, description: "Returns the updated space", content: new OA\JsonContent(ref: new Model(type: Space::class)))]
    #[OA\Response(ref: '#/components/responses/401', response: '401')]
    #[Security(name: 'httpAuth')]
    public function updateInstance(Space $space, ManagerRegistry $doctrine, Mailer $mailer): JsonResponse
    {
        $space->setStatus(Space::SPACE_STATUS_CREATED);
        $entityManager = $doctrine->getManager();
        $entityManager->persist($space);
        $entityManager->flush();

        $mailer->sendCreationSuccessEmail($space);
        return new JsonResponse($this->serializer->serialize($space, 'json'), 200, [], true);
    }
}
