<?php

namespace App\Controller;

use App\Entity\Job;
use App\Entity\NextcloudGroup;
use App\Entity\NextcloudUser;
use App\Entity\Space;
use App\Form\Type\DeleteSpaceType;
use App\Form\Type\ReactivateSpaceType;
use App\Form\Type\SpaceType;
use App\Form\Type\UserProvisioningImportType;
use App\HTTP\Nextcloud\ProvisioningClient;
use App\SpaceManager;
use Doctrine\Persistence\ManagerRegistry;
use League\Csv\Reader;
use League\Csv\Statement;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Translation\TranslatableMessage;

class SpaceController extends AbstractController
{
    private ManagerRegistry $doctrine;
    private SpaceManager $spaceManager;

    public function __construct(ManagerRegistry $doctrine, SpaceManager $spaceManager)
    {
        $this->doctrine = $doctrine;
        $this->spaceManager = $spaceManager;
    }

    #[Route('/space', name: 'manage_space')]
    public function index(Request $request): Response
    {
        /** @var Space $space */
        $space = $this->getUser();
        $jobs = array_filter($space->getJobs()->getValues(), fn (Job $job) => $job->getStatus() !== Job::JOB_STATUS_DONE);
        $hasOngoingJobs = count($jobs) > 0;
        $oldOfficeType = $space->getOffice();
        $form = $this->createForm(SpaceType::class, $space, ['disabled' => $hasOngoingJobs || $space->getDeletedAt() !== null]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->doctrine->getManager();
            $entityManager->persist($space);

            if ($oldOfficeType !== $space->getOffice()) {
                $job = (new Job())
                    ->setSpace($space)
                    ->setType(Job::JOB_TYPE_CHANGE_OFFICE)
                    ->setStatus(Job::JOB_STATUS_PENDING)
                    ->setCreatedAt(new \DateTimeImmutable())
                    ->setData([...$space->getDetails(), 'office_type' => $space->getOffice()])
                ;
                $entityManager->persist($job);
                $this->addFlash('info', new TranslatableMessage('The change of office type has been scheduled. The changes should be applied in a few minutes. Your space might be briefly unavailable during the changes.'));
            }

            $entityManager->flush();

            return $this->redirectToRoute('app_index_index');
        }

        return $this->renderForm('space/edit.html.twig', ['form' => $form, 'organization' => $space->getOrganizationName(), 'has_ongoing_jobs' => $hasOngoingJobs, 'deleted_at' => $space->getDeletedAt(), 'real_deletion_date' => $space->getDeletedAt()?->add(new \DateInterval('P30D'))]);
    }

    #[Route('/space/pre-delete', name: 'pre-delete')]
    public function preDeleteConfirmation(Request $request): Response
    {
        /** @var Space $space */
        $space = $this->getUser();

        $form = $this->createForm(DeleteSpaceType::class, [], ['subdomain_compare' => $space->getDomain()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $job = (new Job())
                ->setType(Job::JOB_TYPE_DISABLE_SPACE)
                ->setSpace($space)
                ->setData([...$space->getDetails(), 'office_type' => $space->getOffice()]);

            $this->spaceManager->preDisableSpace($space);

            $entityManager = $this->doctrine->getManager();
            $entityManager->persist($job);
            $entityManager->flush();

            $this->addFlash('info', new TranslatableMessage("The deletion of your space has been scheduled. The changes should be applied in a few minutes. We will send you an email when that's done."));

            return $this->redirectToRoute('app_index_index');
        }

        return $this->renderForm('space/delete_confirmation.html.twig', [
            'form' => $form,
            'organization' => $space->getOrganizationName(),
            'subdomain' => $space->getDomain(),
            'deletion_date' => (new \DateTime())->add(new \DateInterval('P30D'))
        ]);
    }

    #[Route('/space/pre-reactivate', name: 'pre-reactivate')]
    public function preReactivate(Request $request): Response
    {
        /** @var Space $space */
        $space = $this->getUser();

        $form = $this->createForm(ReactivateSpaceType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $job = (new Job())
                ->setType(Job::JOB_TYPE_REENABLE_SPACE)
                ->setSpace($space)
                ->setData([...$space->getDetails(), 'office_type' => $space->getOffice()])
            ;

            $this->spaceManager->preReenableSpace($space);

            $entityManager = $this->doctrine->getManager();
            $entityManager->persist($space);
            $entityManager->persist($job);
            $entityManager->flush();

            $this->addFlash('info', new TranslatableMessage("The reactivation of your space has been scheduled. The changes should be applied in a few minutes. We will send you an email when that's done."));

            return $this->redirectToRoute('app_index_index');
        }

        return $this->renderForm('space/reactivate.html.twig', [
            'form' => $form,
            'organization' => $space->getOrganizationName(),
            'subdomain' => $space->getDomain(),
            'deletion_date' => (new \DateTime())->add(new \DateInterval('P30D'))
        ]);
    }

    public function import(Request $request, ProvisioningClient $provisioningClient): void
    {
        $form = $this->createForm(UserProvisioningImportType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Space $space */
            $space = $this->getUser();

            /** @var UploadedFile $importFile */
            $importFile = $form->get('file')->getData();

            $csv = Reader::createFromString($importFile->getContent());
            //$csv->setHeaderOffset(1);
            $stmt = Statement::create()
                ->offset(1)
                ->limit(1000)
            ;
            $records = $stmt->process($csv);

            $users = [];
            $groups = [];

            foreach ($records as $record) {
                $user = new NextcloudUser();
                $user->setUserId($record[0])
                    ->setEmail($record[1])
                    ->setDisplayName($record[2])
                    ->setGroups($record[3]);
                array_push($groups, ...explode(',', $record[3]));
                $users[] = $user;
            }

            foreach ($groups as $group) {
                $groupEntity = new NextcloudGroup();
                $groupEntity->setGroupId($group);
                $provisioningClient->addGroup($space, $groupEntity);
            }

            foreach ($users as $user) {
                $provisioningClient->addUser($space, $user);
            }
        }
    }
}
