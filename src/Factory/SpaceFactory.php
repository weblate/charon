<?php

namespace App\Factory;

use App\Entity\Space;
use App\Repository\SpaceRepository;
use App\SpaceManager;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @extends ModelFactory<Space>
 *
 * @method static Space|Proxy createOne(array $attributes = [])
 * @method static Space[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Space[]|Proxy[] createSequence(array|callable $sequence)
 * @method static Space|Proxy find(object|array|mixed $criteria)
 * @method static Space|Proxy findOrCreate(array $attributes)
 * @method static Space|Proxy first(string $sortedField = 'id')
 * @method static Space|Proxy last(string $sortedField = 'id')
 * @method static Space|Proxy random(array $attributes = [])
 * @method static Space|Proxy randomOrCreate(array $attributes = [])
 * @method static Space[]|Proxy[] all()
 * @method static Space[]|Proxy[] findBy(array $attributes)
 * @method static Space[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Space[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static SpaceRepository|RepositoryProxy repository()
 * @method Space|Proxy create(array|callable $attributes = [])
 */
final class SpaceFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services)
    }

    protected function getDefaults(): array
    {
        return [
            // TODO add your default values here (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories)
            'domain' => self::faker()->unique()->domainWord(),
            'details' => [
                'nc_instanceid' => 'ocga9hsuvgjy',
                'nc_admin_username' => SpaceManager::DEFAULT_ADMIN_USERNAME,
                'nc_admin_email' => self::faker()->email(),
                'nc_remote_admin_username' => SpaceManager::DEFAULT_REMOTE_ADMIN_USERNAME,
                'nc_remote_admin_password' => self::faker()->password(),
                'nc_passwordsalt' => "rKdap5hz9jMWJ4iyzXCs0ZwnrldK\/y",
                'nc_secret' => 'uHP4W29JL0CXkTk+\/ZlpmItGW2V9vwmCN24KYsUByUrsSdnT',

                // Common office details
                'office_port' => 1026,
                'office_server' => 'office-1',

                // OnlyOffice details
                'oo_secret' => 'yGBp18zCqHzS0XG9wqnKXzZXnP4O9u',

                // High performance notify push server
                'nc_hp_backend' => 1027,
            ],
            'contactEmail' => self::faker()->email(),
            'contactName' => self::faker()->name(),
            'locale' => self::faker()->locale(),
            'status' => self::faker()->randomElement(Space::SPACE_STATUSES),
            'organizationName' => self::faker()->name(),
            'enabled' => self::faker()->boolean(),
            'office' => self::faker()->randomElement(Space::SPACE_OFFICE_TYPES),
            'createdAt' => \DateTimeImmutable::createFromMutable(self::faker()->dateTime()),
            'updatedAt' => \DateTimeImmutable::createFromMutable(self::faker()->dateTime()),
            'submission' => SubmissionFactory::createOne()
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(Space $space): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Space::class;
    }
}
