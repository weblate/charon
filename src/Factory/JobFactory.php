<?php

namespace App\Factory;

use App\Entity\Job;
use App\Repository\JobRepository;
use App\SpaceManager;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @extends ModelFactory<Job>
 *
 * @method static Job|Proxy createOne(array $attributes = [])
 * @method static Job[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Job[]|Proxy[] createSequence(array|callable $sequence)
 * @method static Job|Proxy find(object|array|mixed $criteria)
 * @method static Job|Proxy findOrCreate(array $attributes)
 * @method static Job|Proxy first(string $sortedField = 'id')
 * @method static Job|Proxy last(string $sortedField = 'id')
 * @method static Job|Proxy random(array $attributes = [])
 * @method static Job|Proxy randomOrCreate(array $attributes = [])
 * @method static Job[]|Proxy[] all()
 * @method static Job[]|Proxy[] findBy(array $attributes)
 * @method static Job[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Job[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static JobRepository|RepositoryProxy repository()
 * @method Job|Proxy create(array|callable $attributes = [])
 */
final class JobFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services)
    }

    protected function getDefaults(): array
    {
        return [
            // TODO add your default values here (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories)
            'type' => self::faker()->randomElement(Job::JOB_TYPES),
            'data' => [
                'nc_instanceid' => 'ocga9hsuvgjy',
                'nc_admin_username' => SpaceManager::DEFAULT_ADMIN_USERNAME,
                'nc_admin_email' => 'admin@myorganization.org',
                'nc_remote_admin_username' => SpaceManager::DEFAULT_REMOTE_ADMIN_USERNAME,
                'nc_remote_admin_password' => "{]:con(`TriPhd2#2|[O=$18A%}Az.",
                'nc_passwordsalt' => "rKdap5hz9jMWJ4iyzXCs0ZwnrldK\/y",
                'nc_secret' => 'uHP4W29JL0CXkTk+\/ZlpmItGW2V9vwmCN24KYsUByUrsSdnT',

                // Common office details
                'office_port' => 1026,
                'office_server' => 'office-1',

                // OnlyOffice details
                'oo_secret' => 'yGBp18zCqHzS0XG9wqnKXzZXnP4O9u',

                // High performance notify push server
                'nc_hp_backend' => 1027,
            ],
            'status' => self::faker()->randomElement(Job::JOB_STATUSES),
            'createdAt' => \DateTimeImmutable::createFromMutable(self::faker()->dateTime()),
            'updatedAt' => \DateTimeImmutable::createFromMutable(self::faker()->dateTime()),
            'space' => SpaceFactory::createOne()
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(Job $job): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Job::class;
    }
}
