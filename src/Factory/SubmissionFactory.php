<?php

namespace App\Factory;

use App\Entity\Submission;
use App\Repository\SubmissionRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @extends ModelFactory<Submission>
 *
 * @method static Submission|Proxy createOne(array $attributes = [])
 * @method static Submission[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Submission[]|Proxy[] createSequence(array|callable $sequence)
 * @method static Submission|Proxy find(object|array|mixed $criteria)
 * @method static Submission|Proxy findOrCreate(array $attributes)
 * @method static Submission|Proxy first(string $sortedField = 'id')
 * @method static Submission|Proxy last(string $sortedField = 'id')
 * @method static Submission|Proxy random(array $attributes = [])
 * @method static Submission|Proxy randomOrCreate(array $attributes = [])
 * @method static Submission[]|Proxy[] all()
 * @method static Submission[]|Proxy[] findBy(array $attributes)
 * @method static Submission[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Submission[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static SubmissionRepository|RepositoryProxy repository()
 * @method Submission|Proxy create(array|callable $attributes = [])
 */
final class SubmissionFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services)
    }

    protected function getDefaults(): array
    {
        return [
            // TODO add your default values here (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories)
            'type' => self::faker()->randomElement(Submission::SUBMISSION_TYPES),
            'name' => self::faker()->name(),
            'object' => self::faker()->text(),
            'topics' => self::faker()->words(),
            'mainActions' => self::faker()->text(),
            'domain' => self::faker()->unique()->domainWord(),
            'identity' => self::faker()->name(),
            'email' => self::faker()->email(),
            'status' => self::faker()->randomElement(Submission::SUBMISSION_STATUSES),
            'locale' => self::faker()->locale(),
            'alreadyUsed' => self::faker()->randomElements(Submission::SUBMISSION_ALREADY_USED_VALUES),
            'accountsNeeded' => 'something',
            'responsible' => self::faker()->randomElement(Submission::SUBMISSION_RESPONSIBLE_VALUES),
            'metadata' => [],
            'createdAt' => \DateTimeImmutable::createFromMutable(self::faker()->dateTime()),
            'updatedAt' => \DateTimeImmutable::createFromMutable(self::faker()->dateTime()),
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(Submission $submission): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Submission::class;
    }
}
